<?php

Route::get('/', function () {
    return view('welcome');
});
Route::redirect('/','/login'); //TEMPORAL

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');


Route::get('/home', 'HomeController@index')->middleware('auth','web')->name('home');

// RUTAS DE PROVEEDORES
Route::middleware(['auth','web'])->name('suppliers.')->prefix('suppliers')->group(function(){
    Route::get('/', 'SuppliersController@index')->name('index');
    Route::get('create','SuppliersController@create')->name('create');
    Route::get('show/{id}','SuppliersController@show')->where('id','[0-9]+')->name('show');
    Route::get('edit/{id}','SuppliersController@edit')->where('id','[0-9]+')->name('edit');

    Route::post('store','SuppliersController@store')->name('store');
    Route::post('update/{id}','SuppliersController@update')->where('id','[0-9]+')->name('update');
    Route::post('destroy','SuppliersController@destroy')->name('destroy');
});

//RUTAS CONSUMIBLES
Route::middleware(['auth','web'])->name('consumables.')->prefix('consumables')->group(function(){
    Route::get('/','ConsumablesController@index')->name('index');
    Route::get('create','ConsumablesController@create')->name('create');
    Route::get('show/{id}','ConsumablesController@show')->where('id','[0-9]+')->name('show');
    Route::get('edit/{id}','ConsumablesController@edit')->where('id','[0-9]+')->name('edit');
    Route::get('import', 'ConsumablesController@import')->name('import');

    Route::post('store', 'ConsumablesController@store')->name('store');
    Route::post('destroy', 'ConsumablesController@destroy')->name('destroy');
    Route::post('update/{id}','ConsumablesController@update')->where('id','[0-9]+')->name('update');
    Route::post('import/store','ConsumablesController@import_store')->name('import_store');
});

//RUTAS EQUIPO
Route::middleware(['auth','web'])->name('equipment.')->prefix('equipment')->group(function(){
    Route::get('/','EquipmentController@index')->name('index');
    Route::get('create','EquipmentController@create')->name('create');
    Route::get('show/{id}','EquipmentController@show')->where('id','[0-9]+')->name('show');
    Route::get('edit/{id}','EquipmentController@edit')->where('id','[0-9]+')->name('edit');
    Route::get('import','EquipmentController@import')->name('import');

    Route::post('destroy', 'EquipmentController@destroy')->name('destroy');
    Route::post('store', 'EquipmentController@store')->name('store');
    Route::post('update/{id}','EquipmentController@update')->where('id','[0-9]+')->name('update');
    Route::post('import/store','EquipmentController@import_store')->name('import_store');
});

//RUTAS DE PRESTAMOS
Route::middleware(['auth','web'])->name('loans.')->prefix('loans')->group(function(){
    Route::get('/','LoanController@index')->name('index');
    Route::get('/create','LoanController@create')->name('create');
    Route::get('/show/{id}','LoanController@show')->where('id','[0-9]+')->name('show');
    Route::get('/edit/{id}','LoanController@edit')->where('id','[0-9]+')->name('edit');

    Route::post('/store','LoanController@store')->name('store');
    Route::post('/update/{id}','LoanController@update')->name('update');
    Route::post('/destroy', 'LoanController@destroy')->name('destroy');
});

//RUTAS DE ORDEN DE COMPRAS
Route::middleware(['auth','web'])->name('purchase_orders.')->prefix('purchase_orders')->group(function(){
    Route::get('/', 'Purcharse_orderController@index')->name('index');
    Route::get('/create','Purcharse_orderController@create')->name('create');
    Route::get('/getItems/{id}','Purcharse_orderController@getItems')->where('id','[0-9]+')->name('getItems');
    Route::get('/getItem/{sku}','Purcharse_orderController@getItem')->name('getItem');
    Route::get('/order/{id}', 'Purcharse_orderController@edit')->where('id','[0-9]+')->name('edit');
    Route::get('/show/{id}', 'Purcharse_orderController@show')->where('id','[0-9]+')->name('show');

    Route::post('/store', 'Purcharse_orderController@store')->name('store');
    Route::post('/approve','Purcharse_orderController@approve')->name('approve');
    Route::post('/paid','Purcharse_orderController@paid')->name('paid');
    Route::post('/destroy', 'Purcharse_orderController@destroy')->name('destroy');
    Route::post('/update/{id}', 'Purcharse_orderController@update')->where('id','[0-9]+')->name('update');
});

//RUTAS PARA ORDENES DE ENTRADA
Route::middleware(['auth','web'])->name('income_orders.')->prefix('income_orders')->group(function(){
    Route::get('/','Income_orderController@index')->name('index');
    Route::get('/create','Income_orderController@create')->name('create');

    Route::post('/store','Income_orderController@store')->name('store');
});

//RUTAS PARA GASTOS
Route::middleware(['auth','web'])->name('expenditures.')->prefix('expenditures')->group(function(){
    Route::get('/', 'ExpenditureController@index')->name('index');
    Route::get('/create','ExpenditureController@create')->name('create');
    Route::get('/show/{id}','ExpenditureController@show')->where('id','[0-9]+')->name('show');
    Route::get('/edit/{id}','ExpenditureController@edit')->where('id','[0-9]+')->name('edit');

    Route::post('/store','ExpenditureController@store')->name('store');
    Route::post('/update/{id}','ExpenditureController@update')->where('id','[0-9]+')->name('update');
    Route::post('/destroy', 'ExpenditureController@destroy')->name('destroy');
});

//RUTAS PARA PROSPECTOS
Route::middleware(['auth','web'])->name('handbills.')->prefix('handbills')->group(function(){
    Route::get('/','HandbillController@index')->name('index');
    Route::get('/create','HandbillController@create')->name('create');
    Route::get('/edit/{id}','HandbillController@edit')->where('id','[0-9]+')->name('edit');

    Route::post('/store','HandbillController@store')->name('store');
    Route::post('/update/{id}','HandbillController@update')->where('id','[0-9]+')->name('update');
    Route::post('/destroy','HandbillController@destroy')->name('destroy');
});

//RUTAS PARA SERVICIOS
Route::middleware(['auth','web'])->name('services.')->prefix('services')->group(function(){
    Route::get('/', 'ServiceController@index')->name('index');
    Route::get('/create','ServiceController@create')->name('create');
    Route::get('/edit/{id}','ServiceController@edit')->where('id','[0-9]+')->name('edit');
    Route::get('/get_services/{price}','ServiceController@get_services')->name('get_services');

    Route::post('/store','ServiceController@store')->name('store');
    Route::post('/destroy','ServiceController@destroy')->name('destroy');
    Route::post('/update/{id}','ServiceController@update')->where('id','[0-9]+')->name('update');
});

//RUTAS PARA CLIENTES
Route::middleware(['auth','web'])->name('clients.')->prefix('clients')->group(function(){
    Route::get('/','ClientController@index')->name('index');
    Route::get('/create','ClientController@create')->name('create');
    Route::get('/check_rfc/{rfc}','ClientController@check_rfc')->name('check_rfc');
    Route::get('/price_list/{id}','ClientController@price_list')->name('price_list');

    Route::post('/store','ClientController@store')->name('store');
});

// RUTAS PARA COTIZACIONES
Route::middleware(['auth','web'])->name('quotations.')->prefix('quotations')->group(function(){
    Route::get('/', 'QuotationController@index')->name('index');
    Route::get('/create','QuotationController@create')->name('create');
    Route::get('/get_quotation/{id}','QuotationController@get_quotation')->name('get_quotation');

    Route::post('/store','QuotationController@store')->name('store');
    Route::post('/destroy','QuotationController@destroy')->name('destroy');
});

// RUTAS PARA REMISIONES
Route::middleware(['web','auth'])->name('referrals.')->prefix('referrals')->group(function(){
    Route::get('/','ReferralController@index')->name('index');
    Route::get('/create','ReferralController@create')->name('create');

    Route::post('/store','ReferralController@store')->name('store');
});
