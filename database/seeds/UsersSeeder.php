<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'SUPER_ADMIN';
        $user->email = 'admin@admin.com';
        $user->password = bcrypt('admin');

        $user->save();

        $user->assignRole('SuperAdmin');
    }
}
