<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('business');
            $table->string('tradename');
            $table->string('business_name');
            $table->string('rfc')->unique();
            $table->string('country');
            $table->string('state');
            $table->string('municipality');
            $table->string('neighborhood')->nullable();
            $table->string('street')->nullable();
            $table->integer('external_number');
            $table->string('internal_number')->nullable();
            $table->string('zipcode');
            $table->string('phone');
            $table->string('fax');
            $table->string('price_list')->nullable();
            $table->string('cfdi')->nullable();
            $table->string('bank_account');
            $table->string('bank');
            $table->string('email');
            $table->string('invoice_email');
            $table->string('credit_days');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
