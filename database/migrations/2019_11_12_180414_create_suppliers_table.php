<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('business_name');
            $table->string('tradename');
            $table->string('rfc')->unique();
            $table->string('credit_days')->nullable();
            $table->string('credit_amount')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('country');
            $table->string('state');
            $table->string('municipality');
            $table->string('colony')->nullable();
            $table->string('street')->nullable();
            $table->string('external_number');
            $table->string('internal_number')->nullable();
            $table->string('zipcode');
            $table->string('phone')->nullable();
            $table->enum('type',['Gastos','Compras']);
            $table->string('contact_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
