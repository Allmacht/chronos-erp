<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_operations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('packing_type')->nullable();
            $table->string('packing_measures')->nullable();
            $table->string('transport_type')->nullable();
            $table->string('product_handling')->nullable();
            $table->string('storage_conditions')->nullable();
            $table->string('delivery_schedule')->nullable();
            $table->float('product_value')->nullable();
            $table->boolean('foreign_commerce')->nullable();
            $table->boolean('import')->nullable();
            $table->boolean('export')->nullable();
            $table->string('foreign_commerce_number')->nullable();
            $table->boolean('tariff_fraction')->nullable();
            $table->boolean('customs_agent')->nullable();
            $table->boolean('quality_inspection')->nullable();
            $table->boolean('importers')->nullable();
            $table->string('dangerous_material')->nullable();
            $table->boolean('fragile')->nullable();

            $table->unsignedBigInteger('client_id');
            $table->foreign('client_id')->references('id')->on('clients');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_operations');
    }
}
