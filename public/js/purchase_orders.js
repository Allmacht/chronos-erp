var app = new Vue({
    el: '#purchase_orders',
    data: {
        items: [],
        selectedItems: [],
        selectedItem: "",
        date: "",
        supplier_id: "",
        getItems: "/purchase_orders/getItems/",
        getItem: "/purchase_orders/getItem/",
        storeURL: "/purchase_orders/store",
        message: "Orden creada correctamente",
        addButton: true,
        file: '',
        fileName: "Factura",
        tableHidden: true,
        total_price: 0,
        button_content: 'CREAR'
    },
    methods: {

        filename(event){
            this.fileName = event.target.files[0].name;
            this.file = event.target.files[0];
        },

        supplier(event){
            this.clear();
            axios.get(this.getItems+event.target.value)
            .then(response => {
                this.addButton = false;
                this.items = response.data.items;
            })
        },

        clear(){
            this.items = [];
            this.selectedItems = [];
            this.total_price = 0;
            this.selectedItem = "";
            this.tableHidden = true;
        },

        item(event){
            this.selectedItem = event.target.value;
        },

        addItem(){
            if(this.selectedItem != ""){
                axios.get(this.getItem+this.selectedItem)
                .then(response => {
                    this.selectedItems.push({
                        id: response.data.item.id,
                        sku: response.data.item.sku,
                        description: response.data.item.description,
                        initial_cost: response.data.item.initial_cost,
                        quantity: 1,
                        subtotal: response.data.item.initial_cost
                    });
                    this.total();
                    this.tableHidden = false;
                });
            }
        },

        quantity(event, index){
            this.selectedItems[index].quantity = event.target.value;
            this.selectedItems[index].subtotal = (event.target.value*this.selectedItems[index].initial_cost);
            this.total();
        },

        total(){
            this.total_price = 0;
            for (var i = 0; i < this.selectedItems.length; i++) {
                this.total_price += this.selectedItems[i].subtotal;
                if(this.total_price < 0){
                    this.total_price = 0;
                }
            }
        },

        removeRow(index){
            this.selectedItems.splice(index, 1);
            if (this.selectedItems.length < 1) {
                this.tableHidden = true;
            }
            this.total();
        },

        submitForm: async function(event){
            event.preventDefault();
            if(this.selectedItems.length > 0 && this.date != "" && this.supplier_id != "" && this.total_price > 0){
                this.button_content = "<i class='fas fa-spinner fa-pulse fa-lg mr-3'></i>CARGANDO...";
                var file = document.querySelector('#file');
                var data = new FormData();
                data.append("amount", this.total_price);
                data.append("supplier_id", this.supplier_id);
                data.append("date", this.date);
                data.append("folio", this.folio);
                data.append('file', $('input[type=file]')[0].files[0]);
                data.append("items", JSON.stringify(this.selectedItems));

                await $.ajax({
                    type: "POST",
                    url: this.storeURL,
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: data,
                    enctype: 'multipart/form-data',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                    success: function(response){
                        if(response.message == "OK"){

                            $('#correct').modal('show');
                        }
                    }
                })
                this.button_content = "CREAR";
            }
        },
    }
});

$('#correct').on('hidden.bs.modal', function (e) {
    location.reload();
})
