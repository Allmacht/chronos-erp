var app = new Vue({
    el:"#referrals",
    data:{
        services_url:"/services/get_services/",
        price_list_url:"/clients/price_list/",
        customs_declaration_url:"http://stage.chronosmexico.com/api/erp_chronos/customs_declarations",
        formats_url:"http://stage.chronosmexico.com/api/erp_chronos/formats/",
        store_url:"/referrals/store",
        previous_quote: "true",
        formats:[],
        selected_service: "",
        quotation_select:false,
        quotation_url:"/quotations/get_quotation/",
        quotation:"",
        customs_declarations:[],
        readonly:false,
        services:[],
        referral:{
            date:"",
            client_id:"",
            transaction_type:"",
            movement_type:"",
            format:"",
            customs_declaration:"",
            notice_type:"",
            notice:"",
            license_plate:"",
            container:"",
            invoice:"",
            invoice_name:"Factura",
        },
        quotation_details:[],
        total: 0,

    },

    mounted(){
        axios.get(this.customs_declaration_url,{
            headers: {
                'Access-Control-Allow-Credentials' : true,
                 'Access-Control-Allow-Origin':'*',
                 'Access-Control-Allow-Methods':'GET',
                 'Access-Control-Allow-Headers':'application/json',
        	},
        })
        .then(response => {
            for (var i = 0; i < response.data.length; i++) {
                this.customs_declarations.push(response.data[i])
            }
        })
    },

    watch:{
        'previous_quote': function(val, oldVal){
            if(this.previous_quote == "true"){
                this.quotation_select = false
            }else{
                this.quotation_select = true
            }
        },

        'referral.movement_type':function(val, oldVal){
            if(this.referral.movement_type != ""){
                this.formats = []
                axios.get(this.formats_url+this.referral.movement_type)
                .then(response => {
                    for (var i = 0; i < response.data.length; i++) {
                        this.formats.push(response.data[i])
                    }
                })
            }
        },

        'quotation_select': function(){
            this.quotation_details = []
            this.total = 0
            this.quotation = ""
            this.referral.client_id = ""
        },

        'referral.client_id': function(){
            this.services = []
            if(this.quotation_select == true && this.referral.client_id != ""){
                axios.get(this.price_list_url+this.referral.client_id)
                .then(response => {
                    axios.get(this.services_url+response.data.price_list)
                    .then(response => {
                        for (var i = 0; i < response.data.services.length; i++) {
                            this.services.push(response.data.services[i])
                        }
                    })
                })
            }
        },

        'quotation' : async function(val, oldVal){
            if (this.quotation != "") {
                this.quotation_details = []
                await axios.get(this.quotation_url+this.quotation)
                .then(response  => {
                    for (var i = 0; i < response.data.details.length; i++) {
                        this.quotation_details.push({
                            id:response.data.details[i].id,
                            name:response.data.details[i].service_name,
                            price:response.data.details[i].price,
                            quantity:response.data.details[i].quantity,
                            subtotal:response.data.details[i].subtotal
                        })
                    }
                    this.referral.client_id = response.data.client_id
                })
                this.readonly = true
                this.total_price()
            }
        },
    },
    methods:{
        'total_price' : function(){
            this.total = 0
            for(var i = 0; i < this.quotation_details.length; i++) {
                this.total += this.quotation_details[i].subtotal
            }
        },

        'add_service': function(){
            if (this.selected_service != ""){
                for (var i = 0; i < this.services.length; i++) {
                    if(this.services[i].id == this.selected_service){
                        this.quotation_details.push({
                            id:this.services[i].id,
                            name:this.services[i].name,
                            price:this.services[i].price,
                            quantity:1,
                            subtotal:this.services[i].price
                        })
                        break
                    }
                }
            }
            this.selected_service=""
            this.readonly = false
            this.total_price()
        },

        'add_quantity' : function(event, index){
            this.quotation_details[index].quantity = event.target.value
            this.quotation_details[index].subtotal = parseFloat((event.target.value * this.quotation_details[index].price).toFixed(2))
            this.total_price()
        },

        'remove_service' : function(index){
            this.quotation_details.splice(index, 1)
            this.total_price()
        },

        'invoice': function(event){
            this.referral.invoice = event.target.files[0]
            this.referral.invoice_name = event.target.files[0].name
        },

        'submit': function(event){
            event.preventDefault()
            var status = false;
            for(prop in this.referral){
                if (this.referral[prop] == "" && prop != 'invoice') {
                    status = false
                    break
                }
                else{
                    status = true
                }
            }

            if (status) {
                if(this.total > 0 && this.quotation_details.length > 0){
                    status = true
                }else {
                    status = false
                }
            }

            if(status){
                var data = new FormData();
                data.append("referral", JSON.stringify(this.referral))
                data.append("invoice", this.referral.invoice)
                data.append("quotation_details", JSON.stringify(this.quotation_details))
                data.append("total", this.total)
                data.append("quotation", this.quotation)

                axios.post(this.store_url, data)
                .then(response => {
                    $('#success').modal('show');
                })

            }
        }
    }
})
$('#success').on('hidden.bs.modal', function (e) {
  location.reload();
})
