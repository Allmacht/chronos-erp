$(document).ready(function(){
    $('.open-modal').click(function(){
        if($(this).data('action') === 'delete'){
            $('#delete-id').val($(this).data('id'));
        }
        if($(this).data('action') == 'approve'){
            $('#order-folio').empty().append('Folio : '+$(this).data('folio'));
            $('#order-amount').empty().append('Importe : $'+$(this).data('amount'));
            $('#order-id').val($(this).data('id'));
        }

        if($(this).data('action') == 'pay'){
            $('#pay-folio').empty().append('Folio : '+$(this).data('folio'));
            $('#pay-amount').empty().append('Importe : $'+$(this).data('amount'));
            $('#pay-id').val($(this).data('id'));
        }
    });
});
