var app = new Vue({
    el: '#quotations_create',
    data:{
        services_url:"/services/get_services/",
        price_list_url:"/clients/price_list/",
        store_url:"/quotations/store",
        quotation:{
            date:"",
            client_id:"",
            conditions:"",
            effect:"",
            amount: 0
        },
        services:[],
        selected_service:"",
        selected_services:[],
        add_button:true,
        table_show:true,
        button_submit:"CREAR"
    },

    watch:{
        'quotation.client_id' : function(val, oldVal){
            axios.get(this.price_list_url+this.quotation.client_id)
                .then(response => {
                    axios.get(this.services_url+response.data.price_list)
                    .then(response => {
                        for (var i = 0; i < response.data.services.length; i++) {
                            this.services.push(response.data.services[i])
                        }
                    })
                })
            this.add_button = false;
        }
    },

    methods:{
        'add_service' : function(event){
            if (this.selected_service != ""){
                for (var i = 0; i < this.services.length; i++) {
                    if (this.services[i].id == this.selected_service) {
                        this.selected_services.push({
                            id:this.services[i].id,
                            name:this.services[i].name,
                            price:this.services[i].price,
                            quantity: 1,
                            subtotal:this.services[i].price
                        })
                        break
                    }
                }
                this.table_show = false;
                this.selected_service = ""
                this.amount()
            }
        },

        'remove_service' : function(index){
            this.selected_services.splice(index, 1)
            if (this.selected_services.length < 1) {
                this.table_show = true;
            }
            this.amount()
        },

        'add_quantity' : function(event,index){
            this.selected_services[index].quantity = event.target.value
            this.selected_services[index].subtotal = Number((this.selected_services[index].quantity * this.selected_services[index].price).toFixed(2))
            this.amount()
        },

        'amount' : function(){
            this.quotation.amount = 0
            for (var i = 0; i < this.selected_services.length; i++) {
                this.quotation.amount += Number((this.selected_services[i].subtotal).toFixed(2))
            }
        },

        'submit' : async function(event){
            event.preventDefault()
            correct = false
            for(var prop in this.quotation){
                if (prop == 'amount') {
                    if (this.quotation[prop] == 0) {
                        correct = false
                        break
                    }else{
                        correct = true
                    }
                }
                else if(this.quotation[prop] == "") {
                    correct = false
                    break
                }else{
                    correct = true
                }
            }
            if (correct) {
                this.button_submit = "<i class='fas fa-spinner fa-pulse fa-lg mr-3'></i>CARGANDO..."
                var data = new FormData();
                data.append("quotation", JSON.stringify(this.quotation))
                data.append("services", JSON.stringify(this.selected_services))

                axios.post(this.store_url, data)
                .then(response => {
                    if(response.status){
                        this.button_submit = 'CREAR'
                        $('#success').modal('show')
                    }
                })
            }
        }
    }
})

$('#success').on('hidden.bs.modal', function (e) {
  location.reload();
})
