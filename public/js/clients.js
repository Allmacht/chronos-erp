var app = new Vue({
    el:"#client-store",
    data:{
        countriesURL: "https://restcountries.eu/rest/v2/all",
        check_rfc: "/clients/check_rfc/",
        get_services:"/services/get_services/",
        store_url:"/clients/store",
        button_text:"GUARDAR",
        services:[],
        regex: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        countries: [],
        prices: [
            {name:"Regular"},
            {name:"Premium"},
            {name:"Socio"}
        ],
        cfdis:[
            {key:'G01'},
            {key:'G02'},
            {key:'G03'},
            {key:'I01'},
            {key:'I02'},
            {key:'I03'},
            {key:'I04'},
            {key:'I05'},
            {key:'I06'},
            {key:'I07'},
            {key:'I08'},
            {key:'D01'},
            {key:'D02'},
            {key:'DO3'},
            {key:'D04'},
            {key:'D05'},
            {key:'D06'},
            {key:'D07'},
            {key:'D08'},
            {key:'D09'},
            {key:'D10'},
            {key:'P01'},
        ],
        client:{
            business:"",
            tradename:"",
            business_name:"",
            rfc:"",
            country:"",
            state:"",
            municipality:"",
            neighborhood:"",
            street:"",
            external_number:"",
            internal_number:"",
            country:"",
            zipcode:"",
            phone:"",
            fax:"",
            cfdi:"",
            bank_account:"",
            bank:"",
            email:"",
            invoice_email:"",
            credit_days:"",
            price_list: "",
            neighborhood:""
        },
        status_client:{
            rfc:"",
            zipcode:"",
            phone:"",
            bank_account:"",
            email:"",
            invoice_email:"",
        },
        steps:{
            step_mdc:"circulo-inactive",
            step_ddp:"circulo-inactive",
            step_odc:"circulo-inactive",
        },
        forms:{
            form_client:"form-show",
            form_mdc:"form-hidde",
            form_ddp:"form-hidde",
            form_odc:"form-hidde",
        },
        director:{
            type:"director",
            name:"",
            position:"",
            email:"",
            phone:"",
            check_email:"",
            check_phone:""
        },
        manager:{
            type:"manager",
            name:"",
            position:"",
            email:"",
            phone:"",
            check_email:"",
            check_phone:""
        },
        foreign:{
            type:"foreign",
            name:"",
            position:"",
            email:"",
            phone:"",
            check_email:"",
            check_phone:""
        },
        operations:{
            type:"operations",
            name:"",
            position:"",
            email:"",
            phone:"",
            check_email:"",
            check_phone:""
        },
        invoices:{
            type:"invoices",
            name:"",
            position:"",
            email:"",
            phone:"",
            check_email:"",
            check_phone:""
        },
        product_data:{
            general_description:"",
            file_message:"Disponibilidad de catálogo",
            catalog_availability:'',
            datasheet_availability:"",
            business_type:"",
            product_type:"",
            product_rotation:""
        },
        selected_service:"",
        selected_services:[],
        client_operations:{
            packing_type:"",
            packing_measures:"",
            transport_type:"",
            product_handling:"",
        	storage_conditions:"",
            delivery_schedule:'',
            delivery_schedule_name:"Programa de entrega",
            product_value:"",
            foreign_commerce: false,
            import: false,
            export: false,
            foreign_commerce_number:"",
            tariff_fraction: false,
            customs_agent: false,
            quality_inspection: false,
            importers: false,
            dangerous_material:"",
            dangerous_material_name:"Material peligroso y/o Hazmat",
            fragile: false
        }
    },

    mounted(){
        axios.get(this.countriesURL)
        .then(response => {
            for (var i = 0; i < response.data.length; i++) {
                if (response.data[i].translations.es) {
                    this.countries.push({name : response.data[i].translations.es})
                }else{
                    this.countries.push({name : response.data[i].name})
                }
            }
        })
    },

    watch:{
        'client.rfc' : function (val,oldVal){
            var rfc = val.trim()
            if(rfc.length >= 12 && rfc.length <=13){
                axios.get(this.check_rfc+rfc)
                .then(response => {
                    if(response.response == false){
                        this.status_client.rfc = 'is-invalid'
                    }else{
                        this.status_client.rfc = 'is-valid'
                    }
                })
            }else{
                this.status_client.rfc = 'is-invalid'
            }
        },

        'client.zipcode' : function(val, oldVal){
            if(val.length >= 5 && !isNaN(val)){
                this.status_client.zipcode = 'is-valid'
            }else{
                this.status_client.zipcode = 'is-invalid'
            }
        },

        'client.phone' : function(val, oldVal){
            if(val.length >= 10 && !isNaN(val)){
                this.status_client.phone = 'is-valid'
            }else{
                this.status_client.phone = 'is-invalid'
            }
        },

        'client.bank_account' : function(val, oldVal){
            if(val.length == 20 && !isNaN(val)){
                this.status_client.bank_account = 'is-valid'
            }else{
                this.status_client.bank_account = 'is-invalid'
            }
        },

        'client.email' : function(val, oldVal){
            if(this.regex.test(val)){
                this.status_client.email = 'is-valid'
            }else{
                this.status_client.email = 'is-invalid'
            }
        },

        'client.invoice_email' : function(val, oldVal){
            if(this.regex.test(val)){
                this.status_client.invoice_email = 'is-valid'
            }else{
                this.status_client.invoice_email = 'is-invalid'
            }
        },

        'director.email' : function(val,oldVal){
            if(this.regex.test(val)){
                this.director.check_email = 'is-valid'
            }else{
                this.director.check_email = 'is-invalid'
            }
        },

        'director.phone' : function(val,oldVal){
            if(val.length >= 10 && !isNaN(val)){
                this.director.check_phone = 'is-valid'
            }else{
                this.director.check_phone = 'is-invalid'
            }
        },

        'manager.email' : function(val,oldVal){
            if(this.regex.test(val)){
                this.manager.check_email = 'is-valid'
            }else{
                this.manager.check_email = 'is-invalid'
            }
        },

        'manager.phone' : function(val,oldVal){
            if(val.length >= 10 && !isNaN(val)){
                this.manager.check_phone = 'is-valid'
            }else{
                this.manager.check_phone = 'is-invalid'
            }
        },

        'operations.email' : function(val,oldVal){
            if(this.regex.test(val)){
                this.operations.check_email = 'is-valid'
            }else{
                this.operations.check_email = 'is-invalid'
            }
        },

        'operations.phone' : function(val,oldVal){
            if(val.length >= 10 && !isNaN(val)){
                this.operations.check_phone = 'is-valid'
            }else{
                this.operations.check_phone = 'is-invalid'
            }
        },

        'foreign.email' : function(val,oldVal){
            if(this.regex.test(val)){
                this.foreign.check_email = 'is-valid'
            }else{
                this.foreign.check_email = 'is-invalid'
            }
        },

        'foreign.phone' : function(val,oldVal){
            if(val.length >= 10 && !isNaN(val)){
                this.foreign.check_phone = 'is-valid'
            }else{
                this.foreign.check_phone = 'is-invalid'
            }
        },

        'invoices.email' : function(val,oldVal){
            if(this.regex.test(val)){
                this.invoices.check_email = 'is-valid'
            }else{
                this.invoices.check_email = 'is-invalid'
            }
        },

        'invoices.phone' : function(val,oldVal){
            if(val.length >= 10 && !isNaN(val)){
                this.invoices.check_phone = 'is-valid'
            }else{
                this.invoices.check_phone = 'is-invalid'
            }
        },

        'forms.form_ddp' : function(val, oldVal){
            var price = this.client.price_list;
            axios.get(this.get_services+price)
            .then(response => {
                for (var i = 0; i < response.data.services.length; i++) {
                    this.services.push({
                        id: response.data.services[i].id,
                        name: response.data.services[i].name,
                        price: response.data.services[i].price,
                        key: response.data.services[i].key
                    })
                }
            })
        }

    },

    methods:{

        hidde_all(form_show){
            for(form in this.forms){
                this.forms[form] = 'form-hidde'
            }
            this.forms[form_show] = 'form-show'
        },

        check_form_client(){
            var check = false;
            for (const prop in this.client) {
                if(this.client[prop].length >= 1){
                    check = true;
                }else{
                    if(prop == 'internal_number' || prop == 'neighborhood'){
                        check = true;
                    }else{
                        check = false;
                        break;
                    }
                }
            }

            if(check == true){
                for(const status in this.status_client){
                    if(this.status_client[status] == 'is-valid'){
                        check = true;
                    }else{
                        check = false;
                        break;
                    }
                }
            }

            if(check){
                this.steps.step_mdc = "circulo-active";
                this.hidde_all('form_mdc');
            }
        },

        form_mdc_back(){
            this.steps.step_mdc = "circulo-inactive";
            this.hidde_all('form_client');
        },

        check_form_mdc(){
            var director = this.contacts_validate(this.director);
            var manager = this.contacts_validate(this.manager);
            var foreign = this.contacts_validate(this.foreign);
            var operations = this.contacts_validate(this.operations);
            var invoices = this.contacts_validate(this.invoices);

            if(director && manager && foreign && operations && invoices){
                this.steps.step_ddp = "circulo-active";
                this.hidde_all('form_ddp');
            }
        },

        check_form_ddp(){
            if(this.selected_services.length > 0){
                this.steps.step_odc = "circulo-active";
                this.hidde_all('form_odc');
            }
        },

        contacts_validate(data){
            var validate = false
            for(prop in data){
                if(data[prop].length >= 1){
                    if(prop == 'check_email' || prop== "check_phone"){
                        if(data[prop] == 'is-valid'){
                            validate = true;
                        }else{
                            validate = false;
                            break;
                        }
                    }else{
                        validate = true;
                    }
                }else{
                    validate = false;
                    break;
                }
            }

            return validate;
        },

        add_service(){
            for (var i = 0; i < this.services.length; i++) {
                if(this.services[i].id == this.selected_service){
                    this.selected_services.push(this.services[i])
                    break;
                }
            }
        },

        removeRow(index){
            this.selected_services.splice(index,1);
        },

        form_ddp_back(){
            this.steps.step_ddp = "circulo-inactive";
            this.hidde_all('form_mdc');
        },

        form_odc_back(){
            this.steps.step_odc = "circulo-inactive";
            this.hidde_all('form_ddp');
        },

        delivery_schedule_file(event){
            this.client_operations.delivery_schedule_name = event.target.files[0].name;
            this.client_operations.delivery_schedule = event.target.files[0];
        },

        catalog_availability_file(event){
            this.product_data.file_message = event.target.files[0].name;
            this.product_data.catalog_availability = event.target.files[0];
        },

        dangerous_material_file(event){
            this.client_operations.dangerous_material_name = event.target.files[0].name;
            this.client_operations.dangerous_material = event.target.files[0];
        },

        submit: async function(event){

            var data = new FormData();

            data.append("client", JSON.stringify(this.client));
            data.append("director", JSON.stringify(this.director));
            data.append("manager", JSON.stringify(this.manager));
            data.append("foreign", JSON.stringify(this.foreign));
            data.append("operations", JSON.stringify(this.operations));
            data.append("invoices", JSON.stringify(this.invoices));
            data.append("product_data", JSON.stringify(this.product_data));
            data.append("selected_services", JSON.stringify(this.selected_services));
            data.append("client_operations", JSON.stringify(this.client_operations));

            data.append("catalog_availability", this.product_data.catalog_availability);

            data.append("delivery_schedule", this.client_operations.delivery_schedule);
            data.append("dangerous_material", this.client_operations.dangerous_material);

            this.button_text = "<i class='fas fa-spinner fa-pulse fa-lg mr-3'></i>CARGANDO...";

            axios.post(this.store_url, data, {
                headers: {
                    'Content-Type':'multipart/form-data'
                }
            }).then(response => {
                this.button_text = "GUARDAR";
                $('#success').modal('show');
            })
        }

    }
})

$('#success').on('hidden.bs.modal', function (e) {
  location.reload();
})
