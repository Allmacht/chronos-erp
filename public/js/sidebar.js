$(document).ready(function(){

    resize();
    $(window).resize(function(){
        resize();
    });
    function resize(){
        var screenWidth = $(window).width();
        if(screenWidth <= 800){
            $('#sidenav').css('left','-300px');
            $('body').css('transition','0.5s').css('margin-left','0');
        }else{
            $('#sidenav').css('left','0px');
            $('body').css('transition','0.5s').css('margin-left','300px');
        }
    }
    $('[data-toggle="tooltip"]').tooltip()

    $('.dropdown-btn').click(function(){
        $(this).toggleClass('side-active');
        var target = $(this).data('target');
        $(target).toggleClass('hide');

        var icon = $(this).find('.fa-caret-down');
        icon.toggleClass('transform-rot');
    });

    $('.button-menu').click(function(){

        var position = $('#sidenav').position();
        var screenWidth = $(window).width();

        if(position.left == 0){
            $('#sidenav').css('left','-300px');
            if(screenWidth >= 800){
                $('body').css('transition','0.5s').css('margin-left','0');
            }
        }else{
            $('#sidenav').css('left','0px');
            if(screenWidth >= 800){
                $('body').css('transition','0.5s').css('margin-left','300px');
            }
        }
    });
})
