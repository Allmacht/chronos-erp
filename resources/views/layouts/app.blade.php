<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title','Chronos')</title>

        <script src="{{asset('js/all.min.js')}}" charset="utf-8"></script>
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
        @auth
            <link rel="stylesheet" href="{{asset('css/layouts/auth.css')}}">
        @endauth
        @yield('styles')
    </head>
    <body>
        <div id="app">
            @guest

                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                    <a class="navbar-brand" href="#">
                        {{-- <img src="" alt=""> --}}
                        Chronos
                    </a>
                    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#initial-navbar">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="initial-navbar">
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                            <li class="nav-item @yield('login')">
                                <a href="{{route('login')}}" class="nav-link">
                                    <i class="fas fa-user mr-2"></i>
                                    {{__('Iniciar sesión')}}
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>

            @else
                <div id="sidenav" class="sidenav">
                    <div class="container-fluid">
                        <div class="row px-0">
                            <div class="col-12 text-right sticky-top">
                                <button type="button" class="btn btn-link button-menu">
                                    <i class="fas fa-times fa-lg icon-close"></i>
                                </button>
                            </div>
                            <div class="col-12 text-center sticky-top top-div">
                                <img src="{{asset('images/logo.png')}}" alt="" class="img-fluid" width="130px"><br>
                            </div>
                            <div class="col-12 form-row mt-3">
                                <div class="col-6 float-left">
                                    <p class="user-name ml-auto">
                                        {{Auth::user()->name}}
                                    </p>
                                </div>
                                <div class="col-6 form-row config">
                                    <a href="#" class="btn-link ml-auto my-auto">
                                        <i class="fas fa-cog fa-lg"></i>
                                    </a>
                                    <button type="button" class="btn btn-link" data-toggle="modal" data-target="#logout">
                                        <i class="fas fa-power-off fa-lg"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-12 mt-4 px-0 list-options">

                                <a href="#" class="side-link @yield('dashboard') text-decoration-none">
                                    <i class="far fa-object-group fa-lg mr-3"></i>
                                    {{__('DASHBOARD')}}
                                </a>
                                {{-- MENU INVENTARIO --}}
                                <a href="#" class="side-link dropdown-btn text-decoration-none" data-target="#dropdown-inv">
                                    <i class="far fa-caret-square-down fa-lg mr-3"></i>
                                    {{__('INVENTARIO')}}
                                    <i class="fa fa-caret-down ml-5"></i>
                                </a>
                                <div class="dropdown-container hide" id="dropdown-inv">
                                    <a href="{{route('consumables.index')}}" class="side-link link-dropdown @yield('consumibles') text-decoration-none">
                                        CONSUMIBLES
                                    </a>
                                    <a href="{{route('equipment.index')}}" class="side-link link-dropdown @yield('equipo') text-decoration-none">
                                        EQUIPO
                                    </a>
                                    <a href="{{route('loans.index')}}" class="side-link link-dropdown @yield('prestamos') text-decoration-none">
                                        PRÉSTAMOS
                                    </a>
                                    <a href="{{route('income_orders.index')}}" class="side-link link-dropdown @yield('orden_entrada') text-decoration-none">
                                        ORDENES DE ENTRADA
                                    </a>
                                </div>
                                {{-- MENU COMPRAS --}}
                                <a href="#" class="side-link dropdown-btn text-decoration-none" data-target="#dropdown-comp">
                                    <i class="fas fa-cart-plus fa-lg mr-3"></i>
                                    {{__('COMPRAS')}}
                                    <i class="fa fa-caret-down ml-5"></i>
                                </a>
                                <div class="dropdown-container hide" id="dropdown-comp">
                                    <a href="{{route('suppliers.index')}}" class="side-link link-dropdown @yield('proveedores') text-decoration-none">
                                        PROVEEDORES
                                    </a>
                                    <a href="{{route('purchase_orders.index')}}" class="side-link link-dropdown @yield('orden_compra') text-decoration-none">
                                        ÓRDENES DE COMPRA
                                    </a>
                                    <a href="{{route('expenditures.index')}}" class="side-link link-dropdown @yield('gastos') text-decoration-none">
                                        GASTOS
                                    </a>
                                </div>
                                {{-- MENU VENTAS --}}
                                <a href="#" class="side-link dropdown-btn text-decoration-none" data-target="#dropdown-vent">
                                    <i class="far fa-money-bill-alt fa-lg mr-3"></i>
                                    {{__('VENTAS')}}
                                    <i class="fa fa-caret-down ml-5"></i>
                                </a>
                                <div class="dropdown-container hide" id="dropdown-vent">
                                    <a href="{{route('handbills.index')}}" class="side-link link-dropdown @yield('prospectos') text-decoration-none">
                                        PROSPECTOS
                                    </a>
                                    <a href="{{route('clients.index')}}" class="side-link link-dropdown @yield('clientes') text-decoration-none">
                                        CLIENTES
                                    </a>
                                    <a href="{{route('services.index')}}" class="side-link link-dropdown @yield('servicios') text-decoration-none">
                                        SERVICIOS
                                    </a>
                                    <a href="{{route('quotations.index')}}" class="side-link link-dropdown @yield('cotizaciones') text-decoration-none">
                                        COTIZACIONES
                                    </a>
                                    <a href="{{route('referrals.index')}}" class="side-link link-dropdown @yield('remisiones') text-decoration-none">
                                        REMISIONES
                                    </a>
                                </div>
                                {{-- MENU REPORTES --}}
                                <a href="#" class="side-link dropdown-btn text-decoration-none" data-target="#dropdown-repo">
                                    <i class="fas fa-chart-line fa-lg mr-3"></i>
                                    {{__('REPORTES')}}
                                    <i class="fa fa-caret-down ml-5"></i>
                                </a>
                                <div class="dropdown-container hide" id="dropdown-repo">

                                </div>
                                {{-- MENU AJUSTES --}}
                                <a href="#" class="side-link dropdown-btn text-decoration-none" data-target="#dropdown-ajus">
                                    <i class="fas fa-sliders-h fa-lg mr-3"></i>
                                    {{__('AJUSTES')}}
                                    <i class="fa fa-caret-down ml-5"></i>
                                </a>
                                <div class="dropdown-container hide" id="dropdown-ajus">
                                    <a href="#" class="side-link link-dropdown @yield('cotizaciones') text-decoration-none">
                                        USUARIOS
                                    </a>
                                    <a href="#" class="side-link link-dropdown @yield('roles') text-decoration-none">
                                        ROLES
                                    </a>
                                    <a href="#" class="side-link link-dropdown @yield('divisas') text-decoration-none">
                                        DIVISAS
                                    </a>
                                    <a href="#" class="side-link link-dropdown @yield('configuracion') text-decoration-none">
                                        CONFIGURACIÓN
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endguest

            <main>
                @auth
                    <nav class="navbar navbar-dark bg-dark button-nav sticky-top">
                        <button type="button" class="btn btn-link navbar-brand button-menu">
                            <i class="fas fa-bars fa-lg"></i>
                        </button>
                        <span class="navbar-brand mr-auto">@yield('top-title')</span>
                    </nav>
                @endauth

                @yield('content')
            </main>

            @include('Notifications')
            @include('layouts.logout')

            <script src="{{asset('js/jquery-3.4.1.min.js')}}" charset="utf-8"></script>
            <script src="{{asset('js/popper.min.js')}}" charset="utf-8"></script>
            <script src="{{asset('js/bootstrap.min.js')}}" charset="utf-8"></script>
            <script src="{{asset('js/TweenMax.min.js')}}" charset="utf-8"></script>
            <script src="{{asset('js/notifications.js')}}" charset="utf-8"></script>

            @auth
            <script src="{{asset('js/sidebar.js')}}" charset="utf-8"></script>
            @endauth

            @yield('scripts')
        </div>
    </body>
</html>
