<div class="modal fade" id="logout" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-design">
            <div class="modal-header">
                <h5 class="modal-title">{{__('ATENCIÓN')}}</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h6 class="text-center">{{__('¿Realmente desea cerrar sesión?')}}</h6>
            </div>
            <div class="modal-footer">
                <form action="{{route('logout')}}" method="post">
                    @csrf
                    <button type="button" class="btn btn-link btn-modal-cancelar" data-dismiss="modal">
                        {{__('CANCELAR')}}
                    </button>
                    <button type="submit" class="btn btn-danger rounded-0 btn-modal-aceptar">
                        {{__('ACEPTAR')}}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
