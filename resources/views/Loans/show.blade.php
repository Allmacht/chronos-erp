@extends('layouts.app')
@section('title','CHRONOS - PRÉSTAMOS')
@section('prestamos','side-active')
@section('top-title','PRÉSTAMOS')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/show.css')}}">
@endsection
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h4 class="title">{{__('MOSTRAR PRÉSTAMO')}}</h4>
            </div>
            <div class="col-10 mx-auto mb-4 table-responsive mt-3">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th>{{__('CLAVE')}}</th>
                            <th>{{__('VALOR')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><b>{{__('Equipo')}}</b></td>
                            <td>{{$loan->equipment->sku}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Fecha de préstamo')}}</b></td>
                            <td>{{$loan->initial_date}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Fecha de retorno')}}</b></td>
                            <td>{{$loan->return_date}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Solicitante')}}</b></td>
                            <td>{{$loan->applicant}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Facilitador')}}</b></td>
                            <td>{{$loan->user->name}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Observaciones')}}</b></td>
                            <td>{{$loan->observations}}</td>
                        </tr>
                    </tbody>
                </table>
                <div class="col-12 text-center">
                    <a href="{{route('loans.index')}}" class="btn back-button rounded-0">{{__('ATRÁS')}}</a>
                </div>
            </div>
        </div>
    </div>

@endsection
