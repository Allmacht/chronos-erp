@extends('layouts.app')
@section('title','CHRONOS - PRESTAMOS')
@section('prestamos','side-active')
@section('top-title','PRESTAMOS')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h4 class="title"><strong>{{__('EDITAR PRÉSTAMO')}}</strong></h4>
            </div>
            <form class="col-12" action="{{route('loans.update',['id' => $loan->id])}}" method="post">
                @csrf
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <select class="form-control create-input select-input" name="equipment_id" required>
                        <option value="{{$loan->equipment_id}}" selected>{{$loan->equipment->sku}}</option>
                        @foreach ($equipments as $equipment)
                            <option value="{{$equipment->id}}">{{$equipment->sku}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="initial_date" class="form-control create-input @error('initial_date') is-invalid @endif" value="{{$loan->initial_date}}" placeholder="Fecha de préstamo*" onfocus="(this.type='date')" onblur="(this.type='text')" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="return_date" class="form-control create-input @error('return_date') is-invalid @endif" value="{{$loan->return_date}}" placeholder="Fecha de retorno*" onfocus="(this.type='date')" onblur="(this.type='text')" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="applicant" class="form-control create-input @error('applicant') is-invalid @endif" value="{{$loan->applicant}}" placeholder="Solicitante*" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="observations" class="form-control create-input @error('observations') is-invalid @endif" value="{{$loan->observations}}" placeholder="Observaciones">
                </div>

                <div class="col-xl-6 mx-auto text-right buttons">
                    <a href="{{route('loans.index')}}" class="btn btn-link">{{__('CANCELAR')}}</a>
                    <button type="submit" class="btn submit-button py-2 px-5 rounded-0">{{__('ACTUALIZAR')}}</button>
                </div>
            </form>
        </div>
    </div>

@endsection
