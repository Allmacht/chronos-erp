@extends('layouts.app')
@section('title','CHRONOS - PRÉSTAMOS')
@section('prestamos','side-active')
@section('top-title','PRÉSTAMOS')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
@endsection
@section('content')

        <div class="container-fluid index-container">
            <div class="row">

                <div class="col-12 form-row">
                    <div class="col-xl-6">
                        <form action="" method="get">
                            <div class="input-group col-xl-8 mb-3">
                              <input type="text" class="form-control search-input rounded-0" name="search" value="{{$search}}" placeholder="BUSCAR" >
                              <div class="input-group-append">
                                <button class="btn btn-outline-secondary btn-search rounded-0" type="submit" >
                                    <i class="fas fa-search"></i>
                                </button>
                              </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-xl-6 text-right float-right">
                        <a href="{{route('loans.create')}}" class="btn index-button rounded-0 text-truncate"><strong>{{__('REGISTRAR PRÉSTAMO')}}</strong></a>
                    </div>
                </div>

                @if($loans->count())
                    <div class="col-12 table-responsive px-4 mt-4">
                        <table class="table">
                            <thead>
                                <tr class="text-center">
                                    <th>{{__('EQUIPO')}}</th>
                                    <th>{{__('FECHA')}}</th>
                                    <th>{{__('SOLICITANTE')}}</th>
                                    <th>{{__('FACILITADOR')}}</th>
                                    <th>{{__('ESTATUS')}}</th>
                                    <th>{{__('ACCIONES')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($loans as $loan)
                                    <tr class="text-center">
                                        <td class="align-middle text-truncate">{{$loan->equipment->sku}}</td>
                                        <td class="align-middle text-truncate">{{$loan->initial_date}}</td>
                                        <td class="align-middle text-truncate">{{$loan->applicant}}</td>
                                        <td class="align-middle text-truncate">{{$loan->user->name}}</td>
                                        <td class="align-middle text-truncate">{{$loan->status ? 'Activa' : 'Inactiva'}}</td>
                                        <td class="align-middle text-truncate">
                                            <a href="{{route('loans.show',['id' => $loan->id])}}" class="px-2" data-toggle="tooltip" data-placement="left" data-title="Mostrar">
                                                <i class="fas fa-clipboard-list icon-table"></i>
                                            </a>
                                            <a href="{{route('loans.edit',['id' => $loan->id])}}" class="px-2" data-toggle="tooltip" data-placement="top" data-title="Editar">
                                                <i class="fas fa-pencil-alt icon-table"></i>
                                            </a>
                                            <span class="open-modal" data-toggle="modal" data-target="#delete" data-action="delete" data-id="{{$loan->id}}">
                                                <button type="button" class="btn btn-link px-2" data-toggle="tooltip" data-placement="right" data-title="Eliminar">
                                                    <i class="fas fa-trash icon-table"></i>
                                                </button>
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="col-12">
                            <div class="col-md-8 col-sm-12 float-left form-inline select-count">

                            </div>
                            <div class="col-md-4 col-sm-12 float-right">
                                {{ $loans->appends(['search' => $search])->links() }}
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-12 text-center mt-5 sin-registros">
                        <i class="fas fa-exclamation-triangle fa-4x mb-2"></i>
                        <h4>{{__('SIN REGISTROS')}}</h4>
                    </div>
                @endif
            </div>
        </div>

        @include('Loans.delete');
@endsection
@section('scripts')
    <script src="{{asset('js/getID.js')}}" charset="utf-8"></script>
@endsection
