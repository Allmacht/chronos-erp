@extends('layouts.app')
@section('title','CHRONOS - PROSPECTOS')
@section('prospectos','side-active')
@section('top-title','PROSPECTOS')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h4 class="title"><strong>{{__('ACTUALIZAR PROSPECTO')}}</strong></h4>
            </div>
            <form class="col-12" action="{{route('handbills.update',['id' => $handbill->id])}}" method="post">
                @csrf
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="business" class="form-control create-input @error('business') is-invalid @enderror" value="{{$handbill->business}}" placeholder="Giro*" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="name" class="form-control create-input @error('business') is-invalid @enderror" value="{{$handbill->name}}" placeholder="Nombre comercial*" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="number" name="phone" class="form-control create-input @error('phone') is-invalid @enderror" value="{{$handbill->phone}}" placeholder="Teléfono*" min="1" max="9999999999" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="email" name="email" class="form-control create-input @error('email') is-invalid @enderror" value="{{$handbill->email}}" placeholder="Correo electrónico*" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="contact" class="form-control create-input @error('contact') is-invalid @enderror" value="{{$handbill->contact}}" placeholder="Contacto*" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="position" class="form-control create-input @error('position') is-invalid @enderror" value="{{$handbill->position}}" placeholder="Cargo*" required>
                </div>
                <div class="col-xl-6 mx-auto text-right buttons">
                    <a href="{{route('handbills.index')}}" class="btn btn-link">{{__('CANCELAR')}}</a>
                    <button type="submit" class="btn submit-button py-2 px-5 rounded-0">{{__('ACTUALIZAR')}}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
