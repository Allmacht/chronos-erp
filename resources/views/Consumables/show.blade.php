@extends('layouts.app')
@section('title','CHRONOS - CONSUMIBLES')
@section('consumibles','side-active')
@section('top-title','CONSUMIBLES')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/show.css')}}">
@endsection
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h4 class="title">{{$consumable->sku}}</h4>
            </div>
            <div class="col-md-10 mx-auto mb-4 table-responsive mt-3">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th>{{__('Clave')}}</th>
                            <th>{{__('Valor')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><b>{{__('SKU')}}</b></td>
                            <td>{{$consumable->sku}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Descripción')}}</b></td>
                            <td>{{$consumable->description}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Unidad de medida')}}</b></td>
                            <td>{{$consumable->unit_measurement}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Tipo')}}</b></td>
                            <td>{{$consumable->type}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Precio inicial')}}</b></td>
                            <td>{{$consumable->initial_cost}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Política de disponibilidad')}}</b></td>
                            <td>{{$consumable->availability_policy}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Disponible')}}</b></td>
                            <td>{{$consumable->available}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Marca')}}</b></td>
                            <td>{{$consumable->brand}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Modelo')}}</b></td>
                            <td>{{$consumable->model}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Proveedor')}}</b></td>
                            <td>{{$consumable->supplier->business_name}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Departamento')}}</b></td>
                            <td>{{$consumable->department->name}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Fecha de registro')}}</b></td>
                            <td>{{$consumable->created_at}}</td>
                        </tr>
                    </tbody>
                </table>
                <div class="text-center">
                    <a href="{{route('consumables.index')}}" class="btn back-button rounded-0"><strong>ATRÁS</strong> </a>
                </div>
            </div>
        </div>
    </div>

@endsection
