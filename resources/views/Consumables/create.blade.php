@extends('layouts.app')
@section('title','CHRONOS - CONSUMIBLES')
@section('consumibles','side-active')
@section('top-title','CONSUMIBLES')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h4 class="title"><strong>{{__('REGISTRAR CONSUMIBLE')}}</strong></h4>
            </div>
            <form class="col-12" action="{{route('consumables.store')}}" method="post">
                @csrf
                <div class="col-xl-6 mx-auto mb-3">
                    <input type="text" name="sku" class="form-control create-input @error('sku') is-invalid @endif" value="{{old('sku')}}" required placeholder="SKU*">
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <input type="text" name="description" class="form-control create-input @error('description') is-invalid @endif" value="{{old('description')}}" required placeholder="Descripción*">
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <select class="form-control create-input select-input" name="unit_measurement" required>
                        <option value="" selected disabled>{{__('Unidad de medida*')}}</option>
                        <option value="Kilo">{{__('Kilo')}}</option>
                        <option value="Gramo">{{__('Gramo')}}</option>
                        <option value="Metro lineal">{{__('Metro lineal')}}</option>
                        <option value="Metro cuadrado">{{__('Metro cuadrado')}}</option>
                        <option value="Pieza">{{__('Pieza')}}</option>
                        <option value="Cabeza">{{__('Cabeza')}}</option>
                        <option value="Litro">{{__('Litro')}}</option>
                        <option value="Par">{{__('Par')}}</option>
                        <option value="Kilowatt">{{__('Kilowatt')}}</option>
                        <option value="MIllar">{{__('Millar')}}</option>
                        <option value="Juego">{{__('Juego')}}</option>
                        <option value="Kilowatt/Hora">{{__('Kilowatt/Hora')}}</option>
                        <option value="Tonelada">{{__('Tonelada')}}</option>
                        <option value="Barril">{{__('Barril')}}</option>
                        <option value="Gramo neto">{{__('Gramo neto')}}</option>
                        <option value="Decenas">{{__('Decenas')}}</option>
                        <option value="Cientos">{{__('Cientos')}}</option>
                        <option value="Docenas">{{__('Docenas')}}</option>
                        <option value="Caja">{{__('Caja')}}</option>
                        <option value="Botella">{{__('Botella')}}</option>
                    </select>
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    {{-- REQUERIDO --}}
                    <select class="form-control create-input select-input" name="department_id" required>
                        <option value="" selected disabled>{{__('Departamento responsble*')}}</option>
                        @foreach ($departments as $department)
                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <input type="text" name="type" class="form-control create-input @error('type') is-invalid @endif" value="{{old('type')}}" required placeholder="Tipo*">
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <input type="number" name="initial_cost" class="form-control create-input @error('initial_cost') is-invalid @endif" value="{{old('initial_cost')}}" min="0" step="0.01" required placeholder="Costo inicial*">
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <input type="text" name="availability_policy" class="form-control create-input @error('availability_policy') is-invalid @endif" value="{{old('availability_policy')}}" required placeholder="Política de disponibilidad*">
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <select class="form-control create-input select-input" name="supplier_id" required>
                        <option value="" selected disabled>{{__('Proveedor*')}}</option>
                        @foreach ($suppliers as $supplier)
                            <option value="{{$supplier->id}}">{{$supplier->business_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <input type="text" name="brand" class="form-control create-input @error('brand') is-invalid @endif" value="{{old('brand')}}" required placeholder="Marca*">
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <input type="text" name="model" class="form-control create-input @error('model') is-invalid @endif" value="{{old('model')}}" required placeholder="Modelo*">
                </div>
                <div class="col-xl-6 mx-auto text-right buttons">
                    <a href="{{route('consumables.index')}}" class="btn btn-link">{{__('CANCELAR')}}</a>
                    <button type="submit" class="btn submit-button py-2 px-5 rounded-0">{{__('CREAR')}}</button>
                </div>
            </form>
        </div>
    </div>

@endsection
