@extends('layouts.app')
@section('title','CHRONOS - CONSUMIBLES')
@section('consumibles','side-active')
@section('top-title','CONSUMIBLES')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <div class="container">
        <div class="row px-4">
            <div class="col-12 text-center mb-4">
                <h4 class="title">{{__('IMPORTAR CONSUMIBLES')}}</h4>
            </div>
            <form class="col-12 px-0" action="{{route('consumables.import_store')}}" id="consumables-form" method="post" enctype="multipart/form-data">
                @csrf
                <div class="custom-file px-0">
                  <input type="file" name="file" class="custom-file-input" id="file" required>
                  <label class="custom-file-label" id="label-file" for="customFile">Elegir archivo</label>
                </div>
            </form>
            <div class="col-12 clearfix buttons text-md-left px-0">
                <div class="col-md-6 float-left px-0 mb-3">
                    <a href="{{asset('templates/consumables_template.csv')}}" class="btn submit-button rounded-0">
                        <i class="fas fa-file-download mr-2"></i>
                        {{__('DESCARGAR PLANTILLA')}}
                    </a>
                </div>
                <div class="col-md-6 float-right text-right px-0">
                    <a href="{{route('consumables.index')}}" class="btn btn-link">CANCELAR</a>
                    <button id="submit-button" class="btn submit-button rounded-0 ml-auto px-5">
                        <i class="fas fa-file-import mr-2"></i>
                        {{__('IMPORTAR')}}
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">

        $('#file').change(function(e){
            $('#label-file').empty().append(e.target.files[0].name);
        })

        $('#submit-button').click(function(){
            $(this).empty().append("<i class='fas fa-spinner fa-pulse mr-2'></i> IMPORTANDO...");
            $('#consumables-form').submit();
        })

    </script>
@endsection
