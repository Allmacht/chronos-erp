@extends('layouts.app')
@section('title','CHRONOS - CONSUMIBLES')
@section('consumibles','side-active')
@section('top-title','CONSUMIBLES')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h4 class="title"><strong>{{$consumable->sku}}</strong></h4>
            </div>
            <form class="col-12" action="{{route('consumables.update',['id' => $consumable->id])}}" method="post">
                @csrf
                <div class="col-xl-6 mx-auto mb-3">
                    <input type="text" name="sku" class="form-control create-input @error('sku') is-invalid @endif" value="{{$consumable->sku}}" required placeholder="SKU*">
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <input type="text" name="description" class="form-control create-input @error('description') is-invalid @endif" value="{{$consumable->description}}" required placeholder="Descripción*">
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <select class="form-control create-input select-input" name="unit_measurement" required>
                        <option value="" selected disabled>{{__('Unidad de medida*')}}</option>
                        <option @if($consumable->unit_measurement == 'Kilo')           selected @endif value="Kilo">{{__('Kilo')}}</option>
                        <option @if($consumable->unit_measurement == 'Gramo')          selected @endif value="Gramo">{{__('Gramo')}}</option>
                        <option @if($consumable->unit_measurement == 'Metro lineal')   selected @endif value="Metro lineal">{{__('Metro lineal')}}</option>
                        <option @if($consumable->unit_measurement == 'Metro cuadrado') selected @endif value="Metro cuadrado">{{__('Metro cuadrado')}}</option>
                        <option @if($consumable->unit_measurement == 'Pieza')          selected @endif value="Pieza">{{__('Pieza')}}</option>
                        <option @if($consumable->unit_measurement == 'Cabeza')         selected @endif value="Cabeza">{{__('Cabeza')}}</option>
                        <option @if($consumable->unit_measurement == 'Litro')          selected @endif value="Litro">{{__('Litro')}}</option>
                        <option @if($consumable->unit_measurement == 'Par')            selected @endif value="Par">{{__('Par')}}</option>
                        <option @if($consumable->unit_measurement == 'Kilowatt')       selected @endif value="Kilowatt">{{__('Kilowatt')}}</option>
                        <option @if($consumable->unit_measurement == 'Millar')         selected @endif value="Millar">{{__('Millar')}}</option>
                        <option @if($consumable->unit_measurement == 'Juego')          selected @endif value="Juego">{{__('Juego')}}</option>
                        <option @if($consumable->unit_measurement == 'Kilowatt/Hora')  selected @endif value="Kilowatt/Hora">{{__('Kilowatt/Hora')}}</option>
                        <option @if($consumable->unit_measurement == 'Tonelada')       selected @endif value="Tonelada">{{__('Tonelada')}}</option>
                        <option @if($consumable->unit_measurement == 'Barril')         selected @endif value="Barril">{{__('Barril')}}</option>
                        <option @if($consumable->unit_measurement == 'Gramo neto')     selected @endif value="Gramo neto">{{__('Gramo neto')}}</option>
                        <option @if($consumable->unit_measurement == 'Decenas')        selected @endif value="Decenas">{{__('Decenas')}}</option>
                        <option @if($consumable->unit_measurement == 'Cientos')        selected @endif value="Cientos">{{__('Cientos')}}</option>
                        <option @if($consumable->unit_measurement == 'Docenas')        selected @endif value="Docenas">{{__('Docenas')}}</option>
                        <option @if($consumable->unit_measurement == 'Caja')           selected @endif value="Caja">{{__('Caja')}}</option>
                        <option @if($consumable->unit_measurement == 'Botella')        selected @endif value="Botella">{{__('Botella')}}</option>
                    </select>
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    {{-- REQUERIDO --}}
                    <select class="form-control create-input select-input" name="department_id">
                        <option value="" selected disabled>{{__('Departamento responsble*')}}</option>
                        @foreach ($departments as $department)
                            <option value="{{ $department->id }}" @if($consumable->department_id == $department->id) selected @endif>{{ $department->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <input type="text" name="type" class="form-control create-input @error('type') is-invalid @endif" value="{{$consumable->type}}" required placeholder="Tipo*">
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <input type="number" name="initial_cost" class="form-control create-input @error('initial_cost') is-invalid @endif" value="{{$consumable->initial_cost}}" min="0" step="0.01" required placeholder="Costo inicial*">
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <input type="text" name="availability_policy" class="form-control create-input @error('availability_policy') is-invalid @endif" value="{{$consumable->availability_policy}}" required placeholder="Política de disponibilidad*">
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <select class="form-control create-input select-input" name="supplier_id" required>
                        <option value="" selected disabled>{{__('Proveedor*')}}</option>
                        @foreach ($suppliers as $supplier)
                            <option value="{{$supplier->id}}" @if($consumable->supplier_id == $supplier->id) selected @endif>{{$supplier->business_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <input type="text" name="brand" class="form-control create-input @error('brand') is-invalid @endif" value="{{$consumable->brand}}" required placeholder="Marca*">
                </div>
                <div class="col-xl-6 mx-auto mb-3">
                    <input type="text" name="model" class="form-control create-input @error('model') is-invalid @endif" value="{{$consumable->model}}" required placeholder="Modelo*">
                </div>
                <div class="col-xl-6 mx-auto text-right buttons">
                    <a href="{{route('consumables.index')}}" class="btn btn-link">{{__('CANCELAR')}}</a>
                    <button type="submit" class="btn submit-button py-2 px-5 rounded-0">{{__('ACTUALIZAR')}}</button>
                </div>
            </form>
        </div>
    </div>

@endsection
