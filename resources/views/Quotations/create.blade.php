@extends('layouts.app')
@section('title','CHRONOS - COTIZACIONES')
@section('cotizaciones','side-active')
@section('top-title','COTIZACIONES')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <div class="container" id="quotations_create">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h4 class="title"><strong>{{__('CREAR COTIZACIÓN')}}</strong></h4>
            </div>
            <form class="col-12" action="" method="post" v-on:submit="submit($event)">
                @csrf
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" v-model="quotation.date" name="date" class="form-control create-input @error('date') is-invalid @enderror" value="{{old('date')}}" placeholder="Fecha*" onfocus="(this.type='date')" onblur="(this.type='text')" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <select v-model="quotation.client_id" class="form-control create-input select-input" name="client_id" required>
                        <option value="" disabled selected>{{__('Cliente*')}}</option>
                        @foreach ($clients as $client)
                            <option value="{{$client->id}}">{{$client->tradename}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" v-model="quotation.conditions" name="conditions" class="form-control create-input @error('conditions') is-invalid @enderror" value="{{old('conditions')}}" placeholder="Condiciones*" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" v-model="quotation.effect" name="effect" class="form-control create-input @error('effect') is-invalid @enderror" value="{{old('effect')}}" placeholder="Vigencia*" onfocus="(this.type='date')" onblur="(this.type='text')" required>
                </div>
                <div class="col-xl-6 mx-auto my-4">
                    <h6 class="title"><strong>{{__('AGREGAR SERVICIOS')}}</strong></h6>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <select v-model="selected_service" class="form-control create-input select-input" name="selected_service">
                        <option value="" selected disabled>{{__('Servicio*')}}</option>
                        <option v-for="service in services" :value="service.id">@{{service.name}}</option>
                    </select>
                    <div class="col-12 text-right mt-2 px-0">
                        <button type="button" v-on:click="add_service" :class="{disabled:add_button}" class="btn btn-link text-left px-0 text-decoration-none title add-button" id="add-button">
                            <strong>{{__('AGREGAR')}}</strong>
                        </button>
                    </div>
                </div>

                <div class="col-12 table-responsive px-4 mt-5" :class="{'table-hidden': table_show}">
                    <table class="table">
                        <thead>
                            <tr class="text-center">
                                <th>{{__('SERVICIO')}}</th>
                                <th>{{__('CANTIDAD')}}</th>
                                <th>{{__('IMPORTE')}}</th>
                                <th>{{__('SUBTOTAL')}}</th>
                                <th>{{__('')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="text-center" v-for="(service, index) in selected_services" :key="index" :item="service">
                                <td class="align-middle text-truncate">@{{service.name}}</td>
                                <td class="align-middle text-truncate">
                                    <input type="number" min="1" class="create-input text-center" :value="service.quantity" @keyup="add_quantity($event,index)" @change="add_quantity($event,index)">
                                </td>
                                <td class="align-middle text-truncate">$@{{service.price}}</td>
                                <td class="align-middle text-truncate">$@{{service.subtotal}}</td>
                                <td class="align-middle text-truncate">
                                    <button type="button" class="btn btn-link text-white" v-on:click="remove_service(index)">
                                        <i class="fas fa-minus-circle fa-lg"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-12 px-4" :class="{'table-hidden': table_show}">
                    <div class="col-6 float-left title px-0">
                        <h5><strong>{{__('TOTAL')}}</strong></h5>
                    </div>
                    <div class="col-6 float-right px-0 text-right title">
                        <h5><strong>$@{{quotation.amount}}</strong></h5>
                    </div>
                </div>

                <div class="col-xl-6 mx-auto text-right buttons mt-5">
                    <a href="{{route('quotations.index')}}" class="btn btn-link">{{__('CANCELAR')}}</a>
                    <button type="submit" class="btn submit-button py-2 px-5 rounded-0" v-html="button_submit">@{{button_submit}}</button>
                </div>
            </form>
        </div>
    </div>

    @include('Quotations.success')

@endsection
@section('scripts')
    <script src="{{asset('js/vue.js')}}" charset="utf-8"></script>
    <script src="{{asset('js/axios.js')}}" charset="utf-8"></script>
    <script src="{{asset('js/quotations.js')}}" charset="utf-8"></script>
@endsection
