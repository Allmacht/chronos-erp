<div class="modal fade" id="success" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-design">
            <div class="modal-header">
                <h5 class="title">{{__('ATENCIÓN')}}</h5>
                <button type="button" class="close" data-dismiss="modal" name="button">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <p>{{__('Cotización registrada correctamente')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger rounded-0 btn-modal-aceptar" data-dismiss="modal">
                    {{__('ACEPTAR')}}
                </button>
            </div>
        </div>
    </div>
</div>
