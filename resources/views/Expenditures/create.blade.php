@extends('layouts.app')
@section('title','CHRONOS - GASTOS')
@section('gastos','side-active')
@section('top-title','GASTOS')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h4 class="title"><strong>{{__('REGISTRAR GASTO')}}</strong></h4>
            </div>
            <form class="col-12" action="{{route('expenditures.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="date" class="create-input form-control @error('date') is-invalid @enderror" value="{{old('date')}}" placeholder="Fecha*" onfocus="(this.type='date')" onblur="(this.type='text')" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <select class="form-control create-input select-input" name="supplier_id">
                        <option value="" selected disabled>{{__('Proveedor*')}}</option>
                        @foreach ($suppliers as $supplier)
                            <option value="{{$supplier->id}}">{{$supplier->business_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="concept" class="form-control create-input @error('concept') is-invalid @enderror" value="{{old('concept')}}" placeholder="Concepto/Producto*" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="number" name="amount" min="1" step="0.1" class="form-control create-input @error('amount') is-invalid @enderror" value="{{old('amount')}}" placeholder="Importe*" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-5">
                    <div class="custom-file custom-file1 px-0">
                      <input type="file" name="file" class="custom-file-input" id="file">
                      <label class="custom-file-label" id="label-file" for="customFile">{{__('Anexos')}}</label>
                    </div>
                </div>
                <div class="col-xl-6 mx-auto text-right buttons">
                    <a href="{{route('expenditures.index')}}" class="btn btn-link">{{__('CANCELAR')}}</a>
                    <button type="submit" class="btn submit-button py-2 px-5 rounded-0">{{__('CREAR')}}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('#file').change(function(e){
            $('#label-file').empty().append(e.target.files[0].name);
        })
    </script>
@endsection
