@extends('layouts.app')
@section('title','CHRONOS - GASTOS')
@section('gastos','side-active')
@section('top-title','GASTOS')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/show.css')}}">
@endsection
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h4 class="title"><strong>{{$expenditure->folio}}</strong></h4>
            </div>
            <div class="col-10 mx-auto mb-4 table-responsive mt-3">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th>{{__('CLAVE')}}</th>
                            <th>{{__('VALOR')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><b>{{__('Folio')}}</b></td>
                            <td>{{$expenditure->folio}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Fecha')}}</b></td>
                            <td>{{$expenditure->date}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Proveedor')}}</b></td>
                            <td>{{$expenditure->supplier->business_name}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Usuario')}}</b></td>
                            <td>{{$expenditure->user->name}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Concepto')}}</b></td>
                            <td>{{$expenditure->concept}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Importe')}}</b></td>
                            <td>{{"$".$expenditure->amount}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Fecha de registro')}}</b></td>
                            <td>{{$expenditure->created_at}}</td>
                        </tr>
                        @if ($expenditure->attachment)
                            <tr>
                                <td class="align-middle"><b>{{__('Anexos')}}</b></td>
                                <td class="align-middle">
                                    <a class="btn btn-link title link text-decoration-none" href="{{asset('/expenditure_attachments/'.$expenditure->attachment)}}" data-toggle="tooltip" data-placement="right" data-title="archivo">
                                        <i class="fas fa-share-square"></i>
                                    </a>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                <div class="col-12 text-center">
                    <a href="{{route('expenditures.index')}}" class="btn back-button rounded-0">{{__('ATRÁS')}}</a>
                </div>
            </div>
        </div>
    </div>

@endsection
