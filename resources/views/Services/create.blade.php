@extends('layouts.app')
@section('title','CHRONOS - SERVICIOS')
@section('servicios','side-active')
@section('top-title','SERVICIOS')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h4 class="title"><strong>{{__('REGISTRAR SERVICIO')}}</strong></h4>
            </div>
            <form class="col-12" action="{{route('services.store')}}" method="post">
                @csrf
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="key" class="form-control create-input @error('key') is-invalid @enderror" value="{{old('key')}}" placeholder="Clave*" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="name" class="form-control create-input @error('name') is-invalid @enderror" value="{{old('name')}}" placeholder="Nombre*" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="modality" class="form-control create-input @error('modality') is-invalid @enderror" value="{{old('modality')}}" placeholder="Modalidad*" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <select class="form-control create-input select-input" name="unit">
                        <option value="" selected disabled>{{'Unidad*'}}</option>

                    </select>
                </div>
                <div class="col-xl-6 mx-auto my-4">
                    <h5 class="title">{{__('PRECIOS')}}</h5>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="number" name="regular_price" class="form-control create-input @error('regular_price') is-invalid @enderror" value="{{old('regular_price')}}" placeholder="Regular*" min="1" step="0.01" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="number" name="premium_price" class="form-control create-input @error('premium_price') is-invalid @enderror" value="{{old('premium_price')}}" placeholder="Premium*" min="1" step="0.01" required>
                    </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="number" name="partner_price" class="form-control create-input @error('partner_price') is-invalid @enderror" value="{{old('partner_price')}}" placeholder="Socio*" min="1" step="0.01" required>
                </div>
                <div class="col-xl-6 mx-auto text-right buttons">
                    <a href="{{route('services.index')}}" class="btn btn-link">{{__('CANCELAR')}}</a>
                    <button type="submit" class="btn submit-button py-2 px-5 rounded-0">{{__('CREAR')}}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
