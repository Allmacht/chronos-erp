@extends('layouts.app')
@section('title','CHRONOS - PROVEEDORES')
@section('proveedores','side-active')
@section('top-title','PROVEEDORES')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/show.css')}}">
@endsection
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h4 class="title">{{$supplier->business_name}}</h4>
            </div>
            <div class="col-10 mx-auto mb-4 table-responsive mt-3">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th>{{__('CLAVE')}}</th>
                            <th>{{__('VALOR')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><b>{{__('Razón social')}}</b></td>
                            <td>{{$supplier->business_name}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Nombre comercial')}}</b></td>
                            <td>{{$supplier->tradename}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('R.F.C.')}}</b></td>
                            <td>{{$supplier->rfc}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Días de crédito')}}</b></td>
                            <td>{{$supplier->credit_days ?? 'N/A'}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Monto del crédito')}}</b></td>
                            <td>{{$supplier->credit_amount ?? 'N/A'}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Correo electónico')}}</b></td>
                            <td>{{$supplier->email}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('País')}}</b></td>
                            <td>{{$supplier->country}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Estado')}}</b></td>
                            <td>{{$supplier->state}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Municipio')}}</b></td>
                            <td>{{$supplier->municipality}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Colonia')}}</b></td>
                            <td>{{$supplier->colony ?? 'N/A'}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Calle')}}</b></td>
                            <td>{{$supplier->street}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Número exterior')}}</b></td>
                            <td>{{$supplier->external_number}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Número interior')}}</b></td>
                            <td>{{$supplier->internal_number ?? 'N/A'}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Código postal')}}</b></td>
                            <td>{{$supplier->zipcode}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Teléfono')}}</b></td>
                            <td>{{$supplier->phone ?? 'N/A'}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Tipo de proveedor')}}</b></td>
                            <td>{{$supplier->type}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Nombre de contacto')}}</b></td>
                            <td>{{$supplier->contact_name ?? 'N/A'}}</td>
                        </tr>
                    </tbody>
                </table>
                <div class="text-center">
                    <a href="{{route('suppliers.index')}}" class="btn back-button rounded-0"><strong>ATRÁS</strong> </a>
                </div>
            </div>
        </div>
    </div>

@endsection
