@extends('layouts.app')
@section('title','CHRONOS - PROVEEDORES')
@section('proveedores','side-active')
@section('top-title','PROVEEDORES')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h4 class="title text-truncate"><strong>{{$supplier->id." - ".$supplier->business_name}}</strong></h4>
            </div>
            <form class="col-12" action="{{route('suppliers.update',['id' => $supplier->id])}}" method="post">
                @csrf
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="business_name" class="form-control create-input @error('business_name') is-invalid @enderror" placeholder="Razón social*" value="{{$supplier->business_name}}" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="tradename" class="form-control create-input @error('tradename') is-invalid @enderror" placeholder="Nombre comercial*" value="{{$supplier->tradename}}" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="rfc" class="form-control create-input @error('rfc') is-invalid @enderror" placeholder="R.F.C*" value="{{$supplier->rfc}}" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="credit_days" class="form-control create-input @error('credit_days') is-invalid @enderror" placeholder="Días de crédito" value="{{$supplier->credit_days}}">
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="credit_amount" class="form-control create-input @error('credit_amount') is-invalid @enderror" placeholder="Monto del crédito" value="{{$supplier->credit_amount}}">
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="email" name="email" class="form-control create-input @error('email') is-invalid @enderror" placeholder="Correo electónico" value="{{$supplier->email}}">
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <select class="form-control create-input select-input" name="country" id="country" required>
                        <option value="{{$supplier->country}}" selected>{{$supplier->country}}</option>
                    </select>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="state" class="form-control create-input @error('state') is-invalid @enderror" placeholder="Estado*" value="{{$supplier->state}}" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="municipality" class="form-control create-input @error('municipality') is-invalid @enderror" placeholder="Municipio*" value="{{$supplier->municipality}}" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="colony" class="form-control create-input @error('colony') is-invalid @enderror" placeholder="Colonia" value="{{$supplier->colony}}">
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="street" class="form-control create-input @error('street') is-invalid @enderror" placeholder="Calle" value="{{$supplier->street}}">
                </div>
                <div class="form-row col-xl-6 mx-auto mb-0">
                    <div class="form-group col-xl-6 px-0 mx-auto mb-3">
                        <input type="number" name="external_number" class="form-control create-input @error('external_number') is-invalid @enderror" placeholder="No. Exterior*" value="{{$supplier->external_number}}" required>
                    </div>
                    <div class="form-group col-xl-6 px-0 mx-auto mb-3">
                        <input type="text" name="internal_number" class="form-control create-input @error('internal_number') is-invalid @enderror" placeholder="No. Interior" value="{{$supplier->internal_number}}">
                    </div>
                    <div class="form-group col-xl-6 px-0 mx-auto mb-3">
                        <input type="number" name="zipcode" class="form-control create-input @error('zipcode') is-invalid @enderror" placeholder="Código postal*" value="{{$supplier->zipcode}}" required>
                    </div>
                    <div class="form-group col-xl-6 px-0 mx-auto mb-3">
                        <input type="number" name="phone" class="form-control create-input @error('phone') is-invalid @enderror" placeholder="Teléfono" value="{{$supplier->phone}}">
                    </div>
                    <div class="col-xl-6 px-1 text-left">
                        <p class="text-optional">Tipo de proveedor</p>
                    </div>
                    <div class="col-xl-6 px-0 custom-control text-center custom-switch">
                        <label for="" class="text-optional mr-5">Gastos</label>
                        <input type="checkbox" name="type" class="custom-control-input mx-5" id="type" @if($supplier->type == 'Compras') checked @endif>
                        <label for="type" class="custom-control-label text-optional">Compras</label>
                    </div>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-5">
                    <input type="text" name="contact_name" class="form-control create-input @error('contact_name') is-invalid @enderror" placeholder="Nombre de contacto" value="{{$supplier->contact_name}}">
                </div>
                <div class="col-xl-6 mx-auto text-right buttons">
                    <a href="{{route('suppliers.index')}}" class="btn btn-link cancel-button">
                        {{__('CANCELAR')}}
                    </a>
                    <button type="submit" class="btn submit-button rounded-0 py-2 px-5">
                        {{__('ACTUALIZAR')}}
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('scripts')
    <script type="text/javascript">

        $(document).ready(function(){
            $.get("https://restcountries.eu/rest/v2/all", function(data){
                for (var i = 0; i < data.length; i++) {
                    var country = data[i].translations.es;
                    $('#country').append("<option value='"+country+"'>"+country+"</option>");
                }
            });
        });

    </script>
@endsection
