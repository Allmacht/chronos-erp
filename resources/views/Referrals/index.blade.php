@extends('layouts.app')
@section('title','CHRONOS - REMISIONES')
@section('remisiones','side-active')
@section('top-title','REMISIONES')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
@endsection
@section('content')

    <div class="container-fluid index-container">
        <div class="row">

            <div class="col-12 form-row">
                <div class="col-xl-6">
                    <form action="" method="get">
                        <div class="input-group col-xl-8 mb-3">
                          <input type="text" class="form-control search-input rounded-0" name="search" value="{{$search}}" placeholder="BUSCAR" >
                          <div class="input-group-append">
                            <button class="btn btn-outline-secondary btn-search rounded-0" type="submit" >
                                <i class="fas fa-search"></i>
                            </button>
                          </div>
                        </div>
                    </form>
                </div>
                <div class="col-xl-6 text-right float-right">
                    <a href="{{route('referrals.create')}}" class="btn index-button rounded-0 text-truncate"><strong>{{__('CREAR REMISIÓN')}}</strong></a>
                </div>
            </div>

            @if($referrals->count())
                <div class="col-12 table-responsive px-4 mt-4">
                    <table class="table">
                        <thead>
                            <tr class="text-center">
                                <th>{{__('FOLIO')}}</th>
                                <th>{{__('FECHA')}}</th>
                                <th>{{__('CLIENTE')}}</th>
                                <th>{{__('OPERACIÓN')}}</th>
                                <th>{{__('IMPORTE')}}</th>
                                <th>{{__('ESTATUS')}}</th>
                                <th>{{__('ACCIONES')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($referrals as $referral)
                                <tr class="text-center">
                                    <td class="align-middle text-truncate">{{$referral->folio}}</td>
                                    <td class="align-middle text-truncate">{{$referral->date}}</td>
                                    <td class="align-middle text-truncate">{{$referral->tradename}}</td>
                                    <td class="align-middle text-truncate">{{$referral->transaction_type}}</td>
                                    <td class="align-middle text-truncate">{{"$".$referral->amount}}</td>
                                    <td class="align-middle text-truncate">
                                        @if($referral->paid)
                                            {{__('Pagada')}}
                                        @else
                                            {{__('Pago pendiente')}}
                                        @endif
                                    </td>
                                    <td class="align-middle text-truncate">
                                        <a href="" class="px-2" data-toggle="tooltip" data-placement="top" data-title="Editar">
                                            <i class="fas fa-pencil-alt icon-table"></i>
                                        </a>
                                        <span class="open-modal" data-toggle="modal" data-target="#delete" data-action="delete" data-id="{{$referral->id}}">
                                            <button type="button" class="btn btn-link px-2" data-toggle="tooltip" data-placement="right" data-title="Eliminar">
                                                <i class="fas fa-trash icon-table"></i>
                                            </button>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="col-12">
                        <div class="col-md-8 col-sm-12 float-left form-inline select-count">

                        </div>
                        <div class="col-md-4 col-sm-12 float-right">
                            {{ $referrals->appends(['search' => $search])->links() }
                        </div>
                    </div>
                </div>
            @else
                <div class="col-12 text-center mt-5 sin-registros">
                    <i class="fas fa-exclamation-triangle fa-4x mb-2"></i>
                    <h4>{{__('SIN REGISTROS')}}</h4>
                </div>
            @endif
        </div>
    </div>

@endsection
