@extends('layouts.app')
@section('title','CHRONOS - REMISIONES')
@section('remisiones','side-active')
@section('top-title','REMISIONES')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <div class="container-fluid" id="referrals">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h4 class="title"><strong>{{__('CREAR REMISIÓN')}}</strong></h4>
            </div>

            <form class="col-12" action="" method="post" v-on:submit="submit($event)" enctype="multipart/form-data">
                @csrf
                <div class="col-12 clearfix px-0">
                    <div class="col-xl-5 float-left">
                        <div class="col-12 mb-3">
                            <h6 class="title"><strong>{{__('SERVICIOS')}}</strong></h6>
                        </div>
                        <div class="col-12 form-row text-white mb-4">
                            <div class="form-check form-check-inline">
                              <input v-model="previous_quote" class="form-check-input" type="radio" name="quotation" id="quotation1" value="true">
                              <label class="form-check-label" for="quotation1">
                                  {{__('Cotización existente')}}
                              </label>
                            </div>
                            <div class="form-check form-check-inline ml-auto">
                              <input v-model="previous_quote" class="form-check-input" type="radio" name="quotation" id="quotation2" value="false">
                              <label class="form-check-label" for="quotation2">
                                  {{__('Sin Cotización prevía')}}
                              </label>
                            </div>
                        </div>
                        <div class="col-xl-12 form-group mb-4">
                            <input type="text" v-model="referral.date" name="date" class="form-control create-input" placeholder="Fecha*" value="{{old('date')}}" onfocus="(this.type='date')" onblur="(this.type='text')" required>
                        </div>
                        <div class="form-group col-xl-12 mb-4" :class="{'table-hidden':quotation_select}">
                            <select v-model="quotation" class="form-control create-input select-input" name="quotation_id">
                                <option value="" disabled selected>{{__('Cotización')}}</option>
                                @foreach ($quotations as $quotation)
                                    <option value="{{$quotation->id}}">{{$quotation->folio}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xl-12 mb-4">
                            <select v-model="referral.client_id" class="form-control create-input select-input" name="client_id">
                                <option value="" disable selected>{{__('Cliente*')}}</option>
                                @foreach ($clients as $client)
                                    <option value="{{$client->id}}">{{$client->tradename}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12 form-row text-white mb-4 mx-1">
                            <label class="mb-0">{{__('Tipo de transacción*')}}</label>
                            <div class="form-check form-check-inline ml-auto">
                              <input v-model="referral.transaction_type" class="form-check-input" type="radio" name="quotation" id="transaction_type1" value="Nacional">
                              <label class="form-check-label" for="transaction_type1">
                                  {{__('Nacional')}}
                              </label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input v-model="referral.transaction_type" class="form-check-input" type="radio" name="quotation" id="transaction_type2" value="Internacional">
                              <label class="form-check-label" for="transaction_type2">
                                  {{__('Internacional')}}
                              </label>
                            </div>
                        </div>
                        <div class="col-12 form-row text-white mb-4 mx-1">
                            <label class="mb-0">{{__('Tipo de movimiento en almacén*')}}</label>
                            <div class="form-check form-check-inline ml-auto">
                              <input v-model="referral.movement_type" class="form-check-input" type="radio" name="movement_type" id="movement_type1" value="Entrada">
                              <label class="form-check-label" for="movement_type1">
                                  {{__('Entrada')}}
                              </label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input  v-model="referral.movement_type" class="form-check-input" type="radio" name="movement_type" id="movement_type2" value="Salida">
                              <label class="form-check-label" for="movement_type2">
                                  {{__('Salida')}}
                              </label>
                            </div>
                        </div>
                        {{-- APIS --}}


                        <div class="form-group col-xl-12 mb-4">
                            <select v-model="referral.format" class="form-control create-input select-input" name="format">
                                <option value="" disable selected>{{__('Formato de chronos*')}}</option>
                                <option v-for="format in formats" :value="format.id">@{{format.id}}</option>
                            </select>
                        </div>
                        <div class="form-group col-xl-12 mb-4">
                            <select v-model="referral.customs_declaration" class="form-control create-input select-input" name="customs_declaration">
                                <option value="" disable selected>{{__('Pedimento*')}}</option>
                                <option v-for="customs_declaration in customs_declarations" :value="customs_declaration.id">@{{customs_declaration.id}}</option>
                            </select>
                        </div>

                        {{-- APIS --}}
                        <div class="col-12 form-row text-white mb-4 mx-1">
                            <label class="mb-0">{{__('Tipo de aviso')}}</label>
                            <div class="form-check form-check-inline ml-auto">
                              <input v-model="referral.notice_type" class="form-check-input" type="radio" name="notice_type" id="notice_type1" value="Numeral">
                              <label class="form-check-label" for="notice_type1">
                                  {{__('Numeral')}}
                              </label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input v-model="referral.notice_type" class="form-check-input" type="radio" name="notice_type" id="notice_type2" value="Final">
                              <label class="form-check-label" for="notice_type2">
                                  {{__('Final')}}
                              </label>
                            </div>
                        </div>
                        <div class="form-group col-12 mb-4">
                            <input v-model="referral.notice" type="text" name="notice" class="form-control create-input" placeholder="Aviso" value="{{old('notice')}}">
                        </div>
                        <div class="form-group col-12 mb-4">
                            <input v-model="referral.license_plate" type="text" name="license_plate" class="form-control create-input" placeholder="Placa*" value="{{old('license_plate')}}">
                        </div>
                        <div class="form-group col-12 mb-4">
                            <input v-model="referral.container" type="text" name="container" class="form-control create-input" placeholder="Contenedor*" value="{{old('container')}}">
                        </div>
                        {{-- file --}}

                        <div class="form-group col-xl-12 mx-auto mb-4">
                            <div class="custom-file custom-file1 px-0">
                              <input type="file" name="invoice" v-on:change="invoice($event)" class="custom-file-input" id="invoice">
                              <label class="custom-file-label" id="label-file" for="invoice">@{{referral.invoice_name}}</label>
                            </div>
                        </div>

                        {{-- file --}}
                    </div>

                    <div class="col-xl-7 float-right">
                        <div class="col-12 mb-3">
                            <h6 class="title"><strong>{{__('SERVICIOS')}}</strong></h6>
                        </div>
                        <div class="form-group col-12 mb-3" :class="{'table-hidden':!quotation_select}">
                            <select v-model="selected_service" class="form-control create-input select-input" name="service">
                                <option value="" selected disabled>{{__('Servicio*')}}</option>
                                <option v-for="service in services" :value="service.id">@{{service.name}}</option>
                            </select>
                        </div>
                        <div class="col-12 text-right" :class="{'table-hidden':!quotation_select}">
                            <button type="button" v-on:click="add_service" class="btn btn-link text-left px-0 text-decoration-none title add-button" id="add-button">
                                <strong>{{__('AGREGAR')}}</strong>
                            </button>
                        </div>

                        <div class="col-12 table-responsive mt-3">
                            <table class="table">
                                <thead>
                                    <tr class="text-center">
                                        <th>{{__('SERVICIO')}}</th>
                                        <th>{{__('CANTIDAD')}}</th>
                                        <th>{{__('IMPORTE')}}</th>
                                        <th>{{__('SUBTOTAL')}}</th>
                                        <th>{{__('')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center" v-for="(detail, index) in quotation_details" :index="index" :item="detail">
                                        <td class="align-middle text-truncate">@{{detail.name}}</td>
                                        <td class="align-middle text-truncate">
                                            <input type="number" name="quantity" :readonly="readonly" min="1" v-on:keyup="add_quantity($event,index)" v-on:change="add_quantity($event,index)" class="create-input text-center" :value="detail.quantity">
                                        </td>
                                        <td class="align-middle text-truncate">$@{{detail.price}}</td>
                                        <td class="align-middle text-truncate">$@{{detail.subtotal}}</td>
                                        <td class="align-middle text-truncate">
                                            <button type="button" class="btn btn-link text-white" :class="{'table-hidden':!quotation_select}" v-on:click="remove_service(index)">
                                                <i class="fas fa-minus-circle fa-lg"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-12 px-4" >
                            <div class="col-6 float-left title px-0">
                                <h5><strong>{{__('TOTAL')}}</strong></h5>
                            </div>
                            <div class="col-6 float-right px-0 text-right title">
                                <h5><strong>$@{{total}}</strong></h5>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-12">
                    <div class="mx-auto text-right buttons">
                        <a href="{{route('referrals.index')}}" class="btn btn-link">{{__('CANCELAR')}}</a>
                        <button type="submit" class="btn submit-button py-2 px-5 rounded-0">{{__('CREAR')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @include('Referrals.success')

@endsection
@section('scripts')
    <script src="{{asset('js/vue.js')}}" charset="utf-8"></script>
    <script src="{{asset('js/axios.js')}}" charset="utf-8"></script>
    <script src="{{asset('js/referrals.js')}}" charset="utf-8"></script>
@endsection
