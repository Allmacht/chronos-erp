@extends('layouts.app')
@section('title','CHRONOS - ÓRDENES DE COMPRA')
@section('orden_compra','side-active')
@section('top-title','ÓRDENES DE COMPRA')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <div class="container-fluid index-container">
        <div class="row">

            <div class="col-12 form-row">
                <div class="col-xl-6">
                    <form action="" method="get">
                        <div class="input-group col-xl-8 mb-3">
                          <input type="text" class="form-control search-input rounded-0" name="search" value="{{$search}}" placeholder="BUSCAR" >
                          <div class="input-group-append">
                            <button class="btn btn-outline-secondary btn-search rounded-0" type="submit" >
                                <i class="fas fa-search"></i>
                            </button>
                          </div>
                        </div>
                    </form>
                </div>
                <div class="col-xl-6 text-right float-right">
                    <a href="{{route('purchase_orders.create')}}" class="btn index-button rounded-0 text-truncate"><strong>{{__('REGISTRAR OC')}}</strong></a>
                </div>
            </div>

            @if($orders->count())
                <div class="col-12 table-responsive px-4 mt-4">
                    <table class="table">
                        <thead>
                            <tr class="text-center">
                                <th>{{__('FOLIO')}}</th>
                                <th>{{__('FECHA')}}</th>
                                <th>{{__('PROVEEDOR')}}</th>
                                <th>{{__('IMPORTE')}}</th>
                                <th>{{__('ESTATUS')}}</th>
                                <th>{{__('PAGO')}}</th>
                                <th>{{__('ACCIONES')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $order)
                                <tr class="text-center">
                                    <td class="align-middle text-truncate">{{$order->folio}}</td>
                                    <td class="align-middle text-truncate">{{$order->date}}</td>
                                    <td class="align-middle text-truncate">{{$order->supplier->business_name}}</td>
                                    <td class="align-middle text-truncate">{{"$".$order->amount}}</td>
                                    <td class="align-middle text-truncate">{{$order->approved ? 'Aprobada' : 'Sin aprobar'}}</td>
                                    <td class="align-middle text-truncate">{{$order->paid ? 'Pagada' : 'Sin pagar'}}</td>
                                    <td class="align-middle text-truncate">
                                        <a href="{{route('purchase_orders.show', ['id' => $order->id])}}" class="btn btn-link" data-toggle="tooltip" data-placement="left" data-title="Mostrar">
                                            <i class="fas fa-clipboard-list icon-table"></i>
                                        </a>
                                        @if(!$order->approved)
                                            <a href="{{route('purchase_orders.edit', ['id' => $order->id])}}" class="btn btn-link" data-toggle="tooltip" data-placement="top" data-title="Editar">
                                                <i class="fas fa-pencil-alt icon-table"></i>
                                            </a>
                                            <span class="open-modal" data-toggle="modal" data-action="approve" data-target="#approve" data-id="{{$order->id}}" data-folio="{{$order->folio}}" data-amount="{{$order->amount}}">
                                                <button type="button" name="button" class="btn btn-link" data-toggle="tooltip" data-placement="top" data-title="Aprobar">
                                                    <i class="fas fa-check-square icon-table"></i>
                                                </button>
                                            </span>
                                        @endif
                                        @if(!$order->paid)
                                            <span class="open-modal" data-toggle="modal" data-action="pay" data-target="#pay" data-id="{{$order->id}}" data-folio="{{$order->folio}}" data-amount="{{$order->amount}}">
                                                <button type="button" name="button" class="btn btn-link" data-toggle="tooltip" data-placement="top" data-title="Pagar">
                                                    <i class="fas fa-money-bill-wave-alt icon-table"></i>
                                                </button>
                                            </span>
                                        @endif
                                        @if($order->paid == false && $order->approved == false)
                                            <span class="open-modal" data-toggle="modal" data-target="#destroy" data-action="delete" data-id="{{$order->id}}" >
                                                <button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="right" data-title="Eliminar">
                                                    <i class="fas fa-trash-alt icon-table"></i>
                                                </button>
                                            </span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="col-12">
                        <div class="col-md-8 col-sm-12 float-left form-inline select-count">

                        </div>
                        <div class="col-md-4 col-sm-12 float-right">
                            {{ $orders->appends(['search' => $search])->links() }}
                        </div>
                    </div>
                </div>
            @else
                <div class="col-12 text-center mt-5 sin-registros">
                    <i class="fas fa-exclamation-triangle fa-4x mb-2"></i>
                    <h4>{{__('SIN REGISTROS')}}</h4>
                </div>
            @endif

        </div>
    </div>

    @include('Purchase_orders.approve')
    @include('Purchase_orders.pay')
    @include('Purchase_orders.destroy')

@endsection
@section('scripts')
    <script src="{{asset('js/getID.js')}}" charset="utf-8"></script>
@endsection
