<div class="modal fade" id="correct" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-design">
            <div class="modal-header">
                <h4 class="modal-title">{{__('ATENCIÓN')}}</h4>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <p>{{$message}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-danger rounded-0 btn-modal-aceptar">
                    {{__('ACEPTAR')}}
                </button>
            </div>
        </div>
    </div>
</div>
