<div class="modal fade" id="pay" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-design">
            <div class="modal-header">
                <h5 class="modal-title">{{__('ATENCIÓN')}}</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('purchase_orders.paid')}}" method="post">
                @csrf
                <div class="modal-body text-center">
                    <p>{{__('¿Realmente desea cambiar el status de pago?')}}</p>
                    <p id="pay-folio"></p>
                    <p id="pay-amount"></p>
                    <input type="hidden" name="id" id="pay-id" required>
                    <input type="text" name="comments" class="form-control create-input" placeholder="Comentarios">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-modal-cancelar" data-dismiss="modal">
                        {{__('CANCELAR')}}
                    </button>
                    <button type="submit" class="btn btn-danger rounded-0 btn-modal-aceptar">
                        {{__('ACEPTAR')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
