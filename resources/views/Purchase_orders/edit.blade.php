@extends('layouts.app')
@section('title','CHRONOS - ÓRDENES DE COMPRA')
@section('orden_compra','side-active')
@section('top-title','ÓRDENES DE COMPRA')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
@endsection
@section('content')

    <div class="container" id="edit-purchase_order">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h4 class="title"><strong>{{"ÓRDEN DE COMPRA - ".$order->folio}}</strong></h4>
            </div>
            <form class="col-12" method="post" enctype="multipart/form-data" @submit="submitForm">
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" v-model="date" name="date" class="form-control create-input" value="{{$order->date}}" placeholder="Fecha*" onfocus="(this.type='date')" onblur="(this.type='text')" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <select v-model="supplier_id" @change="supplier($event)" class="form-control create-input select-input" name="">
                        <option value="" disabled selected>{{__('Proveedor*')}}</option>
                        <option v-for="supplier in suppliers" :value="supplier.id">@{{supplier.business_name}}</option>
                    </select>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-5">
                    <div class="custom-file custom-file1 px-0">
                      <input type="file" name="file" class="custom-file-input" @change="filename($event)" id="file">
                      <label class="custom-file-label" id="label-file" for="customFile">@{{fileName}}</label>
                    </div>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <h5 class="title mb-3"><strong>{{__('AGREGAR EQUIPO/CONSUMIBLES')}}</strong></h5>
                    <select class="form-control create-input select-input" @change="item($event)" name="item" id="item">
                        <option value="" selected disabled>{{__('Equipo/Consumible*')}}</option>
                        <option v-for="item in items" :value="item.sku">@{{ item.description }}</option>
                    </select>
                    <div class="col-12 text-right px-0">
                        <button type="button" v-bind:class="{ disabled:addButton }" @click="addItem()" class="btn btn-link text-left px-0 text-decoration-none title add-button" id="add-button">
                            <strong>{{__('AGREGAR')}}</strong>
                        </button>
                    </div>
                </div>
                <div class="col-12 table-responsive mt-4" id="div-table" >
                    <table class="table" id="table-content">
                        <thead>
                            <tr>
                                <th>{{__('EQUIPO/CONSUMIBLE')}}</th>
                                <th>{{__('CANTIDAD')}}</th>
                                <th>{{__('IMPORTE')}}</th>
                                <th>{{__('SUBTOTAL')}}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table-content">
                            <tr v-for="(item, index) in selectedItems" :key="index" :item="item">
                                <td class="align-middle">@{{ item.description }}</td>
                                <td class="align-middle">
                                    <input type="number" class="create-input" name="quantity" min="1" @change="quantity($event,index)" @keyup="quantity($event,index)" :value="item.quantity">
                                </td>
                                <td class="align-middle">$@{{ item.amount }}</td>
                                <td class="align-middle">$@{{item.subtotal}}</td>
                                <td class="align-middle">
                                    <button type="button" class="btn btn-link text-white" @click="removeRow(index)">
                                        <i class="fas fa-minus-circle fa-lg"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="col-12 px-0 mt-4">
                        <div class="col-6 float-left title px-0">
                            <h5><strong>{{__('TOTAL')}}</strong></h5>
                        </div>
                        <div class="col-6 float-right px-0 text-right title">
                            <h5><strong>$@{{total_price}}</strong></h5>
                        </div>
                    </div>
                </div>
                <div class="col-12 px-0 text-right buttons" style="margin-top: 40px">
                    <a href="{{route('purchase_orders.index')}}" class="btn btn-link">{{__('CANCELAR')}}</a>
                    <button type="submit" class="btn submit-button py-2 px-5 rounded-0" v-html="button_content">@{{button_content}}</button>
                </div>
            </form>
        </div>
    </div>
    @include('Purchase_orders.correct')
@endsection

@section('scripts')
    <script src="{{asset('js/vue.js')}}" charset="utf-8"></script>
    <script src="{{asset('js/axios.js')}}" charset="utf-8"></script>
    <script type="text/javascript">
        var app = new Vue({
            el: '#edit-purchase_order',
            data:{
                current_items: {!!$details!!},
                selectedItems: [],
                date: "{{$order->date}}",
                suppliers : {!!$suppliers!!},
                supplier_id: "{{$order->supplier_id}}",
                fileName: "{{$order->invoice}}",
                file: '',
                items: {!!json_encode($items)!!},
                getItems: "/purchase_orders/getItems/",
                getItem: "/purchase_orders/getItem/",
                message: "Orden actualizada correctamente",
                updateURL: "/purchase_orders/update/"+"{{$order->id}}",
                selectedItem: "",
                button_content: "ACTUALIZAR",
                total_price: "{{$order->amount}}",
                addButton: false,
            },
            mounted: function(){
                for (var i = 0; i < this.current_items.length; i++) {
                    this.selectedItems.push({
                        id: this.current_items[i].id,
                        sku: this.current_items[i].sku,
                        description: this.current_items[i].description,
                        subtotal: this.current_items[i].subtotal,
                        quantity: this.current_items[i].quantity,
                        amount: this.current_items[i].amount
                    })
                }
            },
            methods:{
                supplier(event){
                    this.clear();
                    axios.get(this.getItems+event.target.value)
                    .then(response => {
                        this.addButton = false;
                        this.items = response.data.items;
                    })
                },

                clear(){
                    this.items = [];
                    this.selectedItems = [];
                    this.total_price = 0;
                    this.selectedItem = "";
                },
                filename(event){
                    this.fileName = event.target.files[0].name;
                    this.file = event.target.files[0];
                },
                item(event){
                    this.selectedItem = event.target.value;
                },
                quantity(event, index){
                    this.selectedItems[index].quantity = event.target.value;
                    this.selectedItems[index].subtotal = (event.target.value*this.selectedItems[index].amount);
                    this.total();
                },
                addItem(){
                    if(this.selectedItem != ""){
                        axios.get(this.getItem+this.selectedItem)
                        .then(response => {
                            this.selectedItems.push({
                                id: response.data.item.id,
                                sku: response.data.item.sku,
                                description: response.data.item.description,
                                amount: response.data.item.initial_cost,
                                quantity: 1,
                                subtotal: response.data.item.initial_cost
                            });
                            this.total();
                            this.tableHidden = false;
                        });
                    }
                },
                total(){
                    this.total_price = 0;
                    for (var i = 0; i < this.selectedItems.length; i++) {
                        this.total_price += this.selectedItems[i].subtotal;
                        if(this.total_price < 0){
                            this.total_price = 0;
                        }
                    }
                },
                removeRow(index){
                    this.selectedItems.splice(index, 1);
                    if (this.selectedItems.length < 1) {
                        this.tableHidden = true;
                    }
                    this.total();
                },

                submitForm: async function(event){
                    event.preventDefault();
                    if(this.selectedItems.length > 0 && this.date != "" && this.supplier_id != "" && this.total_price > 0){
                        this.button_content = "<i class='fas fa-spinner fa-pulse fa-lg mr-3'></i>CARGANDO...";
                        var file = document.querySelector('#file');
                        var data = new FormData();
                        data.append("amount", this.total_price);
                        data.append("supplier_id", this.supplier_id);
                        data.append("date", this.date);
                        data.append('file', $('input[type=file]')[0].files[0]);
                        data.append("items", JSON.stringify(this.selectedItems));

                        await $.ajax({
                            type: "POST",
                            url: this.updateURL,
                            processData: false,
                            contentType: false,
                            cache: false,
                            data: data,
                            enctype: 'multipart/form-data',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            },
                            success: function(response){
                                if(response.message == "OK"){
                                    $('#correct').modal('show');
                                }
                            }
                        })
                        this.button_content = "CREAR";
                    }
                }
            }
        })
        $('#correct').on('hidden.bs.modal', function (e) {
            location.reload();
        })
    </script>
@endsection
