@extends('layouts.app')
@section('title','CHRONOS - ÓRDENES DE COMPRA')
@section('orden_compra','side-active')
@section('top-title','ÓRDENES DE COMPRA')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
@endsection
@section('content')

    <div class="container" id="purchase_orders">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h4 class="title"><strong>{{__('REGISTRAR ÓRDEN DE COMPRA')}}</strong></h4>
            </div>
            <form @submit="submitForm" class="col-12" action="" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" v-model="date" name="date" class="form-control create-input @error('date') is-invalid @endif" placeholder="Fecha*"  value="{{old('date')}}"  onfocus="(this.type='date')" onblur="(this.type='text')" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <select v-model="supplier_id" class="form-control create-input select-input" @change="supplier($event)" name="supplier_id" id="supplier_id" required>
                        <option value="" disabled selected>{{__('Proveedor*')}}</option>
                        @foreach ($suppliers as $supplier)
                            <option value="{{$supplier->id}}">{{$supplier->business_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-5">
                    <div class="custom-file custom-file1 px-0">
                      <input type="file" name="file" class="custom-file-input" @change="filename($event)" id="file" required>
                      <label class="custom-file-label" id="label-file" for="customFile">@{{fileName}}</label>
                    </div>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <h5 class="title mb-3"><strong>{{__('AGREGAR EQUIPO/CONSUMIBLES')}}</strong></h5>
                    <select class="form-control create-input select-input" @change="item($event)" name="item" id="item">
                        <option value="" selected disabled>{{__('Equipo/Consumible*')}}</option>
                        <option v-for="item in items" :value="item.sku">@{{ item.description }}</option>
                    </select>
                    <div class="col-12 text-right px-0">
                        <button type="button" v-bind:class="{ disabled:addButton }" @click="addItem()" class="btn btn-link text-left px-0 text-decoration-none title add-button" id="add-button">
                            <strong>{{__('AGREGAR')}}</strong>
                        </button>
                    </div>
                </div>

                <div class="col-12 table-responsive mt-4" id="div-table" :class=" { 'table-hidden':tableHidden } ">
                    <table class="table" id="table-content">
                        <thead>
                            <tr>
                                <th>{{__('EQUIPO/CONSUMIBLE')}}</th>
                                <th>{{__('CANTIDAD')}}</th>
                                <th>{{__('IMPORTE')}}</th>
                                <th>{{__('SUBTOTAL')}}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table-content">
                            <tr v-for="(item, index) in selectedItems" :key="index" :item="item">
                                <td class="align-middle">@{{ item.description }}</td>
                                <td class="align-middle">
                                    <input type="number" class="create-input" name="quantity" min="1" @change="quantity($event,index)" @keyup="quantity($event,index)" :value="item.quantity">
                                </td>
                                <td class="align-middle">$@{{ item.initial_cost }}</td>
                                <td class="align-middle">$@{{item.subtotal}}</td>
                                <td class="align-middle">
                                    <button type="button" class="btn btn-link text-white" @click="removeRow(index)">
                                        <i class="fas fa-minus-circle fa-lg"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="col-12 px-0 mt-4">
                        <div class="col-6 float-left title px-0">
                            <h5><strong>{{__('TOTAL')}}</strong></h5>
                        </div>
                        <div class="col-6 float-right px-0 text-right title">
                            <h5><strong>$@{{total_price}}</strong></h5>
                        </div>
                    </div>
                </div>
                <div class="col-12 px-0 text-right buttons" :class="{ 'table-hidden':tableHidden }"  style="margin-top: 40px">
                    <a href="{{route('purchase_orders.index')}}" class="btn btn-link">{{__('CANCELAR')}}</a>
                    <button type="submit" class="btn submit-button py-2 px-5 rounded-0" v-html="button_content">@{{button_content}}</button>
                </div>
            </form>
        </div>
    </div>

    @include('Purchase_orders.correct')

@endsection
@section('scripts')
    <script src="{{asset('js/vue.js')}}" charset="utf-8"></script>
    <script src="{{asset('js/axios.js')}}" charset="utf-8"></script>
    <script src="{{asset('js/purchase_orders.js')}}" charset="utf-8"></script>
@endsection
