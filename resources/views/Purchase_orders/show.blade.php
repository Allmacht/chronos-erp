@extends('layouts.app')
@section('title','CHRONOS - ÓRDENES DE COMPRA')
@section('orden_compra','side-active')
@section('top-title','ÓRDENES DE COMPRA')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
@endsection
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center mb-4">
                <h4 class="title"><strong>{{'ORDEN DE COMPRA - '.$purchase_order->folio}}</strong></h4>
            </div>
            <div class="col-12 form-row">
                <div class="col-xl-4 table-responsive float-left">
                    <h6 class="title text-center">{{__('INFORMACIÓN GENERAL')}}</h6>
                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th>{{__('CLAVE')}}</th>
                                <th>{{__('VALOR')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><b>{{__('Folio')}}</b></td>
                                <td>{{$purchase_order->folio}}</td>
                            </tr>
                            <tr>
                                <td><b>{{__('Fecha')}}</b></td>
                                <td>{{$purchase_order->date}}</td>
                            </tr>
                            <tr>
                                <td><b>{{__('Importe')}}</b></td>
                                <td>{{$purchase_order->amount}}</td>
                            </tr>
                            <tr>
                                <td><b>{{__('Cantidad de productos')}}</b></td>
                                <td>{{$purchase_order->total_quantity}}</td>
                            </tr>
                            <tr>
                                <td><b>{{__('Proveedor')}}</b></td>
                                <td>{{$purchase_order->supplier->business_name}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xl-4 table-responsive">
                    <h6 class="title text-center">{{__('DATOS DE APROBACIÓN')}}</h6>
                    @if($approved)
                        <table class="table text-center">
                            <thead>
                                <tr>
                                    <th>{{__('CLAVE')}}</th>
                                    <th>{{__('VALOR')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b>{{__('Usuario')}}</b></td>
                                    <td>{{$approved->user->name}}</td>
                                </tr>
                                <tr>
                                    <td><b>{{__('Comentarios')}}</b></td>
                                    <td>{{$approved->comments}}</td>
                                </tr>
                                <tr>
                                    <td><b>{{__('Fecha de aprobación')}}</b></td>
                                    <td>{{$approved->created_at}}</td>
                                </tr>
                            </tbody>
                        </table>
                    @else
                        <div class="col-12 text-center mt-3 title py-4">
                            <i class="fas fa-exclamation-triangle fa-2x mb-2"></i>
                            <p>{{__('La orden de compra aún no ha sido aprobada')}}</p>
                        </div>
                    @endif
                </div>
                <div class="col-xl-4 table-responsive">
                    <h6 class="title text-center">{{__('DATOS DE PAGO')}}</h6>
                    @if($paid)
                        <table class="table text-center">
                            <thead>
                                <tr>
                                    <th>{{__('CLAVE')}}</th>
                                    <th>{{__('VALOR')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b>{{__('Usuario')}}</b></td>
                                    <td>{{$paid->user->name}}</td>
                                </tr>
                                <tr>
                                    <td><b>{{__('Comentarios')}}</b></td>
                                    <td>{{$paid->comments}}</td>
                                </tr>
                                <tr>
                                    <td><b>{{__('Fecha de pago')}}</b></td>
                                    <td>{{$paid->created_at}}</td>
                                </tr>
                            </tbody>
                        </table>
                    @else
                        <div class="col-12 text-center mt-3 title py-4">
                            <i class="fas fa-exclamation-triangle fa-2x mb-2"></i>
                            <p>{{__('La orden de compra aún no ha sido pagada')}}</p>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-12 table-responsive pt-5">
                <h6 class="title text-center mb-3">{{__('DETALLES DE LA ORDEN')}}</h6>
                <table class="table">
                    <thead>
                        <tr>
                            <th>{{__('SKU')}}</th>
                            <th>{{__('DESCRIPCIÓN')}}</th>
                            <th>{{__('CANTIDAD')}}</th>
                            <th>{{__('PRECIO UNITARIO')}}</th>
                            <th>{{__('SUBTOTAL')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($purchase_order->details as $detail)
                            <tr>
                                <td class="align-middle text-truncate">{{$detail->sku}}</td>
                                <td class="align-middle text-truncate">{{$detail->description}}</td>
                                <td class="align-middle text-truncate">{{$detail->quantity}}</td>
                                <td class="align-middle text-truncate">{{"$".$detail->amount}}</td>
                                <td class="align-middle text-truncate">{{"$".$detail->subtotal}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="text-right mt-5 buttons">
                    <a href="{{route('purchase_orders.index')}}" class="btn submit-button rounded-0">
                        {{__('ATRÁS')}}
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection
