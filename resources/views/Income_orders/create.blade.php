@extends('layouts.app')
@section('title','CHRONOS - ÓRDENES DE ENTRADA')
@section('orden_entrada','side-active')
@section('top-title','ÓRDENES DE ENTRADA')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h4 class="title"><strong>{{__('REGISTRAR ÓRDEN DE ENTRADA')}}</strong></h4>
            </div>
            <form class="col-12" action="{{route('income_orders.store')}}" method="post">
                @csrf
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="date" class="form-control create-input @error('date') is-invalid @endif" value="{{old('date')}}" placeholder="Fecha*" onfocus="(this.type='date')" onblur="(this.type='text')" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <select class="form-control create-input select-input" name="purchase_order_id" required>
                        <option value="" selected disabled>{{__('Orden de compra*')}}</option>
                        @foreach ($purchase_orders as $purchase_order)
                            <option value="{{$purchase_order->id}}">{{$purchase_order->folio}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-xl-6 mx-auto text-right buttons">
                    <a href="{{route('income_orders.index')}}" class="btn btn-link">{{__('CANCELAR')}}</a>
                    <button type="submit" class="btn submit-button py-2 px-5 rounded-0">{{__('CONTINUAR')}}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
