@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/app.css')}}">
@endsection
@section('content')
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<div class="container">
    <div class="row">
        <form class="col-xl-7 mx-auto div-form py-5" action="{{ route('password.email') }}" method="post">
            @csrf
            <div class="col-12 text-center">
                <img src="{{asset('images/logo.png')}}" class=" img-fluid mb-4" alt="" width="100px">
                <h5>{{__('Ingrese el Correo electrónico realacionado a su cuenta')}}</h5>
            </div>
            <div class="col-xl-11 mt-5 mx-auto form-group">
                <input type="email" name="email" class="form-control rounded-0 @error('email') is-invalid @endif" value="{{old('email')}}" autofocus required placeholder="Correo electrónico">
            </div>
            <div class="col-xl-12 px-5 text-right">
                <a href="{{route('login')}}" class="btn-link mr-3 btn-cancelar text-decoration-none">CANCELAR</a>
                <button type="submit" class="btn btn-submit rounded-0 px-5">{{__('ENVIAR')}}</button>
            </div>
        </form>
    </div>
</div>
@endsection
