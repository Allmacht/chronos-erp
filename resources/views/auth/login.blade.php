@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/app.css')}}">
@endsection
@section('login','active')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-10 mx-auto login-div py-4">

            <form class="col-12" action="" method="post">
                @csrf
                <div class="col-12 text-center">
                    <img src="{{asset('images/logo.png')}}" class="img-fluid mb-5" width="220px" alt="">
                </div>
                <div class="col-xl-9 mx-auto form-group">
                    <input type="email" class="form-control rounded-0 @error('email') is-invalid @enderror" name="email" placeholder="Correo electrónico" required>
                </div>
                <div class="col-xl-9 mx-auto form-group">
                    <input type="password" class="form-control rounded-0 @error('email') is-invalid @enderror" name="password" placeholder="Contraseña" required>
                </div>
                <div class="col-xl-9 mt-5 mx-auto text-center">
                    <button type="submit" class="btn btn-block mb-5 rounded-0 btn-submit">
                        <strong>{{__('INICIAR SESIÓN')}}</strong>
                    </button>

                    <a href="{{route('password.request')}}" class="btn-link text-decoration-none link-password">
                        {{__('Olvidé mi contraseña')}}
                    </a>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection
