@extends('layouts.app')
@section('title','CHRONOS - CLIENTES')
@section('clientes','side-active')
@section('top-title','CLIENTES')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
@endsection
@section('content')
    <div class="container" id="client-store">
        <div class="row">
            <div class="col-12 text-center mb-4">
                <h4 class="title"><strong>{{__('REGISTRAR CLIENTE')}}</strong></h4>
            </div>
            <div class="col-xl-6 mx-auto text-center mb-3 form-row">
                <div class="col-12 px-0">
                    <div class="col-12 div-line"><hr></div>
                </div>
                <div class="col-3">
                    <div class="mx-auto circulo circulo-active mb-3"></div>
                    <h6 class="title subtitle text-truncate">{{__('DATOS')}}<br>{{__('GENERALES')}}</h6>
                </div>
                <div class="col-3">
                    <div id="step-mdc" :class="steps.step_mdc" class="mx-auto circulo mb-3"></div>
                    <h6 class="title subtitle text-truncate">{{__('MATRIZ DE')}}<br>{{__('CONTACTO')}}</h6>
                </div>
                <div class="col-3">
                    <div id="step-ddp" :class="steps.step_ddp" class="mx-auto circulo mb-3"></div>
                    <h6 class="title subtitle text-truncate">{{__('DETALLES DE')}}<br>{{__('PRODUCTO')}}</h6>
                </div>
                <div class="col-3">
                    <div id="step-odc" :class="steps.step_odc" class="mx-auto circulo mb-3"></div>
                    <h6 class="title subtitle text-truncate">{{__('OPERACIÓN')}}<br>{{__('DEL CLIENTE')}}</h6>
                </div>
            </div>

            <form class="col-12" action="" method="post" >
                @csrf
                <div class="px-0" id="form-dg" :class="forms.form_client">
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="client.business" name="business" class="form-control create-input @error('business') is-invalid @enderror" placeholder="Giro*" value="{{old('business')}}" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="client.tradename" name="tradename" class="form-control create-input @error('tradename') is-invalid @enderror" placeholder="Nombre comercial*" value="{{old('tradename')}}" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="client.business_name" name="business_name" class="form-control create-input @error('business_name') is-invalid @enderror" placeholder="Razón social*" value="{{old('business_name')}}" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" :class="status_client.rfc" v-model="client.rfc" name="rfc" class="form-control create-input @error('rfc') is-invalid @enderror" placeholder="R.F.C.*" value="{{old('rfc')}}" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <select v-model="client.country" class="form-control create-input select-input" name="country" required>
                            <option value="" selected disabled>{{__('País*')}}</option>
                            <option v-for="country in countries" v-bind:value="country.name">@{{country.name}}</option>
                        </select>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="client.state" class="form-control create-input @error('state') is-invalid @enderror" placeholder="Estado*" name="state" value="{{old('state')}}" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="client.municipality" class="form-control create-input @error('municipality') is-invalid @enderror" placeholder="Municipio*" name="municipality" value="{{old('municipality')}}" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="client.neighborhood" name="neighborhood" class="form-control create-input @error('neighborhood') is-invalid @enderror" placeholder="Colonia" value="{{old('neighborhood')}}">
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="client.street" name="street" class="form-control create-input @error('street') is-invalid @enderror" placeholder="Calle" value="">
                    </div>
                    <div class="col-xl-6 mx-auto form-row">
                        <div class="form-group col-xl-6 mx-auto mb-3 px-0">
                            <input type="number" v-model="client.external_number" name="external_number" min="1" class="form-control create-input @error('external_number') is-invalid @enderror" placeholder="Número exterior*" value="{{old('external_number')}}" required>
                        </div>
                        <div class="form-group col-xl-6 mx-auto mb-3 px-0">
                            <input type="text" v-model="client.internal_number" name="internal_number" class="form-control create-input @error('internal_number') is-invalid @enderror" placeholder="Número interior" value="{{old('internal_number')}}">
                        </div>
                        <div class="form-group col-xl-6 mx-auto mb-3 px-0">
                            <input type="number" :class="status_client.zipcode" v-model="client.zipcode" name="zipcode" min="1" class="form-control create-input @error('zipcode') is-invalid @enderror" placeholder="Código postal*" value="{{old('zipcode')}}" required>
                        </div>
                        <div class="form-group col-xl-6 mx-auto mb-3 px-0">
                            <input type="number" :class="status_client.phone" v-model="client.phone" name="phone" min="1" class="form-control create-input @error('phone') is-invalid @enderror" placeholder="Teléfono" value="{{old('phone')}}" required>
                        </div>
                        <div class="form-group col-xl-6 mx-auto mb-3 px-0">
                            <input type="number" v-model="client.fax" name="fax" class="form-control create-input @error('fax') is-invalid @enderror" placeholder="Fax*" value="{{old('fax')}}" required>
                        </div>
                        <div class="form-group col-xl-6 mx-auto mb-3 px-0">
                            <select v-model="client.price_list" class="form-control create-input select-input" name="price_list" required>
                                <option value="" selected disabled>{{__('Lista de precios*')}}</option>
                                <option v-for="price in prices" v-bind:value="price.name">@{{price.name}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <select v-model="client.cfdi" class="form-control create-input select-input" name="cfdi" required>
                            <option value="" selected disabled>{{__('Uso del CFDI*')}}</option>
                            <option v-for="cfdi in cfdis" v-bind:value="cfdi.key">@{{cfdi.key}}</option>
                        </select>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" :class="status_client.bank_account" v-model="client.bank_account" name="bank_account" class="form-control create-input @error('bank_account') is-invalid @enderror" placeholder="Cuenta bancaria*" value="{{old('bank_account')}}" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="client.bank" name="bank" class="form-control create-input @error('bank') is-invalid @enderror" placeholder="Banco*" value="{{old('bank')}}" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="email" :class="status_client.email" v-model="client.email" name="email" class="form-control create-input @error('email') is-invalid @enderror" placeholder="Correo electrónico*" value="{{old('email')}}" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="email" :class="status_client.invoice_email" v-model="client.invoice_email" name="invoice_email" class="form-control create-input @error('invoice_email') is-invalid @enderror" placeholder="Correo electrónico para facturas*" value="{{old('invoice_email')}}" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="client.credit_days" name="credit_days" class="form-control create-input @error('credit_days') is-invalid @enderror" placeholder="Días de crédito" value="{{old('credit_days')}}" required>
                    </div>
                    <div class="col-xl-6 mx-auto text-right buttons" id="form-dg-buttons">
                        <a href="{{route('clients.index')}}" class="btn btn-link">{{__('CANCELAR')}}</a>
                        <button type="button" @click="check_form_client()" id="form-dg-next" class="btn submit-button py-2 px-5 rounded-0">{{__('SIGUIENTE')}}</button>
                    </div>
                </div>

                <div class="px-0" id="form-mdc" :class="forms.form_mdc">
                    <div class="form-group col-xl-6 mx-auto my-3">
                        <h5 class="title">{{__('DIRECTOR O GERENTE DE CONTACTO')}}</h5>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="director.name" name="name" class="form-control create-input" placeholder="Nombre*" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="director.position" name="position" class="form-control create-input" placeholder="Puesto exacto*" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="email" :class="director.check_email" v-model="director.email" name="email" class="form-control create-input" placeholder="Correo electrónico*" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-4">
                        <input type="number" :class="director.check_phone" v-model="director.phone" name="phone" class="form-control create-input" placeholder="Teléfono*" min="1" required>
                    </div>


                    <div class="form-group col-xl-6 mx-auto my-3">
                        <h5 class="title">{{__('RESPONSABLE COMERCIAL')}}</h5>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="manager.name" name="name" class="form-control create-input" placeholder="Nombre*" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="manager.position" name="position" class="form-control create-input" placeholder="Puesto exacto*" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="email" :class="manager.check_email" v-model="manager.email" name="email" class="form-control create-input" placeholder="Correo electrónico*" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-4">
                        <input type="number" :class="manager.check_phone" v-model="manager.phone" name="phone" class="form-control create-input" placeholder="Teléfono*" min="1" required>
                    </div>


                    <div class="form-group col-xl-6 mx-auto my-3">
                        <h5 class="title">{{__('RESPONSABLE DE OPERACIONES / LOGÍSTICA')}}</h5>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="operations.name" name="name" class="form-control create-input" placeholder="Nombre*" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="operations.position" name="position" class="form-control create-input" placeholder="Puesto exacto*" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="email" :class="operations.check_email" v-model="operations.email" name="email" class="form-control create-input" placeholder="Correo electrónico*" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-4">
                        <input type="number" :class="operations.check_phone" v-model="operations.phone" name="phone" class="form-control create-input" placeholder="Teléfono*" min="1" required>
                    </div>


                    <div class="form-group col-xl-6 mx-auto my-3">
                        <h5 class="title">{{__('RESPONSABLE DE COMERCIO EXTERIOR')}}</h5>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="foreign.name" name="name" class="form-control create-input" placeholder="Nombre*" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="foreign.position" name="position" class="form-control create-input" placeholder="Puesto exacto*" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="email" :class="foreign.check_email" v-model="foreign.email" name="email" class="form-control create-input" placeholder="Correo electrónico*" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-4">
                        <input type="number" :class="foreign.check_phone" v-model="foreign.phone" name="phone" class="form-control create-input" placeholder="Teléfono*" min="1" required>
                    </div>


                    <div class="form-group col-xl-6 mx-auto my-3">
                        <h5 class="title">{{__('RESPONSABLE DE FACTURAS')}}</h5>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="invoices.name" name="name" class="form-control create-input" placeholder="Nombre*" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="text" v-model="invoices.position" name="position" class="form-control create-input" placeholder="Puesto exacto*" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="email" :class="invoices.check_email" v-model="invoices.email" name="email" class="form-control create-input" placeholder="Correo electrónico*" required>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="number" :class="invoices.check_phone" v-model="invoices.phone" name="phone" class="form-control create-input" placeholder="Teléfono*" min="1" required>
                    </div>

                    <div class="col-xl-6 mx-auto text-right buttons" id="form-dg-buttons">
                        <button type="button" @click="form_mdc_back()" class="btn btn-link btn-next">{{__('ATRÁS')}}</button>
                        <button type="button" @click="check_form_mdc()" class="btn submit-button py-2 px-5 rounded-0">{{__('SIGUIENTE')}}</button>
                    </div>

                </div>

                <div class="px-0" id="form-ddp" :class="forms.form_ddp">
                    <div class="form-group col-xl-6 mx-auto my-3">
                        <h5 class="title">{{__('DATOS GENERALES DEL PRODUCTO')}}</h5>
                    </div>
                    <div class="form-group col-xl-6 mx-auto my-3">
                        <input type="text" v-model="product_data.general_description" class="form-control create-input" placeholder="Descripción general" name="general_description">
                    </div>
                    {{-- file --}}
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <div class="custom-file custom-file1 px-0">
                          <input type="file" name="catalog_availability" @change="catalog_availability_file($event)" class="custom-file-input" id="catalog_availability">
                          <label class="custom-file-label" id="label-file" for="customFile">@{{product_data.file_message}}</label>
                        </div>
                    </div>
                    {{-- file --}}
                    <div class="form-group col-xl-6 mx-auto my-3">
                        <input type="text" v-model="product_data.datasheet_availability" name="datasheet_availability" class="form-control create-input" placeholder="Disponibilidad de ficha técnica de productos">
                    </div>
                    <div class="form-group col-xl-6 mx-auto my-3">
                        <input type="text" v-model="product_data.business_type" name="business_type" class="form-control create-input" placeholder="Giro industrial/Comercial" >
                    </div>
                    <div class="form-group col-xl-6 mx-auto my-3">
                        <input type="text" v-model="product_data.product_type" name="product_type" class="form-control create-input" placeholder="Producto Terminado/Materia Prima">
                    </div>
                    <div class="form-group col-xl-6 mx-auto my-4">
                        <input type="text" v-model="product_data.product_rotation" name="product_rotation" class="form-control create-input" placeholder="Rotación del producto">
                    </div>

                    <div class="form-group col-xl-6 mx-auto my-3">
                        <h5 class="title">{{__('SERVICIOS CONTRATADOS')}}</h5>
                    </div>

                    <div class="form-group col-xl-6 mx-auto mb-3 text-right">
                        <select v-model="selected_service" class="form-control create-input select-input" name="services">
                            <option value="" selected disabled>{{__('Servicio*')}}</option>
                            <option v-for="service in services" :value="service.id">@{{service.name}}</option>
                        </select>
                        <button type="button" @click="add_service()" class="btn btn-link text-decoration-none title add-button" name="button">{{__('AGREGAR')}}</button>
                    </div>

                    <div class="col-xl-6 mx-auto mb-3">
                        <table class="table">
                            <tbody>
                                <tr v-for="(service, index) in selected_services" :key="index">
                                    <td class="align-middle">@{{service.name}}</td>
                                    <td class="align-middle text-right">$@{{service.price}}</td>
                                    <td class="align-middle text-right">
                                        <button type="button" @click="removeRow(index)" class="btn btn-link text-white">
                                            <i class="fas fa-minus-circle fa-lg"></i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-xl-6 mx-auto text-right buttons" id="form-dg-buttons">
                        <button type="button" @click="form_ddp_back()" class="btn btn-link btn-next">{{__('ATRÁS')}}</button>
                        <button type="button" @click="check_form_ddp()" class="btn submit-button py-2 px-5 rounded-0">{{__('SIGUIENTE')}}</button>
                    </div>
                </div>

                <div class="px-0" id="form-odc" :class="forms.form_odc">
                    <div class="form-group col-xl-6 mx-auto my-3">
                        <h5 class="title">{{__('OPERACIÓN DEL CLIENTE')}}</h5>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mx-auto my-3">
                        <select v-model="client_operations.packing_type" class="form-control create-input select-input" name="packing_type">
                            <option value="" selected disabled>{{__('Tipo de embalaje')}}</option>
                        </select>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mx-auto my-3">
                        <input v-model="client_operations.packing_measures" type="text" name="packing_measures" class="form-control create-input" placeholder="Medidas de embalaje">
                    </div>
                    <div class="form-group col-xl-6 mx-auto mx-auto my-3">
                        <select v-model="client_operations.transport_type" class="form-control create-input select-input" name="transport_type">
                            <option value="" selected disabled>{{__('Tipo de transporte')}}</option>
                        </select>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mx-auto my-3">
                        <input type="text" v-model="client_operations.product_handling" name="product_handling" class="form-control create-input" placeholder="Descripción del manejo del producto">
                    </div>
                    <div class="form-group col-xl-6 mx-auto mx-auto my-3">
                        <input type="text" v-model="client_operations.storage_conditions" name="storage_conditions" class="form-control create-input" placeholder="Condiciones de almacenaje">
                    </div>
                    {{-- file --}}
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <div class="custom-file custom-file1 px-0">
                          <input type="file" name="delivery_schedule" @change="delivery_schedule_file($event)" class="custom-file-input" id="delivery_schedule">
                          <label class="custom-file-label" id="label-file" for="customFile">@{{client_operations.delivery_schedule_name}}</label>
                        </div>
                    </div>
                    {{-- file --}}
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <input type="number" step="0.01" v-model="client_operations.product_value" name="product_value" class="form-control create-input" placeholder="Valor de la mercancía a ingresar">
                    </div>

                    <div class="form-group col-xl-6 mx-auto my-3">
                        <h5 class="title">{{__('COMERCIO EXTERIOR')}}</h5>
                    </div>
                    <div class="col-xl-6 mx-auto mb-3 form-row text-truncate">
                        <div class="col-10">
                            <h5 class="text-white text-truncate">{{__('Operación de comercio exterior')}}</h5>
                        </div>
                        <div class="col-2 custom-control custom-switch float-right text-right title">
                            <label for="" class="title" style="margin-right:37px">NO</label>
                            <input v-model="client_operations.foreign_commerce" type="checkbox" class="custom-control-input" id="foreign_commerce">
                            <label class="custom-control-label" for="foreign_commerce">SI</label>
                        </div>
                    </div>
                    <div class="col-xl-6 mx-auto mb-3 form-row text-truncate">
                        <div class="col-10">
                            <h5 class="text-white">{{__('Importación')}}</h5>
                        </div>
                        <div class="col-2 custom-control custom-switch float-right text-right title">
                            <label for="" class="title" style="margin-right:37px">NO</label>
                            <input v-model="client_operations.import" type="checkbox" class="custom-control-input" id="import">
                            <label class="custom-control-label" for="import">SI</label>
                        </div>
                    </div>
                    <div class="col-xl-6 mx-auto mb-3 form-row text-truncate">
                        <div class="col-10">
                            <h5 class="text-white">{{__('Exportación')}}</h5>
                        </div>
                        <div class="col-2 custom-control custom-switch float-right text-right title">
                            <label for="" class="title" style="margin-right:37px">NO</label>
                            <input v-model="client_operations.export" type="checkbox" class="custom-control-input" id="export">
                            <label class="custom-control-label" for="export">SI</label>
                        </div>
                    </div>
                    <div class="form-group col-xl-6 mx-auto mb-4">
                        <input type="text" v-model="client_operations.foreign_commerce_number" name="foreign_commerce_number" class="form-control create-input" placeholder="Programa de comercio exterior/Número">
                    </div>
                    <div class="col-xl-6 mx-auto mb-3 form-row text-truncate">
                        <div class="col-10">
                            <h5 class="text-white text-truncate">{{__('Catálogo de fracción arancelaria')}}</h5>
                        </div>
                        <div class="col-2 custom-control custom-switch float-right text-right title">
                            <label for="" class="title" style="margin-right:37px">NO</label>
                            <input v-model="client_operations.tariff_fraction" type="checkbox" class="custom-control-input" id="tariff_fraction">
                            <label class="custom-control-label" for="tariff_fraction">SI</label>
                        </div>
                    </div>
                    <div class="col-xl-6 mx-auto mb-3 form-row text-truncate">
                        <div class="col-10">
                            <h5 class="text-white">{{__('Agente aduanal')}}</h5>
                        </div>
                        <div class="col-2 custom-control custom-switch float-right text-right title">
                            <label for="" class="title" style="margin-right:37px">NO</label>
                            <input v-model="client_operations.customs_agent" type="checkbox" class="custom-control-input" id="customs_agent">
                            <label class="custom-control-label" for="customs_agent">SI</label>
                        </div>
                    </div>
                    <div class="col-xl-6 mx-auto mb-3 form-row text-truncate">
                        <div class="col-10">
                            <h5 class="text-white text-truncate">{{__('Inspector de calidad')}}</h5>
                        </div>
                        <div class="col-2 custom-control custom-switch float-right text-right title">
                            <label for="" class="title" style="margin-right:37px">NO</label>
                            <input v-model="client_operations.quality_inspection" type="checkbox" class="custom-control-input" id="quality_inspection">
                            <label class="custom-control-label" for="quality_inspection">SI</label>
                        </div>
                    </div>
                    <div class="col-xl-6 mx-auto mb-3 form-row text-truncate">
                        <div class="col-10">
                            <h5 class="text-white text-truncate">{{__('Pradrón de importadores')}}</h5>
                        </div>
                        <div class="col-2 custom-control custom-switch float-right text-right title">
                            <label for="" class="title" style="margin-right:37px">NO</label>
                            <input v-model="client_operations.importers" type="checkbox" class="custom-control-input" id="importers">
                            <label class="custom-control-label" for="importers">SI</label>
                        </div>
                    </div>
                    {{-- file --}}
                    <div class="form-group col-xl-6 mx-auto mb-3">
                        <div class="custom-file custom-file1 px-0">
                          <input type="file" name="dangerous_material" @change="dangerous_material_file($event)" class="custom-file-input" id="dangerous_material">
                          <label class="custom-file-label" id="label-file" for="customFile">@{{client_operations.dangerous_material_name}}</label>
                        </div>
                    </div>
                    {{-- file --}}
                    <div class="col-xl-6 mx-auto mb-3 form-row text-truncate">
                        <div class="col-10">
                            <h5 class="text-white text-truncate">{{__('mercancía sensible')}}</h5>
                        </div>
                        <div class="col-2 custom-control custom-switch float-right text-right title">
                            <label for="" class="title" style="margin-right:37px">NO</label>
                            <input v-model="client_operations.fragile" type="checkbox" class="custom-control-input" id="fragile">
                            <label class="custom-control-label" for="fragile">SI</label>
                        </div>
                    </div>

                    <div class="col-xl-6 mx-auto text-right buttons" id="form-dg-buttons">
                        <button type="button" @click="form_odc_back()" class="btn btn-link btn-next">{{__('ATRÁS')}}</button>
                        <button type="button" @click="submit()" class="btn submit-button py-2 px-5 rounded-0" v-html="button_text">@{{button_text}}</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

    @include('Clients.success')

@endsection
@section('scripts')
    <script src="{{asset('js/vue.js')}}" charset="utf-8"></script>
    <script src="{{asset('js/axios.js')}}" charset="utf-8"></script>
    <script src="{{asset('js/clients.js')}}" charset="utf-8"></script>
@endsection
