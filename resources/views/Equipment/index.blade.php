@extends('layouts.app')
@section('title','CHRONOS - EQUIPO')
@section('equipo','side-active')
@section('top-title','EQUIPO')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
@endsection
@section('content')

    <div class="container-fluid index-container">
        <div class="row">

            <div class="col-12 form-row">
                <div class="col-xl-6">
                    <form action="" method="get">
                        <div class="input-group col-xl-8 mb-3">
                          <input type="text" class="form-control search-input rounded-0" name="search" value="{{$search}}" placeholder="BUSCAR" >
                          <div class="input-group-append">
                            <button class="btn btn-outline-secondary btn-search rounded-0" type="submit" >
                                <i class="fas fa-search"></i>
                            </button>
                          </div>
                        </div>
                    </form>
                </div>
                <div class="col-xl-6 text-right float-right">
                    <a href="{{route('equipment.import')}}" class="btn index-button rounded-0 text-truncate px-5 mr-3"><strong>{{__('IMPORTAR')}}</strong> </a>
                    <a href="{{route('equipment.create')}}" class="btn index-button rounded-0 text-truncate"><strong>{{__('REGISTRAR EQUIPO')}}</strong></a>
                </div>
            </div>

            @if($equipment->count())
                <div class="col-12 table-responsive px-4 mt-4">
                    <table class="table">
                        <thead>
                            <tr class="text-center">
                                <th>{{__('SKU')}}</th>
                                <th>{{__('DESCRIPCIÓN')}}</th>
                                <th>{{__('MARCA')}}</th>
                                <th>{{__('RESPONSABLE')}}</th>
                                <th>{{__('UNIDAD')}}</th>
                                <th>{{__('CANTIDAD')}}</th>
                                <th>{{__('ACCIONES')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($equipment as $equip)
                                <tr class="text-center">
                                    <td class="align-middle text-truncate">{{$equip->sku}}</td>
                                    <td class="align-middle text-truncate">{{$equip->description}}</td>
                                    <td class="align-middle text-truncate">{{$equip->brand}}</td>
                                    <td class="align-middle text-truncate">{{"PENDIENTE"}}</td>
                                    <td class="align-middle text-truncate">{{$equip->unit_measurement}}</td>
                                    <td class="align-middle text-truncate">{{$equip->quantity}}</td>
                                    <td class="align-middle text-truncate">
                                        <a href="{{route('equipment.show',['id' => $equip->id])}}" class="px-2" data-toggle="tooltip" data-placement="left" data-title="Mostrar">
                                            <i class="fas fa-clipboard-list icon-table"></i>
                                        </a>
                                        <a href="{{route('equipment.edit', ['id' => $equip->id])}}" class="px-2" data-toggle="tooltip" data-placement="top" data-title="Editar">
                                            <i class="fas fa-pencil-alt icon-table"></i>
                                        </a>
                                        <span class="open-modal" data-toggle="modal" data-target="#delete" data-action="delete" data-id="{{$equip->id}}">
                                            <button type="button" class="btn btn-link px-2" data-toggle="tooltip" data-placement="right" data-title="Eliminar">
                                                <i class="fas fa-trash icon-table"></i>
                                            </button>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="col-12">
                        <div class="col-md-8 col-sm-12 float-left form-inline select-count">

                        </div>
                        <div class="col-md-4 col-sm-12 float-right">
                            {{$equipment->appends(['search' => $search])->links()}}
                        </div>
                    </div>
                </div>
            @else
                <div class="col-12 text-center mt-5 sin-registros">
                    <i class="fas fa-exclamation-triangle fa-4x mb-2"></i>
                    <h4>{{__('SIN REGISTROS')}}</h4>
                </div>
            @endif
        </div>
    </div>


    @include('Equipment.delete');
@endsection
@section('scripts')
    <script src="{{asset('js/getID.js')}}" charset="utf-8"></script>
@endsection
