<div class="modal fade" id="delete" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-design">
            <div class="modal-header">
                <h5 class="modal-title">{{__('ATENCIÓN')}}</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('equipment.destroy')}}" method="post">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="id" id="delete-id">
                    <p class="text-center">{{__('¿Realmente desea eliminar este elemento?')}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-modal-cancelar" data-dismiss="modal">
                        {{__('CANCELAR')}}
                    </button>
                    <button type="submit" class="btn btn-danger rounded-0 btn-modal-aceptar">
                        {{__('ACEPTAR')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
