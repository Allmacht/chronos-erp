@extends('layouts.app')
@section('title','CHRONOS - EQUIPO')
@section('equipo','side-active')
@section('top-title','EQUIPO')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h4 class="title"><strong>{{$equipment->sku." - ".$equipment->description}}</strong></h4>
            </div>
            <form class="col-12" action="{{route('equipment.update',['id' => $equipment->id])}}" method="post">
                @csrf
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="sku" class="form-control create-input @error('sku') is-invalid @enderror" value="{{$equipment->sku}}" placeholder="SKU*" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="description" class="form-control create-input @error('description') is-invalid @enderror" value="{{$equipment->description}}" placeholder="Descripción*" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <select class="form-control create-input select-input" name="unit_measurement" required>
                        <option value="" selected disabled>{{__('Unidad de medida*')}}</option>
                        <option @if($equipment->unit_measurement == "Kilo") selected @endif value="Kilo">{{__('Kilo')}}</option>
                        <option @if($equipment->unit_measurement == "Gramo") selected @endif value="Gramo">{{__('Gramo')}}</option>
                        <option @if($equipment->unit_measurement == "Metro lineal") selected @endif value="Metro lineal">{{__('Metro lineal')}}</option>
                        <option @if($equipment->unit_measurement == "Metro cuadrado") selected @endif value="Metro cuadrado">{{__('Metro cuadrado')}}</option>
                        <option @if($equipment->unit_measurement == "Pieza") selected @endif value="Pieza">{{__('Pieza')}}</option>
                        <option @if($equipment->unit_measurement == "Cabeza") selected @endif value="Cabeza">{{__('Cabeza')}}</option>
                        <option @if($equipment->unit_measurement == "Litro") selected @endif value="Litro">{{__('Litro')}}</option>
                        <option @if($equipment->unit_measurement == "Par") selected @endif value="Par">{{__('Par')}}</option>
                        <option @if($equipment->unit_measurement == "Kilowatt") selected @endif value="Kilowatt">{{__('Kilowatt')}}</option>
                        <option @if($equipment->unit_measurement == "Millar") selected @endif value="Millar">{{__('Millar')}}</option>
                        <option @if($equipment->unit_measurement == "Juego") selected @endif value="Juego">{{__('Juego')}}</option>
                        <option @if($equipment->unit_measurement == "Kilowatt/Hora") selected @endif value="Kilowatt/Hora">{{__('Kilowatt/Hora')}}</option>
                        <option @if($equipment->unit_measurement == "Tonelada") selected @endif value="Tonelada">{{__('Tonelada')}}</option>
                        <option @if($equipment->unit_measurement == "Barril") selected @endif value="Barril">{{__('Barril')}}</option>
                        <option @if($equipment->unit_measurement == "Gramo neto") selected @endif value="Gramo neto">{{__('Gramo neto')}}</option>
                        <option @if($equipment->unit_measurement == "Decenas") selected @endif value="Decenas">{{__('Decenas')}}</option>
                        <option @if($equipment->unit_measurement == "Cientos") selected @endif value="Cientos">{{__('Cientos')}}</option>
                        <option @if($equipment->unit_measurement == "Docenas") selected @endif value="Docenas">{{__('Docenas')}}</option>
                        <option @if($equipment->unit_measurement == "Caja") selected @endif value="Caja">{{__('Caja')}}</option>
                        <option @if($equipment->unit_measurement == "Botella") selected @endif value="Botella">{{__('Botella')}}</option>
                    </select>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <select class="form-control create-input select-input" name="department_id">
                        <option value="" selected disabled>{{__('Departamento responsble*')}}</option>
                        @foreach ($departments as $department)
                            {{-- <option value="{{$department->id}}">{{$department->name}}</option> --}}
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="number" name="initial_cost" step="0.1" class="form-control create-input @error('initial_cost') is-invalid @endif" value="{{$equipment->initial_cost}}" min="1" placeholder="Costo inicial*" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="category" class="form-control create-input @error('category') is-invalid @endif" value="{{$equipment->category}}" placeholder="Categoría*" required>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <select class="form-control create-input select-input" name="supplier_id" required>
                        <option value="" selected disabled>{{__('Proveedor*')}}</option>
                        @foreach ($suppliers as $supplier)
                            <option @if($equipment->supplier_id == $supplier->id) selected @endif value="{{$supplier->id}}">{{$supplier->business_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="brand" class="form-control create-input @error('brand') is-invalid @enderror" value="{{$equipment->brand}}" placeholder="Marca*" required>
                </div>
                <div class="from-group col-xl-6 mx-auto mb-3">
                    <input type="text" name="model" class="form-control create-input @error('model') is-invalid @enderror" value="{{$equipment->model}}" placeholder="Modelo*" required>
                </div>

                <div class="col-xl-6 mx-auto text-right buttons">
                    <a href="{{route('equipment.index')}}" class="btn btn-link">{{__('CANCELAR')}}</a>
                    <button type="submit" class="btn submit-button py-2 px-5 rounded-0">{{__('ACTUALIZAR')}}</button>
                </div>
            </form>
        </div>
    </div>

@endsection
