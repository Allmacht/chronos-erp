@extends('layouts.app')
@section('title','CHRONOS - EQUIPO')
@section('equipo','side-active')
@section('top-title','EQUIPO')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/show.css')}}">
@endsection
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h4 class="title">{{$equipment->sku." - ".$equipment->description}}</h4>
            </div>
            <div class="col-10 mx-auto mb-4 table-responsive mt-3">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th>{{__('CLAVE')}}</th>
                            <th>{{__('VALOR')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><b>{{__('SKU')}}</b></td>
                            <td>{{$equipment->sku}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Descripción')}}</b></td>
                            <td>{{$equipment->description}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Unidad de medida')}}</b></td>
                            <td>{{$equipment->unit_measurement}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Categoría')}}</b></td>
                            <td>{{$equipment->category}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Cantidad')}}</b></td>
                            <td>{{$equipment->quantity}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Marca')}}</b></td>
                            <td>{{$equipment->brand}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Modelo')}}</b></td>
                            <td>{{$equipment->model}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Departamento responsable')}}</b></td>
                            <td>{{__('PENDIENTE')}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Precio inicial')}}</b></td>
                            <td>{{$equipment->initial_cost}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Proveedor')}}</b></td>
                            <td>{{$equipment->supplier->business_name}}</td>
                        </tr>
                        <tr>
                            <td><b>{{__('Estado')}}</b></td>
                            <td>{{$equipment->status ? 'Disponible' : 'No disponible'}}</td>
                        </tr>
                    </tbody>
                </table>
                <div class="col-12 text-center">
                    <a href="{{route('equipment.index')}}" class="btn back-button rounded-0">{{__('ATRÁS')}}</a>
                </div>
            </div>
        </div>
    </div>

@endsection
