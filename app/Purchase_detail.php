<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase_detail extends Model
{
    public function Equipment(){
        return $this->belogsTo(Equipment::class);
    }
    public function Consumable(){
        return $this->belongsTo(Consumable::class);
    }
}
