<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Validators\ValidationException;
use App\Imports\ConsumablesImport;
use App\Consumable;
use App\Supplier;
use App\Department;

class ConsumablesController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $consumables = Consumable::where(DB::raw("CONCAT(sku,' ',description,' ',type,' ',initial_cost,' ',brand,' ',model)"),'like',"%$search%")->paginate(15);

        return view('Consumables.index',compact('search','consumables'));
    }

    public function create()
    {
        $suppliers = Supplier::all();
        $departments = Department::all();
        if($suppliers->isEmpty())
            return redirect()->route('consumables.index')->withErrors('Ningún proveedor ha sido registrado');

        return view('Consumables.create', compact('suppliers','departments'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'sku'                 => 'required|unique:consumables',
            'description'         => 'required|min:3',
            'type'                => 'required|min:3',
            'initial_cost'        => 'required|numeric',
            'availability_policy' => 'required',
            'supplier_id'         => 'required|numeric',
            'brand'               => 'required|min:2',
            'model'               => 'required|min:2',
        ],[
            'sku.required'                 => 'El SKU es requerido',
            'sku.unique'                   => 'El SKU ingresado ya está en uso',
            'description.required'         => 'La descripción es requerida',
            'description.min'              => 'La descripción debe tener almenos 3 caracteres',
            'type.required'                => 'El tipo es requerido',
            'type.min'                     => 'El tipo debe tener almenos 3 caracteres',
            'initial_cost.required'        => 'El costo inicial es requerido',
            'initial_cost.numeric'         => 'El costo inicial debe ser numérico',
            'availability_policy.required' => 'La Política de disponibilidad es requerida',
            'brand.required'               => 'La marca es requerida',
            'brand.min'                    => 'La marca debe tener almenos 2 caracteres',
            'model.required'               => 'El modelo es requerido',
            'model.min'                    => 'El modelo debe tener almenos 2 caracteres'

        ]);

        $consumable = new Consumable();

        $consumable->sku                 = $request->sku;
        $consumable->description         = $request->description;
        $consumable->unit_measurement    = $request->unit_measurement;
        $consumable->type                = $request->type;
        $consumable->initial_cost        = $request->initial_cost;
        $consumable->availability_policy = $request->availability_policy;
        $consumable->brand               = $request->brand;
        $consumable->model               = $request->model;
        $consumable->supplier_id         = $request->supplier_id;
        $consumable->department_id       = $request->department_id;

        $consumable->save();

        return redirect()->route('consumables.index')->withStatus('Consumible registrado correctamente');

    }

    public function import(Request $request)
    {
        $suppliers = Supplier::all();
        if($suppliers->isEmpty())
            return redirect()->route('consumables.index')->withErrors('Ningún proveedor ha sido registrado');
        return view('Consumables.import');

    }

    public function import_store(Request $request)
    {

        if($request->file('file')):
            if($request->file('file')->getClientOriginalExtension() == 'csv'):

                $file = $request->file('file');
                $name = uniqid() .'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path('/temp');
                $file->move($destinationPath, $name);

                try {
                    Excel::import(new ConsumablesImport, $destinationPath."/".$name);

                } catch (ValidationException $e) {

                    $failures = $e->failures();
                    foreach ($failures as $failure) {
                        foreach ($failure->errors() as $error) {
                            $errors[] = "Hubo un error en la fila ".$failure->row().". ". $error;
                        }
                    }

                    \File::delete(public_path('/temp')."/".$name);
                    return redirect()->route('consumables.import')->withErrors($errors);
                }

                \File::delete(public_path('/temp')."/".$name);

                return redirect()->route('consumables.import')->withStatus('Consumibles importados correctamente');
            else:
                return redirect()->route('consumables.import')->withErrors('El archivo seleccionado no es compatible');
            endif;
        else:
            return redirect()->route('consumables.import')->withErrors('El archivo CSV es requerido');
        endif;

    }

    public function show($id)
    {
        $consumable = Consumable::findOrfail($id);
        return view('Consumables.show', compact('consumable'));
    }

    public function edit($id)
    {
        $consumable = Consumable::findOrfail($id);
        $departments = Department::all();
        $suppliers = Supplier::all();

        return view('Consumables.edit', compact('consumable','suppliers', 'departments'));

    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'sku'                 => ['required',Rule::unique('consumables')->ignore($id)],
            'description'         => 'required|min:3',
            'type'                => 'required|min:3',
            'initial_cost'        => 'required|numeric',
            'type'                => 'required|min:3',
            'availability_policy' => 'required',
            'supplier_id'         => 'required|numeric',
            'brand'               => 'required|min:2',
            'model'               => 'required|min:2',
        ],[
            'sku.required'                 => 'El SKU es requerido',
            'sku.unique'                   => 'El SKU ingresado ya está en uso',
            'description.required'         => 'La descripción es requerida',
            'description.min'              => 'La descripción debe tener almenos 3 caracteres',
            'type.required'                => 'El tipo es requerido',
            'type.min'                     => 'El tipo debe tener almenos 3 caracteres',
            'initial_cost.required'        => 'El costo inicial es requerido',
            'initial_cost.numeric'         => 'El costo inicial debe ser numérico',
            'availability_policy.required' => 'La Política de disponibilidad es requerida',
            'brand.required'               => 'La marca es requerida',
            'brand.min'                    => 'La marca debe tener almenos 2 caracteres',
            'model.required'               => 'El modelo es requerido',
            'model.min'                    => 'El modelo debe tener almenos 2 caracteres'

        ]);

        $consumable = Consumable::findOrfail($id);

        $consumable->sku                 = $request->sku;
        $consumable->description         = $request->description;
        $consumable->unit_measurement    = $request->unit_measurement;
        $consumable->type                = $request->type;
        $consumable->initial_cost        = $request->initial_cost;
        $consumable->availability_policy = $request->availability_policy;
        $consumable->brand               = $request->brand;
        $consumable->model               = $request->model;
        $consumable->supplier_id         = $request->supplier_id;
        $consumable->department_id       = $request->department_id;

        $consumable->save();

        return redirect()->route('consumables.index')->withStatus('Consumible actualizado correctamente');
    }

    public function destroy(Request $request)
    {
        dd($request->id);
    }
}
