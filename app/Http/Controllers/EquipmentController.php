<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Validators\ValidationException;
use App\Imports\EquipmentImport;
use App\Equipment;
use App\Department;
use App\Supplier;

class EquipmentController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $equipment = Equipment::where(DB::raw("CONCAT(sku,' ',description,' ',category,' ',brand,' ',model)"),'like',"%$search%")->paginate(15);
        return view('Equipment.index', compact('search','equipment'));
    }

    public function create()
    {
        $suppliers = Supplier::all();
        if($suppliers->isEmpty())
            return redirect()->route('equipment.index')->withErrors('Ningún proveedor ha sido registrado');

        $departments = Department::all();

        return view('Equipment.create', compact('suppliers','departments'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'sku'              => 'required|unique:equipment',
            'description'      => 'required|min:3',
            'unit_measurement' => 'required|min:3',
            'category'         => 'required|min:3',
            'brand'            => 'required|min:2',
            'initial_cost'     => 'required|numeric|min:1',
            'model'            => 'required|min:3',
            'supplier_id'      => 'required|numeric|exists:suppliers,id',
            'department_id'    => 'sometimes|numeric|exists:departments,id'
        ],[
            'sku.required'              => 'El SKU es requerido',
            'sku.unique'                => 'El SKU ingresado ya está en uso',
            'description.required'      => 'La descripción es requerida',
            'description.min'           => 'La descripción debe tener almenos 3 caracteres',
            'unit_measurement.required' => 'La unidad de medida es requerida',
            'unit_measurement.min'      => 'La unidad de medida debe tener almenos 3 caracteres',
            'category.required'         => 'La categoría es requerida',
            'category.min'              => 'La categoría debe tener almenos 3 caracteres',
            'brand.required'            => 'La marca es requerida',
            'brand.min'                 => 'La marca debe contener almenos 3 caracteres',
            'initial_cost.required'     => 'El costo inicial es requerido',
            'initial_cost.numeric'      => 'El costo inicial debe ser númeico',
            'initial_cost.min'          => 'El costo inicial debe tener un valor mínimo de 1',
            'model.required'            => 'El modelo es requerido',
            'model.min'                 => 'El modelo debe contener almenos 3 caracteres',
            'supplier_id.required'      => 'El ID del proveedor es requerido',
            'supplier_id.numeric'       => 'El ID del proveedor debe ser númerico',
            'supplier_id.exists'        => 'El ID del proveedor no es válido'
        ]);

        $equipment = new Equipment();

        $equipment->sku              = $request->sku;
        $equipment->description      = $request->description;
        $equipment->unit_measurement = $request->unit_measurement;
        $equipment->category         = $request->category;
        $equipment->initial_cost     = $request->initial_cost;
        $equipment->brand            = $request->brand;
        $equipment->model            = $request->model;
        $equipment->supplier_id      = $request->supplier_id;
        $equipment->department_id    = $request->department_id;
        $equipment->quantity         = 0;

        $equipment->save();

        return redirect()->route('equipment.index')->withStatus('Equipo registrado correctamente');

    }

    public function import()
    {
        $suppliers = Supplier::all();
        if($suppliers->isEmpty())
            return redirect()->route('equipment.index')->withErrors('Ningún proveedor ha sido registrado');
        return view('Equipment.import');
    }

    public function import_store(Request $request)
    {
        if($request->file('file')):
            if($request->file('file')->getClientOriginalExtension() == 'csv'):

                $file = $request->file('file');
                $name = uniqid().'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path('/temp');
                $file->move($destinationPath, $name);

                try{
                    Excel::import(new EquipmentImport, $destinationPath.'/'.$name);

                }catch(ValidationException $e){

                    $failures = $e->failures();
                    foreach ($failures as $failure):
                        foreach ($failure->errors() as $error):
                            $errors[] = "Hubo un error en la fila ".$failure->row().". ". $error;
                        endforeach;
                    endforeach;
                    \File::delete(public_path('/temp')."/".$name);
                    return redirect()->route('equipment.import')->withErrors($errors);
                }

                \File::delete(public_path('/temp')."/".$name);
                return redirect()->route('equipment.import')->withStatus('Equipo importado correctamente');
            else:
                return redirect()->route('equipment.import')->withErrors('El archivo selecionado no es compatible');
            endif;
        else:
            return redirect()->route('equipment.import')->withErrors('El archivo CSV es requerido');
        endif;
    }

    public function show($id)
    {
        $equipment = Equipment::findOrfail($id);

        return view('Equipment.show', compact('equipment'));
    }

    public function edit($id)
    {
        $equipment = Equipment::findOrfail($id);
        $suppliers = Supplier::all();
        $departments = Department::all();

        return view('Equipment.edit', compact('equipment','suppliers','departments'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'sku'              => ['required',Rule::unique('equipment')->ignore($id)],
            'description'      => 'required|min:3',
            'unit_measurement' => 'required|min:3',
            'category'         => 'required|min:3',
            'brand'            => 'required|min:2',
            'initial_cost'     => 'required|numeric|min:1',
            'model'            => 'required|min:3',
            'supplier_id'      => 'required|numeric|exists:suppliers,id',
            'department_id'    => 'sometimes|numeric|exists:departments,id'
        ],[
            'sku.required'              => 'El SKU es requerido',
            'sku.unique'                => 'El SKU ingresado ya está en uso',
            'description.required'      => 'La descripción es requerida',
            'description.min'           => 'La descripción debe tener almenos 3 caracteres',
            'unit_measurement.required' => 'La unidad de medida es requerida',
            'unit_measurement.min'      => 'La unidad de medida debe tener almenos 3 caracteres',
            'category.required'         => 'La categoría es requerida',
            'category.min'              => 'La categoría debe tener almenos 3 caracteres',
            'brand.required'            => 'La marca es requerida',
            'brand.min'                 => 'La marca debe contener almenos 3 caracteres',
            'initial_cost.required'     => 'El costo inicial es requerido',
            'initial_cost.numeric'      => 'El costo inicial debe ser númeico',
            'model.required'            => 'El modelo es requerido',
            'model.min'                 => 'El modelo debe contener almenos 3 caracteres',
            'supplier_id.required'      => 'El ID del proveedor es requerido',
            'supplier_id.numeric'       => 'El ID del proveedor debe ser númerico',
            'supplier_id.exists'        => 'El ID del proveedor no es válido'
        ]);

        $equipment = Equipment::findOrfail($id);

        $equipment->sku              = $request->sku;
        $equipment->description      = $request->description;
        $equipment->unit_measurement = $request->unit_measurement;
        $equipment->category         = $request->category;
        $equipment->initial_cost     = $request->initial_cost;
        $equipment->brand            = $request->brand;
        $equipment->department_id    = $request->department_id;
        $equipment->supplier_id      = $request->supplier_id;

        $equipment->update();

        return redirect()->route('equipment.index')->withStatus('Elemento actualizado correctamente');
    }

    public function destroy(Request $request)
    {
        dd($request->id);
    }

}
