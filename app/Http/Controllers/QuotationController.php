<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Quotation;
use App\Quotation_detail;
use App\Client;

class QuotationController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $quotations = Quotation::select('*', 'quotations.id as id')
                                ->join('clients','quotations.client_id','clients.id')
                                ->where(DB::raw("CONCAT(quotations.folio,' ',quotations.date,' ',clients.tradename,' ',quotations.amount,' ',quotations.effect)"),'like',"%$search%")
                                ->paginate(15);

        return view('Quotations.index', compact('search','quotations'));
    }

    public function create()
    {
        $clients = Client::all();
        return view('Quotations.create', compact('clients'));
    }

    public function get_quotation($id){
        $quotation = Quotation::whereId($id)->first();
        if(is_null($quotation))
            return response()->json(['error' => 'invalid ID'], 400);

        $details = array();
        foreach ($quotation->details as $detail) {
            $detail->service_name = $detail->service->name;
            array_push($details, $detail);
        }

        return response()->json(['details' => $details,'client_id' => $quotation->client_id],200);
    }

    public function store(Request $request)
    {
        $check_folio = false;
        do{
            $folio = $this->code();
            $folio = "COT".$folio;
            $quotation_verify = Quotation::whereFolio($folio)->first();
            $check_folio = is_null($quotation_verify);
        }while($check_folio == false);


        $data_quotation = json_decode($request->quotation);

        $quotation = new Quotation();
        $quotation->folio      = $folio;
        $quotation->date       = $data_quotation->date;
        $quotation->conditions = $data_quotation->conditions;
        $quotation->effect     = $data_quotation->effect;
        $quotation->amount     = $data_quotation->amount;
        $quotation->client_id  = $data_quotation->client_id;

        $quotation->save();

        $data_services = json_decode($request->services);

        foreach ($data_services as $service) {
            $detail = new Quotation_detail();
            $detail->quotation_id = $quotation->id;
            $detail->service_id   = $service->id;
            $detail->quantity     = $service->quantity;
            $detail->price        = $service->price;
            $detail->subtotal     = $service->subtotal;

            $detail->save();
        }

        return response()->json(['status' => true], 200);
    }

    public function code()
    {
        $code = (string)rand(0,999999);
        if (strlen($code)<6) {
            $code = str_pad($code, 6, "0", STR_PAD_LEFT);
        }
        return $code;
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy(Request $request)
    {
        $quotation = Quotation::findOrfail($request->id);
        foreach ($quotation->details as $detail) {
            $detail->delete();
        }
        $quotation->delete();

        return redirect()->route('quotations.index')->withStatus('Cotización eliminada correctamente');
    }
}
