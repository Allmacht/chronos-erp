<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Expenditure;
use App\Supplier;

class ExpenditureController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $expenditures = Expenditure::select('*','expenditures.id as id')
                                ->join('suppliers','expenditures.supplier_id','suppliers.id')
                                ->where(DB::raw("CONCAT(expenditures.folio,' ',expenditures.date,' ',suppliers.business_name,' ',expenditures.concept,' ',expenditures.amount)"),'like',"%$search%")
                                ->paginate(15);
        return view('Expenditures.index', compact('search','expenditures'));
    }

    public function create()
    {
        $suppliers = Supplier::whereType('Gastos')->get();
        if($suppliers->isEmpty())
            return redirect()->route('expenditures.index')->withErrors('Sin proveedores disponibles');
        return view('Expenditures.create', compact('suppliers'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'date'        => 'required|date|date_format:"Y-m-d"',
            'supplier_id' => 'required|numeric|exists:suppliers,id',
            'concept'     => 'required',
            'amount'      => 'required|numeric|min:1',
            'file'        => 'nullable|file'
        ],[
            'date.required'        => 'La fecha es requerida',
            'date.date'            => 'La fecha ingresada no es válida',
            'date.date_format'     => 'El formato de la fecha no es válido',
            'supplier_id.required' => 'El proveedor es requerido',
            'supplier_id.numeric'  => 'El valor del proveedor no es válido',
            'supplier_id.exists'   => 'El proveedor ingresado no es válido',
            'concept.required'     => 'El concepto es requerido',
            'amount.required'      => 'El importe es requerido',
            'amount.numeric'       => 'El importe ingresado no es válido',
            'amount.min'           => 'La cantidad mínima del importe es 1',
            'file.file'            => 'El archivo seleccionado no es válido'
        ]);

        $status = false;
        do{
            $folio = $this->code();
            $folio = "GA".$folio;
            $expenditure_verify = Expenditure::whereFolio($folio)->first();
            $status = is_null($expenditure_verify);
        }while($status == false);

        $expenditure = new Expenditure();
        $expenditure->folio       = $folio;
        $expenditure->date        = $request->date;
        $expenditure->supplier_id = $request->supplier_id;
        $expenditure->user_id     = Auth::user()->id;
        $expenditure->concept     = $request->concept;
        $expenditure->amount      = $request->amount;

        if($request->file('file')):
            $file = $request->file('file');
            $name = uniqid().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/expenditure_attachments');
            $file->move($destinationPath, $name);

            $expenditure->attachment = $name;
        endif;

        $expenditure->save();

        return redirect()->route('expenditures.index')->withStatus('Gasto registrado correctamente');

    }

    public function code()
    {
        $code = (string)rand(0,99999999);
        if (strlen($code)<5) {
            $code = str_pad($code, 8, "0", STR_PAD_LEFT);
        }
        return $code;
    }

    public function show($id)
    {
        $expenditure = Expenditure::findOrfail($id);
        return view('Expenditures.show', compact('expenditure'));
    }

    public function edit($id)
    {
        $suppliers = Supplier::whereType('Gastos')->get();
        $expenditure = Expenditure::findOrfail($id);

        return view('Expenditures.edit', compact('suppliers','expenditure'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'date'        => 'required|date|date_format:"Y-m-d"',
            'supplier_id' => 'required|numeric|exists:suppliers,id',
            'concept'     => 'required',
            'amount'      => 'required|numeric|min:1',
            'file'        => 'nullable|file'
        ],[
            'date.required'        => 'La fecha es requerida',
            'date.date'            => 'La fecha ingresada no es válida',
            'date.date_format'     => 'El formato de la fecha no es válido',
            'supplier_id.required' => 'El proveedor es requerido',
            'supplier_id.numeric'  => 'El valor del proveedor no es válido',
            'supplier_id.exists'   => 'El proveedor ingresado no es válido',
            'concept.required'     => 'El concepto es requerido',
            'amount.required'      => 'El importe es requerido',
            'amount.numeric'       => 'El importe ingresado no es válido',
            'amount.min'           => 'La cantidad mínima del importe es 1',
            'file.file'            => 'El archivo seleccionado no es válido'
        ]);

        $expenditure = Expenditure::findOrfail($id);
        $expenditure->date        = $request->date;
        $expenditure->supplier_id = $request->supplier_id;
        $expenditure->concept     = $request->concept;
        $expenditure->amount      = $request->amount;

        if($request->file('file')):
            \File::delete(public_path('/expenditure_attachments')."/".$expenditure->attachment);
            $file = $request->file('file');
            $name = uniqid().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/expenditure_attachments');
            $file->move($destinationPath, $name);

            $expenditure->attachment = $name;
        endif;

        $expenditure->update();
        return redirect()->route('expenditures.index')->withStatus('Gasto actualizado correctamente');
    }

    public function destroy(Request $request)
    {
        $expenditure = Expenditure::findOrfail($request->id);
        \File::delete(public_path('/expenditure_attachments')."/".$expenditure->attachment);
        $expenditure->delete();

        return redirect()->route('expenditures.index')->withStatus('Elemento borrado correctamente');
    }
}
