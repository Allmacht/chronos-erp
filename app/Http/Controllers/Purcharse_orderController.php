<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Purchase_order;
use App\Purchase_detail;
use App\Approved_order;
use App\Paid_order;
use App\Supplier;
use App\Consumable;
use App\Equipment;

class Purcharse_orderController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $orders = Purchase_order::select('*','purchase_orders.id as id')
                                ->where(DB::raw("CONCAT(purchase_orders.folio,' ',purchase_orders.date,' ',purchase_orders.amount,' ',suppliers.business_name)"),'like',"%$search%")
                                ->join('suppliers','purchase_orders.supplier_id','suppliers.id')
                                ->paginate(15);

        return view('Purchase_orders.index', compact('orders','search'));
    }

    public function create()
    {
        $suppliers = Supplier::whereType('Compras')->get();
        $message = "Orden creada correctamente";
        if($suppliers->isEmpty())
            return redirect()->route('purchase_orders.index')->withErrors('No existen proveedores registrados');
        return view('Purchase_orders.create',compact('suppliers','message'));
    }

    public function getItems(Request $request, $id)
    {

            $items = [];
            $consumables = Consumable::whereSupplier_id($id)->get();
            $equipment = equipment::whereSupplier_id($id)->get();

            foreach ($consumables as $consumable) {
                $items[] = $consumable;
            }
            foreach ($equipment as $equipment) {
                $items[] = $equipment;
            }
            return response()->json(['items' => $items], 200);

    }

    public function getItem(Request $request, $sku)
    {

            $item = Consumable::whereSku($sku)->first();

            if(is_null($item)):
                $item = Equipment::whereSku($sku)->first();
            endif;

            return response(['item' => $item], 200);

    }

    public function store(Request $request)
    {
        if($request->ajax()):
            if($request->file('file')):

                $file = $request->file('file');
                $name = uniqid() .'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path('/order_invoice');
                $file->move($destinationPath, $name);

                $items = json_decode($request->items);
                $order = new Purchase_order();
                $order->date        = $request->date;
                $order->amount      = $request->amount;
                $order->invoice     = $name;
                $order->supplier_id = $request->supplier_id;

                $status = false;
                do{
                    $folio = $this->code();
                    $folio = "OC".$folio;
                    $order_verify = Purchase_order::whereFolio($folio)->first();
                    $status = is_null($order_verify);

                }while($status == false);

                $order->folio = "OC".$folio;

                $total = 0;
                foreach ($items as $item) {
                    $total += $item->quantity;
                }

                $order->total_quantity = $total;

                $order->save();

                foreach ($items as $item) {
                    $detail = new Purchase_detail;
                    $detail->quantity          = $item->quantity;
                    $detail->amount            = $item->initial_cost;
                    $detail->subtotal          = $item->subtotal;
                    $detail->purchase_order_id = $order->id;

                    $item_foreign = Consumable::whereId($item->id)->whereSku($item->sku)->first();
                    if(is_null($item_foreign)):
                        $item_foreign = Equipment::whereId($item->id)->whereSku($item->sku)->first();
                        $detail->equipment_id = $item_foreign->id;
                    else:
                        $detail->consumable_id = $item_foreign->id;
                    endif;

                    $detail->description = $item_foreign->description;
                    $detail->sku         = $item_foreign->sku;
                    $detail->save();
                }

                return response()->json(['status' => 200, 'message' => 'OK'], 200);

            endif;
        endif;
    }

    public function code()
    {
        $code = (string)rand(0,99999999);
        if (strlen($code)<8) {
            $code = str_pad($code, 8, "0", STR_PAD_LEFT);
        }
        return $code;
    }

    public function show($id)
    {
        $purchase_order = Purchase_order::findOrfail($id);
        $approved = false;
        $paid = false;

        if($purchase_order->approved):
            $approved = Approved_order::wherePurchase_order_id($purchase_order->id)->first();
        endif;

        if($purchase_order->paid):
            $paid = Paid_order::wherePurchase_order_id($purchase_order->id)->first();
        endif;
        return view('Purchase_orders.show', compact('purchase_order','approved','paid'));
    }

    public function edit($id)
    {
        $order = Purchase_order::findOrfail($id);
        if($order->approved)
            return redirect()->route('purchase_orders.index')->withErrors('La orden ya fue aprovada, no se puede modificar');

        $suppliers = Supplier::select('id','business_name')->whereType('Compras')->get();
        $details = $order->details;
        $items = [];
        $consumables = Consumable::whereSupplier_id($order->supplier_id)->get();
        $equipment = equipment::whereSupplier_id($order->supplier_id)->get();

        foreach ($consumables as $consumable) {
            $items[] = $consumable;
        }
        foreach ($equipment as $equipment) {
            $items[] = $equipment;
        }
        $message = "Orden actualizada correctamente";
        return view('Purchase_orders.edit', compact('order','details','suppliers','items','message'));
    }

    public function update(Request $request, $id)
    {
        if($request->ajax()):
            $order = Purchase_order::findOrfail($id);
            if($request->file('file')):

                \File::delete(public_path('/order_invoice')."/".$order->invoice);
                $file = $request->file('file');
                $name = uniqid() .'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path('/order_invoice');
                $file->move($destinationPath, $name);
            endif;
            $current_items = $order->details;
            foreach ($current_items as $current_item) {
                $current_item->delete();
            }

            $order->date        = $request->date;
            $order->amount      = $request->amount;
            $order->invoice     = $name ?? $order->invoice;
            $order->supplier_id = $request->supplier_id;

            $items = json_decode($request->items);
            $total = 0;
            foreach ($items as $item) {
                $detail = new Purchase_detail;
                $detail->quantity          = $item->quantity;
                $detail->amount            = $item->amount;
                $detail->subtotal          = $item->subtotal;
                $detail->purchase_order_id = $order->id;

                $item_foreign = Consumable::whereSku($item->sku)->first();
                if(is_null($item_foreign)):
                    $item_foreign = Equipment::whereSku($item->sku)->first();
                    $detail->equipment_id = $item_foreign->id;
                else:
                    $detail->consumable_id = $item_foreign->id;
                endif;

                $detail->description = $item_foreign->description;
                $detail->sku         = $item_foreign->sku;
                $detail->save();
                $total += $item->quantity;
            }

            $order->total_quantity = $total;
            $order->save();
            return response()->json(['status' => 200, 'message' => 'OK'], 200);

        endif;
    }

    public function approve(Request $request)
    {
        $purchase_order = Purchase_order::findOrfail($request->id);

        $approved_order = new Approved_order();
        $approved_order->user_id           = Auth::user()->id;
        $approved_order->purchase_order_id = $purchase_order->id;
        $approved_order->comments          = $request->comments;

        $purchase_order->approved = true;

        $approved_order->save();
        $purchase_order->update();

        return redirect()->route('purchase_orders.index')->withStatus('Orden aprobada correctamente');
    }

    public function paid(Request $request)
    {
        $purchase_order = Purchase_order::findOrfail($request->id);
        $purchase_order->paid = true;

        $paid_order = new Paid_order();
        $paid_order->user_id           = Auth::user()->id;
        $paid_order->purchase_order_id = $purchase_order->id;
        $paid_order->comments          = $request->comments;

        $purchase_order->update();
        $paid_order->save();

        return redirect()->route('purchase_orders.index')->withStatus('El status de pago fue actualizado correctamente');
    }

    public function destroy(Request $request)
    {
        $purchase_order = Purchase_order::findOrfail($request->id);
        if($purchase_order->paid == false && $purchase_order->approved == false){
            foreach ($purchase_order->details as $detail) {
                $detail->delete();
            }
            $purchase_order->delete();

            return redirect()->route('purchase_orders.index')->withStatus('La orden fue eliminada correctamente');
        }else{
            return redirect()->route('purchase_orders.index')->withErrors('La orden no puede ser eliminada');
        }
    }
}
