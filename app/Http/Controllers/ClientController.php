<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Client;
use App\Contacts_by_client;
use App\Product_data;
use App\Contracted_service;
use App\Service;
use App\Client_operation;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $clients = Client::select('*','clients.id as id','clients.phone as phone')
                            ->join('contacts_by_clients','contacts_by_clients.client_id','clients.id')
                            ->where('contacts_by_clients.type','manager')
                            ->where(DB::raw("CONCAT(clients.tradename,' ',clients.state,' ',contacts_by_clients.names,' ',clients.email,' ',clients.phone)"),'like',"%$search%")
                            ->paginate(15);

        return view('Clients.index',compact('search','clients'));
    }

    public function create()
    {
        $services = Service::all();
        return view('Clients.create', compact('services'));
    }

    public function check_rfc($rfc)
    {
        $check = Client::whereRfc($rfc)->first();
        if(is_null($check))
            return response()->json(['response' => true], 200);
        return response()->json(['response' => false], 200);
    }

    public function price_list($id)
    {
        $price_list = Client::whereId($id)->value('price_list');
        if(!is_null($price_list))
            return response()->json(['price_list' => $price_list],200);

        return response()->json(['error' => 'ID is not valid'],400);
    }

    public function store(Request $request)
    {
        //REGISTRO DE CLIENTE
        $data_client = json_decode($request->client);

        $client = new Client();
        $client->business        = $data_client->business;
        $client->tradename       = $data_client->tradename;
        $client->business_name   = $data_client->business_name;
        $client->rfc             = $data_client->rfc;
        $client->country         = $data_client->country;
        $client->state           = $data_client->state;
        $client->municipality    = $data_client->municipality;
        $client->neighborhood    = $data_client->neighborhood;
        $client->street          = $data_client->street;
        $client->external_number = $data_client->external_number;
        $client->internal_number = $data_client->internal_number;
        $client->zipcode         = $data_client->zipcode;
        $client->phone           = $data_client->phone;
        $client->fax             = $data_client->fax;
        $client->price_list      = $data_client->price_list;
        $client->cfdi            = $data_client->cfdi;
        $client->bank_account    = $data_client->bank_account;
        $client->bank            = $data_client->bank;
        $client->email           = $data_client->email;
        $client->invoice_email   = $data_client->invoice_email;
        $client->credit_days     = $data_client->credit_days;

        $client->save();

        // FIN DE REGISTRO DE CLIENTE

        // CONTACTO DIRECTOR O GERENTE DE CONTACTO
        $data_director = json_decode($request->director);
        $director = new Contacts_by_client();

        $director->type      = $data_director->type;
        $director->names     = $data_director->name;
        $director->position  = $data_director->position;
        $director->email     = $data_director->email;
        $director->phone     = $data_director->phone;
        $director->client_id = $client->id;

        $director->save();

        //CONTACTO RESPONSABLE COMERCIAL
        $data_manger = json_decode($request->manager);
        $manager = new Contacts_by_client();

        $manager->type      = $data_manger->type;
        $manager->names     = $data_manger->name;
        $manager->position  = $data_manger->position;
        $manager->email     = $data_manger->email;
        $manager->phone     = $data_manger->phone;
        $manager->client_id = $client->id;

        $manager->save();

        //CONTACTO RESPONSABLE DE OPERACIONES / LOGISTICA
        $data_operations = json_decode($request->operations);
        $operations = new Contacts_by_client();

        $operations->type      = $data_operations->type;
        $operations->names     = $data_operations->name;
        $operations->position  = $data_operations->position;
        $operations->email     = $data_operations->email;
        $operations->phone     = $data_operations->phone;
        $operations->client_id = $client->id;

        $operations->save();

        //CONTACTO RESPONSABLE DEL COMERCIO EXTERIOR
        $data_foreign = json_decode($request->foreign);
        $foreign = new Contacts_by_client();

        $foreign->type      = $data_foreign->type;
        $foreign->names     = $data_foreign->name;
        $foreign->position  = $data_foreign->position;
        $foreign->email     = $data_foreign->email;
        $foreign->phone     = $data_foreign->phone;
        $foreign->client_id = $client->id;

        $foreign->save();

        // DETALLES DE PRODUCTO
        $data_product_details = json_decode($request->product_data);
        $product_details = new Product_data();

        if($request->file('catalog_availability')):
            $catalog_availability = $request->file('catalog_availability');
            $file_name = uniqid().'.'.$catalog_availability->getClientOriginalExtension();
            $destination_path = public_path('/product_data/files');
            $catalog_availability->move($destination_path, $file_name);

            $product_details->catalog_availability = $file_name;
        endif;

        $product_details->client_id              = $client->id;
        $product_details->general_description    = $data_product_details->general_description;
        $product_details->datasheet_availability = $data_product_details->datasheet_availability;
        $product_details->business_type          = $data_product_details->business_type;
        $product_details->product_type           = $data_product_details->product_type;
        $product_details->product_rotation       = $data_product_details->product_rotation;

        $product_details->save();


        // SERVICIOS CONTRATADOS
        $data_services = json_decode($request->selected_services);
        foreach ($data_services as $service) {
            $contracted_service = new Contracted_service();

            $contracted_service->client_id  = $client->id;
            $contracted_service->service_id = $service->id;
            $contracted_service->price      = $service->price;

            $contracted_service->save();
        }

        // OPERACION DEL CLIENTE
        $data_client_operation = json_decode($request->client_operations);
        $client_operation = new Client_operation();

        if($request->file('delivery_schedule')):
            $delivery_schedule = $request->file('delivery_schedule');
            $delivery_schedule_name = uniqid().'.'.$delivery_schedule->getClientOriginalExtension();
            $destination_path = public_path('client_operations/delivery_schedules');
            $delivery_schedule->move($destination_path, $delivery_schedule_name);

            $client_operation->delivery_schedule = $delivery_schedule_name;
        endif;

        if($request->file('dangerous_material')):
            $dangerous_material = $request->file('dangerous_material');
            $dangerous_material_name = uniqid().'.'.$dangerous_material->getClientOriginalExtension();
            $destination_path = public_path('client_operations/dangerous_materials');
            $dangerous_material->move($destination_path, $dangerous_material_name);

            $client_operation->dangerous_material = $dangerous_material_name;
        endif;

        $client_operation->packing_type            = $data_client_operation->packing_type;
        $client_operation->packing_measures        = $data_client_operation->packing_measures;
        $client_operation->transport_type          = $data_client_operation->transport_type;
        $client_operation->product_handling        = $data_client_operation->product_handling;
        $client_operation->storage_conditions      = $data_client_operation->storage_conditions;
        $client_operation->product_value           = $data_client_operation->product_value;
        $client_operation->foreign_commerce        = $data_client_operation->foreign_commerce;
        $client_operation->import                  = $data_client_operation->import;
        $client_operation->export                  = $data_client_operation->export;
        $client_operation->foreign_commerce_number = $data_client_operation->foreign_commerce_number;
        $client_operation->tariff_fraction         = $data_client_operation->tariff_fraction;
        $client_operation->customs_agent           = $data_client_operation->customs_agent;
        $client_operation->quality_inspection      = $data_client_operation->quality_inspection;
        $client_operation->importers               = $data_client_operation->importers;
        $client_operation->fragile                 = $data_client_operation->fragile;
        $client_operation->client_id               = $client->id;

        $client_operation->save();



        return response()->json(['status' => true], 200);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
