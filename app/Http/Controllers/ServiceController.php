<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Service;

class ServiceController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $services = Service::where(DB::raw("CONCAT(services.key,' ',services.name,' ',services.modality)"),'like',"%$search%")
                            ->paginate(15);

        return view('Services.index', compact('search','services'));
    }

    public function get_services($price)
    {
        $price_list = "";
        if($price == 'Regular')
            $price_list = 'regular_price';
        if($price == 'Premium')
            $price_list = 'premium_price';
        if($price == 'Socio')
            $price_list = 'partner_price';

        if($price_list != ""){
            $services = Service::select('id','name','key',$price_list." as price")->get();
            return response()->json(['status' => '200','services' => $services], 200);
        }else{
            return response()->json(['status' => '400','error' => 'price option is not valid'], 400);
        }
    }

    public function create()
    {
        return view('Services.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'key'           => 'required|unique:services,key',
            'name'          => 'required|unique:services,name',
            'modality'      => 'required',
            'unit'          => 'nullable', //TEMPORAL PENDIENTE DE CATALOGO
            'regular_price' => 'required|numeric|min:1',
            'premium_price' => 'required|numeric|min:1',
            'partner_price' => 'required|numeric|min:1'
        ],[
            'key.required'          => 'La clave del servicio es requerida',
            'key.unique'            => 'La clave del servicio ya está en uso',
            'name.required'         => 'El nombre del servicio es requerido',
            'name.unique'           => 'El nombre del servicio ya está en uso',
            'modality.required'     => 'La modalidad es requerida',
            'unit.required'         => 'La unidad es requerida',
            'regular_price.requied' => 'El precio regular es requerido',
            'regular_price.numeric' => 'El precio regular no es válido',
            'regular_price.min'     => 'El valor mínimo del precio regular es de 1',
            'premium_price.requied' => 'El precio premium es requerido',
            'premium_price.numeric' => 'El precio premium no es válido',
            'premium_price.min'     => 'El valor mínimo del precio premium es de 1',
            'partner_price.requied' => 'El precio socio es requerido',
            'partner_price.numeric' => 'El precio socio no es válido',
            'partner_price.min'     => 'El valor mínimo del precio socio es de 1'
        ]);

        $service = new Service();
        $service->key = $request->key;
        $service->name = $request->name;
        $service->modality = $request->modality;
        $service->unit = $request->unit;
        $service->regular_price = $request->regular_price;
        $service->premium_price = $request->premium_price;
        $service->partner_price = $request->partner_price;

        $service->save();

        return redirect()->route('services.index')->withStatus('Servicio registrado correctamente');

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $service = Service::findOrfail($id);
        return view('Services.edit', compact('service'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'key'           => ['required',Rule::unique('services')->ignore($id)],
            'name'          => ['required',Rule::unique('services')->ignore($id)],
            'modality'      => 'required',
            'unit'          => 'nullable', //TEMPORAL PENDIENTE DE CATALOGO
            'regular_price' => 'required|numeric|min:1',
            'premium_price' => 'required|numeric|min:1',
            'partner_price' => 'required|numeric|min:1'
        ],[
            'key.required'          => 'La clave del servicio es requerida',
            'key.unique'            => 'La clave del servicio ya está en uso',
            'name.required'         => 'El nombre del servicio es requerido',
            'name.unique'           => 'El nombre del servicio ya está en uso',
            'modality.required'     => 'La modalidad es requerida',
            'unit.required'         => 'La unidad es requerida',
            'regular_price.requied' => 'El precio regular es requerido',
            'regular_price.numeric' => 'El precio regular no es válido',
            'regular_price.min'     => 'El valor mínimo del precio regular es de 1',
            'premium_price.requied' => 'El precio premium es requerido',
            'premium_price.numeric' => 'El precio premium no es válido',
            'premium_price.min'     => 'El valor mínimo del precio premium es de 1',
            'partner_price.requied' => 'El precio socio es requerido',
            'partner_price.numeric' => 'El precio socio no es válido',
            'partner_price.min'     => 'El valor mínimo del precio socio es de 1'
        ]);

        $service = Service::findOrfail($id);
        $service->key = $request->key;
        $service->name = $request->name;
        $service->modality = $request->modality;
        $service->unit = $request->unit;
        $service->regular_price = $request->regular_price;
        $service->premium_price = $request->premium_price;
        $service->partner_price = $request->partner_price;

        $service->update();

        return redirect()->route('services.index')->withStatus('Servicio actualizado correctamente');
    }

    public function destroy(Request $request)
    {
        $service = Service::findOrfail($request->id);
        $service->delete();

        return redirect()->route('services.index')->withStatus('Servicio eliminado correctamente');
    }
}
