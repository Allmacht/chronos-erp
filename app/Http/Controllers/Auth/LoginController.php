<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request){

        $email = $request->email;
        $password = $request->password;

        $checkUser = User::whereEmail($email)->first();
        if(is_null($checkUser)){
            return redirect()->route('login')->withErrors(['email' => 'Usuario y/o contraseña incorrecto']);
        }elseif($checkUser->status == false){
            return redirect()->route('login')->withErrors(['email' => 'Usuario desactivado, consulte al Administrador']);
        }elseif(Auth::attempt(['status' => 1,'email' => $email, 'password' => $password])){
            return redirect()->intended('home');
        }else{
            return redirect()->route('login')->withErrors(['email' => 'Usuario y/o contraseña incorrecto']);
        }

    }
}
