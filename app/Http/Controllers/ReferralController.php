<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Client;
use App\Referral;
use App\Quotation;
use App\Quotation_detail;

class ReferralController extends Controller
{

    public function index(Request $request)
    {
        $search = $request->input('search');
        $referrals = Referral::select('*','referrals.id as id','referrals.folio as folio','referrals.date as date')
                        ->join('clients','referrals.client_id','clients.id')
                        ->join('quotations','referrals.quotation_id','quotations.id')
                        ->where(DB::raw("CONCAT(referrals.folio,' ',referrals.date,' ',clients.tradename,' ',referrals.transaction_type,' ',quotations.amount)"),'like',"%$search%")
                        ->paginate(15);
        
        return view('Referrals.index', compact('search','referrals'));
    }

    public function create()
    {
        $quotations = Quotation::all();
        $clients = Client::all();

        return view('Referrals.create',compact('quotations','clients'));
    }

    public function store(Request $request)
    {

        $data_quotation_details = json_decode($request->quotation_details);
        $data_referral = json_decode($request->referral);

        $quotation_id = $request->quotation;

        if($quotation_id == ""){
            $check_folio = false;
            do{
                $folio = $this->code();
                $folio = "COT".$folio;
                $quotation_verify = Quotation::whereFolio($folio)->first();
                $check_folio = is_null($quotation_verify);
            }while($check_folio == false);

            $quotation = new Quotation();
            $quotation->folio      = $folio;
            $quotation->date       = $data_referral->date;
            $quotation->conditions = "N/A";
            $quotation->effect     = $data_referral->date;
            $quotation->client_id  = $data_referral->client_id;
            $quotation->amount     = $request->total;

            $quotation->save();

            $quotation_id = $quotation->id;

            foreach ($data_quotation_details as $detail) {
                $quotation_detail = new Quotation_detail();
                $quotation_detail->quotation_id = $quotation->id;
                $quotation_detail->service_id   = $detail->id;
                $quotation_detail->quantity     = $detail->quantity;
                $quotation_detail->price        = $detail->price;
                $quotation_detail->subtotal     = $detail->subtotal;

                $quotation_detail->save();
            }
        }

        $check_folio = false;
        do{
            $folio = $this->code();
            $folio = "VE".$folio;
            $quotation_verify = Referral::whereFolio($folio)->first();
            $check_folio = is_null($quotation_verify);
        }while($check_folio == false);

        $referral = new Referral();
        $referral->folio               = $folio;
        $referral->date                = $data_referral->date;
        $referral->quotation_id        = $quotation_id;
        $referral->client_id           = $data_referral->client_id;
        $referral->transaction_type    = $data_referral->transaction_type;
        $referral->movement_type       = $data_referral->movement_type;
        $referral->format              = $data_referral->format;
        $referral->customs_declaration = $data_referral->customs_declaration;
        $referral->notice_type         = $data_referral->notice_type;
        $referral->notice              = $data_referral->notice;
        $referral->license_plate       = $data_referral->license_plate;
        $referral->container           = $data_referral->container;

        if($request->file('invoice')){
            $file = $request->file('invoice');
            $filename = uniqid().'.'.$file->getClientOriginalExtension();
            $destination_path = public_path('/referrals_files/files');
            $file->move($destination_path, $filename);

            $referral->invoice = $filename;
        }

        $referral->save();



        return response()->json(['status' => 200],200);
    }

    public function code()
    {
        $code = (string)rand(0,999999);
        if (strlen($code)<6) {
            $code = str_pad($code, 6, "0", STR_PAD_LEFT);
        }
        return $code;
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
