<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Handbill;

class HandbillController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('request');
        $handbills = Handbill::where(DB::raw("CONCAT(business,' ',name,' ',phone,' ',email, ' ',contact)"),'like',"%$search%")
                                ->paginate(15);

        return view('Handbills.index', compact('search','handbills'));
    }

    public function create()
    {
        return view('Handbills.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'business' => 'required',
            'name'     => 'required',
            'phone'    => 'required|numeric|digits:10',
            'email'    => 'required|email',
            'contact'  => 'required',
            'position' => 'required'
        ],[
            'business.required' => 'El giro es requerido',
            'name.required'     => 'El nombre comercial es requerido',
            'phone.required'    => 'El teléfono es requerido',
            'phone.numeric'     => 'El teléfono ingresado no es válido',
            'phone.digits'      => 'El teléfono debe contener 10 digitos'
        ]);

        $handbill = new Handbill();
        $handbill->business = $request->business;
        $handbill->name     = $request->name;
        $handbill->phone    = $request->phone;
        $handbill->email    = $request->email;
        $handbill->contact  = $request->contact;
        $handbill->position = $request->position;

        $handbill->save();

        return redirect()->route('handbills.index')->withStatus('Prospecto registrado correctamente');
    }

    public function edit($id)
    {
        $handbill = Handbill::findOrfail($id);
        return view('Handbills.edit', compact('handbill'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'business' => 'required',
            'name'     => 'required',
            'phone'    => 'required|numeric|digits:10',
            'email'    => 'required|email',
            'contact'  => 'required',
            'position' => 'required'
        ],[
            'business.required' => 'El giro es requerido',
            'name.required'     => 'El nombre comercial es requerido',
            'phone.required'    => 'El teléfono es requerido',
            'phone.numeric'     => 'El teléfono ingresado no es válido',
            'phone.digits'      => 'El teléfono debe contener 10 digitos'
        ]);

        $handbill = Handbill::findOrfail($id);
        $handbill->business = $request->business;
        $handbill->name     = $request->name;
        $handbill->phone    = $request->phone;
        $handbill->email    = $request->email;
        $handbill->contact  = $request->contact;
        $handbill->position = $request->position;

        $handbill->update();

        return redirect()->route('handbills.index')->withStatus('Prospecto actualizado correctamente');
    }

    public function destroy(Request $request)
    {
        $handbill = Handbill::findOrfail($request->id);
        $handbill->delete();

        return redirect()->route('handbills.index')->withStatus('Elemento eliminado correctamente');
    }
}
