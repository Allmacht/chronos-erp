<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Income_order;
use App\Consumable;
use App\Equipment;
use App\Purchase_order;

class Income_orderController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $income_orders = Income_order::select('*','income_orders.id as id','income_orders.folio as folio_income')
                                        ->join('purchase_orders','income_orders.purchase_order_id','purchase_orders.id')
                                        ->join('suppliers','purchase_orders.supplier_id','suppliers.id')
                                        ->where(DB::raw("CONCAT(income_orders.folio,' ',income_orders.date,' ',purchase_orders.folio,' ',suppliers.business_name)"),'like',"%$search%")
                                        ->paginate(15);

        return view('Income_orders.index', compact('search','income_orders'));
    }

    public function create()
    {
        $purchase_orders = Purchase_order::whereApproved(true)->whereDelivered(false)->get();
        if($purchase_orders->isEmpty())
            return redirect()->route('income_orders.index')->withErrors('No existen órdenes de compra disponibles');
        return view('Income_orders.create', compact('purchase_orders'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'date'              => 'required|date|date_format:"Y-m-d"',
            'purchase_order_id' => 'required|numeric|exists:purchase_orders,id'
        ],[
            'date.required'    => 'La fecha es requerida',
            'date.date'        => 'La fecha ingresada no es válida',
            'date.date_format' => 'El formato de la fecha ingresada no es válido'
        ]);

        $status = false;
        do{
            $folio = $this->code();
            $folio = "OE".$folio;
            $income_verify = Income_order::whereFolio($folio)->first();
            $status = is_null($income_verify);
        }while($status == false);

        $income_order = new Income_order();
        $income_order->date              = $request->date;
        $income_order->folio             = $folio;
        $income_order->user_id           = Auth::user()->id;
        $income_order->purchase_order_id = $request->purchase_order_id;

        $purchase_order = Purchase_order::findOrfail($request->purchase_order_id);
        $purchase_order->delivered = true;

        foreach ($purchase_order->details as $detail) {
            if($detail->consumable_id != null){
                $consumable = Consumable::findOrfail($detail->consumable_id);
                $consumable->available += $detail->quantity;
                $consumable->update();
            }else{
                $equipment = Equipment::findOrfail($detail->equipment_id);
                $equipment->quantity += $detail->quantity;
                $equipment->update();
            }
        }

        $income_order->save();
        $purchase_order->update();

        return redirect()->route('income_orders.index')->withStatus('Orden de entrada registada correctamente');

    }

    public function code()
    {
        $code = (string)rand(0,99999999);
        if (strlen($code)<5) {
            $code = str_pad($code, 8, "0", STR_PAD_LEFT);
        }
        return $code;
    }



}
