<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Loan;
use DateTime;
use App\Equipment;
use App\User;

class LoanController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $loans = Loan::select('*', 'loans.id as id')
                            ->where(DB::raw("CONCAT(loans.applicant,' ',loans.initial_date,' ',loans.return_date,' ',users.name,' ',equipment.sku)"),'like',"%$search%")
                            ->join('equipment','equipment.id','loans.equipment_id')
                            ->join('users','users.id','loans.user_id')
                            ->paginate(15);

        return view('Loans.index', compact('search','loans'));
    }

    public function create()
    {
        $equipments = Equipment::whereStatus(true)->where('quantity','>','0')->get();
        foreach ($equipments as $equipment) {
            $current_loans = Loan::whereEquipment_id($equipment->id)->count();
            if($current_loans >= $equipment->quantity)
                unset($equipment);
        }
        if($equipments->isEmpty())
            return redirect()->route('loans.index')->withErrors('No hay equipo disponible para préstamo');

        return view('Loans.create',compact('equipments'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'equipment_id' => 'required|numeric|exists:equipment,id',
            'initial_date' => 'required|date|date_format:"Y-m-d"|after_or_equal:today',
            'return_date'  => 'required|date|date_format:"Y-m-d"|after_or_equal:initial_date',
            'applicant'    => 'required|min:3'
        ],[
            'equipment_id.required'       => 'El equipo a prestar es requerido',
            'equipment_id.numeric'        => 'El ID del equipo a prestar debe ser númerico',
            'equipment_id.exists'         => 'El ID del equipo a prestar es inválido',
            'initial_date.required'       => 'La fecha de préstamo es requerida',
            'initial_date.date'           => 'La fecha de préstamo no es válida',
            'initial_date.date_format'    => 'La fecha de préstamo no coincide con el formato Y-m-d',
            'initial_date.after_or_equal' => 'La fecha de préstamo debe ser igual o mayor a la fecha actual',
            'return_date.required'        => 'La fecha de retorno es requerida',
            'return_date.date'            => 'La fecha de retorno es inválida',
            'return_date.date_format'     => 'La fecha de retorno no coincide con el formato Y-m-d',
            'return_date.after_or_equal'  => 'La fecha de retorno debe ser igual o mayor que la fecha de préstamo',
            'applicant.required'          => 'El solicitante es requerido',
            'applicant.min'               => 'El nombre del solicitante debe tener almenos 3 caracteres'

        ]);

        $loan = new Loan();
        $loan->applicant    = $request->applicant;
        $loan->observations = $request->observations;
        $loan->initial_date = $request->initial_date;
        $loan->return_date  = $request->return_date;
        $loan->user_id      = Auth::user()->id;
        $loan->equipment_id = $request->equipment_id;

        $loan->save();

        return redirect()->route('loans.index')->withStatus('Préstamo registrado correctamente');

    }

    public function show($id)
    {
        $loan = Loan::findOrfail($id);

        return view('Loans.show', compact('loan'));
    }

    public function edit($id)
    {
        $equipments = Equipment::whereStatus(true)->get();
        $loan = Loan::findOrfail($id);

        return view('Loans.edit',compact('equipments','loan'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'equipment_id' => 'required|numeric|exists:equipment,id',
            'initial_date' => 'required|date|date_format:"Y-m-d"',
            'return_date'  => 'required|date|date_format:"Y-m-d"|after_or_equal:initial_date',
            'applicant'    => 'required|min:3'
        ],[
            'equipment_id.required'       => 'El equipo a prestar es requerido',
            'equipment_id.numeric'        => 'El ID del equipo a prestar debe ser númerico',
            'equipment_id.exists'         => 'El ID del equipo a prestar es inválido',
            'initial_date.required'       => 'La fecha de préstamo es requerida',
            'initial_date.date'           => 'La fecha de préstamo no es válida',
            'initial_date.date_format'    => 'La fecha de préstamo no coincide con el formato Y-m-d',
            'return_date.required'        => 'La fecha de retorno es requerida',
            'return_date.date'            => 'La fecha de retorno es inválida',
            'return_date.date_format'     => 'La fecha de retorno no coincide con el formato Y-m-d',
            'return_date.after_or_equal'  => 'La fecha de retorno debe ser igual o mayor que la fecha de préstamo',
            'applicant.required'          => 'El solicitante es requerido',
            'applicant.min'               => 'El nombre del solicitante debe tener almenos 3 caracteres'

        ]);

        $loan = Loan::findOrfail($id);
        $loan->applicant    = $request->applicant;
        $loan->observations = $request->observations;
        $loan->initial_date = $request->initial_date;
        $loan->return_date  = $request->return_date;

        $new_equipment = Equipment::findOrfail($request->equipment_id);
        if($new_equipment->id != $loan->equipment_id):

            $old_equipment = Equipment::findOrfail($loan->equipment_id);
            $old_equipment->status = true;
            $old_equipment->update();

            $loan->equipment_id = $request->equipment_id;

            $new_equipment->status = false;
            $new_equipment->update();

        endif;

        $loan->update();

        return redirect()->route('loans.index')->withStatus('Préstamo actualizado correctamente');

    }

    public function destroy(Request $request)
    {
        $loan = Loan::findOrfail($request->id);

        $equipment = Equipment::findOrfail($loan->equipment_id);
        $equipment->status = true;
        $equipment->update();

        $loan->delete();

        return redirect()->route('loans.index')->withStatus('Préstamo eliminado correctamente');
    }
}
