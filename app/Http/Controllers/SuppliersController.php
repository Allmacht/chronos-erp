<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Supplier;

class SuppliersController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $count = Supplier::all()->count();
        $suppliers = Supplier::where(DB::raw("CONCAT(business_name,' ',tradename,' ',rfc,' ',email,' ',country,' ',state,' ',type,' ',phone)"),'like',"%$search%")->paginate(15);
        return view('Suppliers.index', compact('suppliers','search','count'));
    }

    public function create()
    {
        return view('Suppliers.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'business_name'   => 'required',
            'tradename'       => 'required',
            'rfc'             => 'required|max:13|min:12|alpha_num|unique:suppliers',
            'email'           => 'nullable|email',
            'external_number' => 'required|numeric',
            'zipcode'         => 'required|numeric|digits:5',
            'phone'           => 'nullable|numeric|digits:10',
        ],[
            'business_name.required'   => 'Razón social es requerido',
            'tradename.required'       => 'Nombre comercial es requerido',
            'rfc.required'             => 'El R.F.C. es requerido',
            'rfc.max'                  => 'El R.F.C. no puede ser mayor a 13 caracteres',
            'rfc.min'                  => 'El R.F.C. no puede ser menor a 12 caracteres',
            'rfc.alpha_numeric'        => 'El R.F.C. no puede contener caracteres especiales',
            'rfc.unique'               => 'El R.F.C. ingresado ya está en uso',
            'email.email'              => 'El Correo electónico no es válido',
            'external_number.required' => 'El No. exterior es requerido',
            'external_number.numeric'  => 'El No. exterior debe ser númerico',
            'zipcode.required'         => 'El código postal es requerido',
            'zipcode.numeric'          => 'El código postal debe ser númerico',
            'zipcode.digits'           => 'El código postal debe tener 5 caracteres',
            'phone.numeric'            => 'El teléfono debe ser númerico',
            'phone.digits'             => 'El teléfono no puede ser menor a 10 caracteres',
        ]);

        $supplier = new Supplier();

        $supplier->business_name         = $request->business_name;
        $supplier->tradename             = $request->tradename;
        $supplier->rfc                   = $request->rfc;
        $supplier->credit_days           = $request->credit_days;
        $supplier->credit_amount         = $request->credit_amount;
        $supplier->email                 = $request->email;
        $supplier->country               = $request->country;
        $supplier->state                 = $request->state;
        $supplier->municipality          = $request->municipality;
        $supplier->colony                = $request->colony;
        $supplier->street                = $request->street;
        $supplier->external_number       = $request->external_number;
        $supplier->internal_number       = $request->internal_number;
        $supplier->zipcode               = $request->zipcode;
        $supplier->phone                 = $request->phone;
        $supplier->contact_name          = $request->contact_name;
        $request->type ? $supplier->type = 'Compras' : $supplier->type = 'Gastos';

        $supplier->save();

        return redirect()->route('suppliers.index')->withStatus('Proveedor registrado correctamente');
    }
    public function show($id)
    {
        $supplier = Supplier::findOrfail($id);
        return view('Suppliers.show', compact('supplier'));
    }
    public function edit($id)
    {
        $supplier = Supplier::findOrfail($id);
        return view('Suppliers.edit', compact('supplier'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'business_name'   => 'required',
            'tradename'       => 'required',
            'rfc'             => 'required|max:13|min:12|alpha_num',
            'email'           => 'nullable|email',
            'external_number' => 'required|numeric',
            'zipcode'         => 'required|numeric|digits:5',
            'phone'           => 'nullable|numeric|digits:10',
        ],[
            'business_name.required'   => 'Razón social es requerido',
            'tradename.required'       => 'Nombre comercial es requerido',
            'rfc.required'             => 'El R.F.C. es requerido',
            'rfc.max'                  => 'El R.F.C. no puede ser mayor a 13 caracteres',
            'rfc.min'                  => 'El R.F.C. no puede ser menor a 12 caracteres',
            'rfc.alpha_numeric'        => 'El R.F.C. no puede contener caracteres especiales',
            'email.email'              => 'El Correo electónico no es válido',
            'external_number.required' => 'El No. exterior es requerido',
            'external_number.numeric'  => 'El No. exterior debe ser númerico',
            'zipcode.required'         => 'El código postal es requerido',
            'zipcode.numeric'          => 'El código postal debe ser númerico',
            'zipcode.digits'           => 'El código postal debe tener 5 caracteres',
            'phone.numeric'            => 'El teléfono debe ser númerico',
            'phone.digits'             => 'El teléfono no puede ser menor a 10 caracteres',
        ]);

        $supplier = Supplier::findOrfail($id);

        $supplier->business_name         = $request->business_name;
        $supplier->tradename             = $request->tradename;
        $supplier->rfc                   = $request->rfc;
        $supplier->credit_days           = $request->credit_days;
        $supplier->credit_amount         = $request->credit_amount;
        $supplier->email                 = $request->email;
        $supplier->country               = $request->country;
        $supplier->state                 = $request->state;
        $supplier->municipality          = $request->municipality;
        $supplier->colony                = $request->colony;
        $supplier->street                = $request->street;
        $supplier->external_number       = $request->external_number;
        $supplier->internal_number       = $request->internal_number;
        $supplier->zipcode               = $request->zipcode;
        $supplier->phone                 = $request->phone;
        $supplier->contact_name          = $request->contact_name;
        $request->type ? $supplier->type = 'Compras' : $supplier->type = 'Gastos';

        $supplier->update();

        return redirect()->route('suppliers.index')->withStatus('Proveedor actualizado correctamente');

    }
    public function destroy(Request $request)
    {
        $supplier = Supplier::findOrfail($request->id);
        $supplier->delete();

        return redirect()->route('suppliers.index')->withStatus('Proveedor eliminado correctamente');
    }
}
