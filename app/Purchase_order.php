<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase_order extends Model
{
    public function supplier(){
        return $this->belongsTo(Supplier::class);
    }
    public function details(){
        return $this->hasMany(Purchase_detail::class);
    }
    public function approved_order(){
        return $this->belongsTo(Approved_order::class);
    }
}
