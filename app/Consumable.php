<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consumable extends Model
{

    protected $fillable = [
        'sku',
        'description',
        'unit_measurement',
        'type',
        'initial_cost',
        'availability_policy',
        'available',
        'brand',
        'model',
        'supplier_id',
        'department_id'
    ];

    public function supplier(){
        return $this->belongsTo(Supplier::class);
    }
    public function department(){
        return $this->belongsTo(Department::class);
    }
}
