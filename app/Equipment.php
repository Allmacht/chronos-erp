<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    public function supplier(){
        return $this->belongsTo(Supplier::class);
    }
    public function department(){
        return $this->belongsTo(department::class);
    }
}
