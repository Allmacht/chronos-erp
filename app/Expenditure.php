<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenditure extends Model
{
    public function User(){
        return $this->belongsTo(User::class);
    }
    public function Supplier(){
        return $this->belongsTo(Supplier::class);
    }
}
