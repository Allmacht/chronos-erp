<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    public function details(){
        return $this->hasMany(Quotation_detail::class);
    }
}
