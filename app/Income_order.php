<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Income_order extends Model
{
    public function purchase_order(){
        return $this->belongsTo(Purchase_order::class);
    }
}
