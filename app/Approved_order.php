<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Approved_order extends Model
{
    public function User(){
        return $this->belongsTo(User::class);
    }
}
