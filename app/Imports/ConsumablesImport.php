<?php

namespace App\Imports;

use App\Consumable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ConsumablesImport implements ToModel, WithValidation, WithHeadingRow
{
     use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function rules():array{
        return[
            'sku'                        => 'required|unique:consumables',
            'descripcion'                => 'required|min:3',
            'tipo'                       => 'required|min:3',
            'costo_inicial'              => 'required|numeric',
            'unidad_de_medida'           => 'required|min:3',
            'politica_de_disponibilidad' => 'required',
            'departamento_responsable'   => 'required|numeric',
            'proveedor'                  => 'required|numeric|exists:suppliers,id',
            'marca'                      => 'required|min:2',
            'modelo'                     => 'required|min:2',
            'disponible'                 => 'required|numeric'
        ];
    }

    public function customValidationMessages(){
        return [
            'sku.required'                        => 'El campo SKU es requerido',
            'sku.unique'                          => 'El SKU ingresado ya está en uso',
            'descripcion.required'                => 'La descripción es requerida',
            'descripcion.min'                     => 'La descripción debe tener almenos 3 caracteres',
            'tipo.required'                       => 'El tipo es requerido',
            'tipo.min'                            => 'El tipo debe tener almenos 3 caracteres',
            'costo_inicial.required'              => 'El costo inicial es requerido',
            'costo_inicial.numeric'               => 'El costo inicial debe ser númerico',
            'unidad_de_medida.required'           => 'La unidad de medida es requerida',
            'unidad_de_medida.min'                => 'La unidad de medida debe tener almenos 3 carateres',
            'politica_de_disponibilidad.required' => 'La politica de disponibilidad es requerida',
            'departamento_responsable.required'   => 'El ID del departamento responsable es requerido',
            'departamento_responsable.numeric'    => 'El ID del departamento responsable debe ser númerico',
            'proveedor.required'                  => 'El ID del proveedor es requerido',
            'proveedor.numeric'                   => 'El ID del proveedor debe ser númerico',
            'proveedor.exists'                    => 'El ID del proveedor es inválido',
            'marca.required'                      => 'La marca es requerida',
            'marca.min'                           => 'La marca debe tener almenos 2 caracteres',
            'modelo.required'                     => 'El modelo es requerido',
            'modelo.min'                          => 'El modelo debe tener almenos 2 caracteres',
            'disponible.required'                 => 'La cantidad disponible es requerida',
            'disponible.numeric'                  => 'La cantidad disponible debe ser númerica'
        ];
    }

    public function model(array $row)
    {
        return new Consumable([
            'sku'                 => $row['sku'],
            'description'         => $row['descripcion'],
            'unit_measurement'    => $row['unidad_de_medida'],
            'type'                => $row['tipo'],
            'initial_cost'        => $row['costo_inicial'],
            'availability_policy' => $row['politica_de_disponibilidad'],
            'supplier_id'         => $row['proveedor'],
            'brand'               => $row['marca'],
            'model'               => $row['modelo'],
            'available'           => $row['disponible'],
            // 'department_id' => $row[4],
        ]);
    }

}
