<?php

namespace App\Imports;

use App\Equipment;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class EquipmentImport implements ToModel, WithValidation, WithHeadingRow
{
     use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function rules():array
    {
        return [
            'sku'                      => 'required|unique:equipment,sku',
            'descripcion'              => 'required|min:3',
            'unidad_de_medida'         => 'required|min:3',
            'departamento_responsable' => 'required|numeric',
            'categoria'                => 'required|min:3',
            'proveedor'                => 'required|numeric|exists:suppliers,id',
            'marca'                    => 'required|min:2',
            'modelo'                   => 'required|min:3',
            'cantidad'                 => 'required|numeric'
        ];
    }

    public function customValidationMessages()
    {
        return [
            'sku.required'                      => 'El campo SKU es requerido',
            'sku.unique'                        => 'El SKU ingresado ya está en uso',
            'descripcion.required'              => 'La descripción es requerida',
            'descripcion.min'                   => 'La descripción debe tener almenos 3 caracteres',
            'unidad_de_medida.required'         => 'La unidad de medida es requerida',
            'unidad_de_medida.min'              => 'La unidad de medida debe tener almenos 3 caracteres',
            'departamento_responsable.required' => 'El ID del departamento responsable es requerido',
            'departamento_responsable.numeric'  => 'El ID del departamento responsable debe ser númerico',
            'categoria.required'                => 'La categoría es requerida',
            'categoria.min'                     => 'La categoría debe tener almenos 3 caracteres',
            'proveedor.required'                => 'El ID del proveedor es requerido',
            'proveedor.numeric'                 => 'El ID del proveedor debe ser númerico',
            'proveedor.exists'                  => 'El ID del proveedor no es válido',
            'marca.required'                    => 'La marca es requerido',
            'marca.min'                         => 'La marca debe tener almenos 2 caracteres',
            'modelo.required'                   => 'El modelo es requerido',
            'modelo.min'                        => 'El modelo debe tener almenos 3 caracteres',
            'cantidad.required'                 => 'La cantidad es requerida',
            'cantidad.numeric'                  => 'La cantidad debe ser númerica'
        ];
    }

    public function model(array $row)
    {
        
        $equipment = new Equipment();

        $equipment->sku              = $row['sku'];
        $equipment->description      = $row['descripcion'];
        $equipment->unit_measurement = $row['unidad_de_medida'];
        $equipment->category         = $row['categoria'];
        $equipment->quantity         = $row['cantidad'];
        $equipment->brand            = $row['marca'];
        $equipment->model            = $row['modelo'];
        $equipment->supplier_id      = $row['proveedor'];
        // $equipment->department_id    = $row['departamento_responsable'];

        $equipment->save();

        return $equipment;
    }
}
